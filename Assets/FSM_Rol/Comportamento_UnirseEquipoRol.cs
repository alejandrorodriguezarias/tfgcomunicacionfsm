﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comportamento_UnirseEquipoRol : MonoBehaviour
{

    private Axente_FSM_Rol axente;
    private List<string> nomesEquipo = new List<string>();

    /// <summary>
    /// Obten o axente do comportamento , o lider de equipo do axente e envia un mensaxe aceptando unirse ao equipo
    /// </summary>
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
    }

    /// <summary>
    /// Recibe os mensaxes inform neste comportamento, se o inform é unha lista de nomes de equipo dispara o evento EquipoFormado
    /// </summary>
    /// <param name="contido">Contido da mensaxe</param>
    /// <param name="remitente">Nome do axente que envia a mensaxe</param>
    public void TratarInform(string contido)
    {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("equipo")){
            string[] nomes = contidoDiv[1].Split(',');
            foreach (string nome in nomes){
                if (!nome.Equals("")) {
                    nomesEquipo.Add(nome);
                }
            }
            axente.nomesEquipo =  nomesEquipo;
            axente.nomesEquipoCompleto = nomesEquipo;
            axente.DispararEvento((int)Eventos_Rol.EventoEquipoFormado);
        }
    }
    /// <summary>
    /// Recibe os mensaxes request neste comportamento, se é un request para unirse a un equipo respondese cun accept
    /// </summary>
    /// <param name="contido">Contido da mensaxe</param>
    /// <param name="remitente">Nome do axente que envia a mensaxe</param>
    public void TratarRequest(string contido, string remitente , string id)
    {
        if (contido.Equals("equipo"))
        {
            Script_Comunicacion receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("accept", null, gameObject.name, remitente, receptor, id);
        }
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar(){
        Destroy(this);
    }
}
public class Invocar_UnirseEquipoRol : Comportamento
{
    private GameObject obxecto = null;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_UnirseEquipoRol>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_UnirseEquipoRol componente = gameObject.AddComponent<Comportamento_UnirseEquipoRol>() as Comportamento_UnirseEquipoRol;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        obxecto.GetComponent<Comportamento_UnirseEquipoRol>().TratarInform(contido);
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        obxecto.GetComponent<Comportamento_UnirseEquipoRol>().TratarRequest(contido, remitente, id);
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}
