﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_DividirAtaque : MonoBehaviour
{
    //Axente propietario do script
    private Axente_FSM_Rol axente;
    private NavMeshAgent axenteNav;
    private List<string> nomesEquipo;
    private bool comprobarMensaxes;
    private bool manexarDistancias;
    private float tempoComprobacions;
    private const float TempoEntreComprobacions = 0.1f;
    private Dictionary<string,float> distancias = new Dictionary<string,float>();
    private Dictionary<string, Dictionary<string, float>> distanciasEquipo = new Dictionary<string, Dictionary<string, float>>();
    List<Transform> entradas;
    
    //Prueba
    bool calcularDistancias = false;
    int iteradorDistancias = 0;
    Transform entradaActual = null;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = axente.GetComponent<NavMeshAgent>();
        entradas = ObterEntradas();
        nomesEquipo = new List<string>(axente.nomesEquipo);
        tempoComprobacions = TempoEntreComprobacions;
        ComprobarEquipo();
        axenteNav.isStopped = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (comprobarMensaxes)
        {
            if (tempoComprobacions <= 0)
            {
                if (MesmoEstado())
                {
                    comprobarMensaxes = false;
                    tempoComprobacions = TempoEntreComprobacions;
                    calcularDistancias = true;
                    
                }
                else
                {
                    comprobarMensaxes = false;
                    tempoComprobacions = TempoEntreComprobacions;
                    LimparConversas();
                    ComprobarEquipo();
                }
            }
            else
            {
                tempoComprobacions -= Time.deltaTime;
            }
        }
        if (calcularDistancias)
        {
            entradaActual = entradas[iteradorDistancias];
            axenteNav.destination = entradaActual.position;
            if (!axenteNav.pathPending)
            {
                distancias.Add(entradaActual.gameObject.name, Vector3.Distance(gameObject.transform.position, entradaActual.position));
                if (iteradorDistancias < entradas.Count - 1)
                {
                    iteradorDistancias += 1;
                }
                else
                {
                    calcularDistancias = false;
                    iteradorDistancias = 0;
                    Planificar();
                }
            }
        }
        if (manexarDistancias)
        {
            if (tempoComprobacions <= 0)
            {
                manexarDistancias = false;
                PedirDestino();
                tempoComprobacions = TempoEntreComprobacions;
            }
            else
            {
                tempoComprobacions -= Time.deltaTime;
            }
        }
    }
    /// <summary>
    /// Calcula as entradas que os defensores deben bloquear para parar ao axente
    /// </summary>
    /// <returns>A lista de entradas as habitaciones mas cercanas a obxectivo</returns>
    private List<Transform> ObterEntradas()
    {
        bool menor = true;
        int i = 0;
        string habitacion = axente.habitacionXogador;
        //Script_ControladorXogador xogador = GameObject.Find("Xogador").GetComponent<Script_ControladorXogador>();
        Sala sala = GameObject.Find(habitacion).GetComponent<Sala>();
        List<Transform> entradas = new List<Transform>();
        List<Cela> celas = new List<Cela>(sala.ObterEntradas());
        while (entradas.Count < axente.nomesEquipo.Count)
        {
            menor = true;
            if (celas.Count == 0)
            {
                break;
            }
            Cela cela = celas[i];
            //Comprobamos se a entrada é a que esta mais cerca da habitacion obxectivo do ladron
            foreach (Cela outraCela in celas)
            {
                if (Vector3.Distance(cela.transform.position, axente.habitacionObxectivo.transform.position) > Vector3.Distance(outraCela.transform.position, axente.habitacionObxectivo.transform.position))
                {
                    menor = false;
                    break;
                }
            }
            //Se é a menor a engadimos a lista de entradas
            if (menor)
            {
                entradas.Add(cela.transform);
                celas.Remove(cela);
            }

            //A seguinte entrada pode formar parte da misma sala o doutra
            if (celas.Count != 0){
                i = (i + 1) % celas.Count;
            }
        }
        return entradas;
    }

    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    

    /// <summary>
    /// Pregunta ao resto dos axentes do equipo se estan no estado planificar
    /// </summary>
    private void ComprobarEquipo() {
        Script_Comunicacion receptor;
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("query", "estadoPlanificar", gameObject.name, nome, receptor, "-1");
        }
        comprobarMensaxes = true;
    }
    /// <summary>
    /// Comportamento cando recibese unha mensaxe query, se esta e unha pregunta por estado planificar respondese true
    /// </summary>
    /// <param name="contido">contido da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    public void TratarQuery(string contido, string remitente, string id)
    {

        if (contido.Equals("estadoPlanificar"))
        {
            Script_Comunicacion receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            if (axente != null)
            {
                axente.EnviarMensaxe("inform", "estadoPlanificar\\true", gameObject.name, remitente, receptor, id);
            }

        }
    }
    /// <summary>
    /// Comproba se todos os axentes responderon ao mensaxe do estado recorrendo as conversas pendentes
    /// </summary>
    /// <returns>true se todos os axentes estan no estado planificar, false noutro caso</returns>
    private bool MesmoEstado() {
        bool mesmoEstado = true;
        List<Conversa> listaConversas = axente.ObterConversas();
        foreach (Conversa conversa in listaConversas)
        {
            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar")) {
                mesmoEstado = false;
                break;
            }
        }
        return mesmoEstado;
    }
    /// <summary>
    /// Antes de reenviar as preguntas do estado limpanse das conversas as que quedaran colgando
    /// </summary>
    private void LimparConversas() {
        List<Conversa> listaConversasAux = axente.ObterConversas();
        List<Conversa> listaConversas = new List<Conversa>(axente.ObterConversas());
        foreach (Conversa conversa in listaConversasAux)
        {
            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar"))
            {
                listaConversas.Remove(conversa);
            }
        }
        axente.EstablecerConversas(listaConversas);
    }

   
    /// <summary>
    /// Funcionalidad principal do comportamento
    /// </summary>
    private void Planificar() {
        InformarSobreDistancias();
    }

    /// <summary>
    /// Comunica ao resto do equipo a sua posicion respecto as entradas
    /// </summary>
    private void InformarSobreDistancias() {
        Script_Comunicacion receptor;
        string contido = "distancias";
        foreach (KeyValuePair<string, float> distancia in distancias)
        {
            contido +=  "\\" + distancia.Key + "\\" + distancia.Value;
        }
        foreach (string nome in nomesEquipo){
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", contido, gameObject.name, nome, receptor, "-1");
        }
        manexarDistancias = true;
    }

    /// <summary>
    /// Se os axentes informan das suas distancias almacenanse, se un axente solicita unha entrada a eliminamos das posiblidades deste axente
    /// </summary>
    /// <param name="contido">contido da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    public void TratarInform(string contido, string remitente) {
        Dictionary<string, float> distancias = new Dictionary<string, float>();
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("distancias"))
        {
            for (int i = 1; i < contidoDiv.Length; i += 2)
            {
                distancias.Add(contidoDiv[i], float.Parse(contidoDiv[i + 1]));
            }
            distanciasEquipo.Add(remitente, distancias);
        }
        else if (contidoDiv[0].Equals("eleccion"))
        {
            distanciasEquipo.Remove(remitente);
            nomesEquipo.Remove(remitente);
            foreach (Transform entrada in entradas)
            {
                if (entrada.name.Equals(contidoDiv[1]))
                {
                    entradas.Remove(entrada);
                    break;
                }
            }
        }else if (contidoDiv[0].Equals("engadirEquipo"))
        {
            if (contidoDiv[1].Equals("atacantes"))
            {
                axente.nomesEquipo = axente.atacantes;
                axente.DispararEvento((int)Eventos_Rol.EventoCambioEquipo);
            }
        }
    }

    /// <summary>
    /// Selecciona o mellor destino posible para o axente (o mais curto) e o comunica ao resto do equipo
    /// </summary>
    public void PedirDestino() {
        List<Tuple<string, float>> melloresDestinos;
        string mellorDestino = null;
        float menorDistancia = -1;
        //Se xa disponse de todas as distancias
        if (distanciasEquipo.Count == nomesEquipo.Count - 1)
        {
            melloresDestinos = ObterMelloresDestinos();
            if (melloresDestinos != null)
            {
                if (melloresDestinos.Count > 0)
                {
                    foreach (Tuple<string, float> destino in melloresDestinos)
                    {
                        if ((menorDistancia == -1) || (menorDistancia > destino.Item2))
                        {
                            mellorDestino = destino.Item1;
                            menorDistancia = destino.Item2;
                        }
                    }
                    if (mellorDestino != null)
                    {
                        foreach (string nome in nomesEquipo)
                        {
                            Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                            axente.EnviarMensaxe("inform", "eleccion\\" + mellorDestino, gameObject.name, nome, receptor, "-1");
                        }
                        string[] destinoDiv = mellorDestino.Split(',');
                        axente.destino = destinoDiv[1] + "," + destinoDiv[2];
                        axente.DispararEvento((int)Eventos_Rol.EventoFinDividir);
                    }
                }
                else
                {
                    manexarDistancias = true;
                }
            }
        }
        else
        {
            manexarDistancias = true;
        }
    }
    /// <summary>
    /// Funcion auxiliar que calcula as entradas mais cercanas ao axente que ao resto do equipo
    /// </summary>
    /// <returns>A lista coas entradas cercanas</returns>
    public List<Tuple<string,float>> ObterMelloresDestinos() {
        List<Tuple<string, float>> melloresDestinos = new List<Tuple<string, float>>();
        bool menor;
        Dictionary<string, Dictionary<string, float>> distanciasEquipo = new Dictionary<string, Dictionary<string, float>>(this.distanciasEquipo);
        ActualizarDistancias();
        Dictionary<string, float> distancias = new Dictionary<string, float>(this.distancias);
        //Se non quedan distancias van ao centro
        if (distancias.Count == 0)
        {
            string centroHabitacion = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro().gameObject.name;
            string[] centroHabitacionDiv = centroHabitacion.Split(',');
            axente.destino = centroHabitacionDiv[1] + "," + centroHabitacionDiv[2];
            axente.DispararEvento((int)Eventos_Rol.EventoFinDividir);
            return null;
            //entradas = ObterEntradasPrediccion();
        }

        foreach (KeyValuePair<string, float> distancia in distancias)
        {
            menor = true;
            //masCercano = true;
            foreach (KeyValuePair<string, Dictionary<string, float>> distAxente in distanciasEquipo)
            {
                if (distAxente.Value[distancia.Key] < distancia.Value)
                {
                    menor = false;
                }
            }
            if (menor)
            //if(masCercano)
            {
                melloresDestinos.Add(new Tuple<string, float>(distancia.Key, distancia.Value)); //CAMBIAR para que vaya al menor de las suyas
            }
        }
        return melloresDestinos;
    }
    /// <summary>
    /// Actualiza as distancias do axente eliminando aqueles puntos que foron seleccionados por outro axente
    /// </summary>
    public void ActualizarDistancias() {
        bool borrar;
        Dictionary<string, float> distanciasAux = new Dictionary<string, float>(distancias);
        if (distancias.Count != entradas.Count){
            foreach (KeyValuePair<string, float> distancia in distanciasAux){
                borrar = true;
                foreach (Transform entrada in entradas) {
                    if (distancia.Key.Equals(entrada.name)) {
                        borrar = false;
                    }
                }
                if (borrar){
                    distancias.Remove(distancia.Key);
                }
            }
        }

    }
}
public class Invocar_DividirAtaque : Comportamento
{
    GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_DividirAtaque>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_DividirAtaque componente = gameObject.AddComponent<Comportamento_DividirAtaque>() as Comportamento_DividirAtaque;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_DividirAtaque>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_DividirAtaque>().TratarQuery(contido, remitente, id);
        }
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
    }
}
