﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_EntrarSalaRol : MonoBehaviour
{
    private Axente_FSM_Rol axente;
    private NavMeshAgent axenteNav;
    private RecorrerCamiñoRol axenteMov;
    private List<string> nomesEquipo;
    private Dictionary<string, bool> respostasEquipoPosicion = new Dictionary<string, bool>();
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiñoRol>();
        PrepararCamiñoCela();
        axenteNav.isStopped = false;
    }

    private void PrepararCamiñoCela() {
        Cela cela = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro();
        string[] celaDiv = cela.gameObject.name.Split(',');
        axente.destino = celaDiv[1] + ',' + celaDiv[2];
        axente.camino = buscador.ObterCamino(gameObject);
        axenteMov.ObterDestino();
        
    }

    public void TratarInform(string contido, string remitente) {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("CoordXogador"))
        {
            axente.destino = contidoDiv[1];
            axente.DispararEvento((int)Eventos_Rol.EventoVerXogador);
        }else if (contidoDiv[0].Equals("engadirEquipo"))
        {
            if (contidoDiv[1].Equals("atacantes"))
            {
                axente.nomesEquipo = axente.atacantes;
                axente.DispararEvento((int)Eventos_Rol.EventoCambioEquipo);
            }
        }
    }
    void Update()
    {
        if (axenteMov.finCamino) {
            axente.DispararEvento((int)Eventos_Rol.EventoXogadorPerdido);
        }
    }

    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }
}

public class Invocar_EntrarSalaRol : Comportamento
{
    GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_EntrarSalaRol>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_EntrarSalaRol componente = gameObject.AddComponent<Comportamento_EntrarSalaRol>() as Comportamento_EntrarSalaRol;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_EntrarSalaRol>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
    }
}
