﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RecorrerCamiñoRol : MonoBehaviour
{

    Axente_FSM_Rol axente;
    private Cela proximaCela;
    int i = 0;
    List<string> caminos = null;
    NavMeshAgent axenteNav;
    private const float RetrasoPortaPechada = 5f;
    public bool finCamino { get; set; } = false;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
    }

    public void ObterDestino() {
        i = 0;
        finCamino = false;
        caminos = axente.camino;
        if (axente.camino != null)
        {
            if (axente.camino.Count == 0)
            {
                finCamino = true;
            }
            else {
                axenteNav.destination = gameObject.transform.position;
                proximaCela = GameObject.Find(caminos[i]).GetComponent<Cela>();
                proximaCela.ocupada = false;
                if (proximaCela.EPorta())
                {
                    axenteNav.isStopped = true;
                    Script_Porta porta = proximaCela.transform.GetChild(0).gameObject.GetComponent<Script_Porta>();
                    if (porta.EstaPechada())
                    {
                        porta.Abrir();
                        StartCoroutine("EsperarPorta");
                    }
                    else
                    {
                        porta.Reinicio();
                        axenteNav.isStopped = false;
                    }
                }
                i++;
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (axenteNav.remainingDistance < 1 && axente.camino !=null && !axenteNav.pathPending)
        {
            axente.celaActual = proximaCela;
            if (i < caminos.Count)
            {
                proximaCela.ocupada = false;
                proximaCela = GameObject.Find(caminos[i]).GetComponent<Cela>();
                if (!proximaCela.ocupada)
                {
                    //proximaCela.ocupada = true;
                    //Debug.Log("proximaCela: " + proximaCela.transform.position);
                    axenteNav.destination = proximaCela.transform.position;
                    if (proximaCela.EPorta())
                    {
                        axenteNav.isStopped = true;
                        Script_Porta porta = proximaCela.scriptPorta;
                        if (porta.EstaPechada())
                        {
                            porta.Abrir();
                            StartCoroutine("EsperarPorta");
                        }
                        else
                        {
                            porta.Reinicio();
                            axenteNav.isStopped = false;
                        }
                    }
                    i++;
                }
                else
                {
                    axente.camino = buscador.ObterCamino(gameObject);
                    ObterDestino();
                }
            }
            else
            {
                finCamino = true;
                proximaCela.ocupada = true;
                axenteNav.isStopped = true;
            }
        }
    }
    IEnumerator EsperarPorta()
    {
        yield return new WaitForSeconds(0.8f);
        axenteNav.isStopped = false;
    }
}
