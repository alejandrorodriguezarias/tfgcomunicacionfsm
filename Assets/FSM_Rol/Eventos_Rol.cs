﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Eventos_Rol : int {

    EventoVerXogador= 1,
    EventoFormarEquipo = 2,
    EventoEquipoFormado = 3,
    EventoPatrullarPortero = 4,
    EventoFinDividir = 5,
    EventoXogadorPerdidoDefensa = 6,
    EventoEnPosicion = 7,
    EventoXogadorPerdido = 8,
    EventoCambioHabitacion = 9,
    EventoRolPortero = 10,
    EventoRolDefensa = 11,
    EventoRolAtacante = 12,
    EventoRolPerseguidor = 13,
    EventoBotinRobado = 14,
    EventoCambioAtacante = 15,
    EventoCambioEquipo = 16,
}
