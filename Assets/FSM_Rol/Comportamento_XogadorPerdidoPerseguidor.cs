﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_XogadorPerdidoPerseguidor : MonoBehaviour
{
    private Axente_FSM_Rol axente;
    private NavMeshAgent axenteNav;
    private RecorrerCamiñoRol axenteMov;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    private float cambioDirBusqueda = 0f;
    private int radioBusqueda = 5;
    private const float TempoDirBusqueda = 15.0f;
    private bool enPosicion = true;
    Vector3 centroBusqueda;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiñoRol>();
        centroBusqueda = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro().transform.position;
        //centroBusqueda = transform.position;
        axenteNav.isStopped = true;
    }

    // Update is called once per frame
    void Update()
    {
        AtoparXogador();
    }

    /// <summary>
    /// Funcion de control para os cambios de direccion do axente, gradualmente vai aumentando o radio de busqueda
    /// </summary>
    private void AtoparXogador()
    {
        if ((axenteMov.finCamino) || (cambioDirBusqueda <= 0))
        {
            //se podría aumentar la dir de busqueda a cada cambio de dirección
            DirAleatoria(radioBusqueda);
            radioBusqueda += 1;
            cambioDirBusqueda = TempoDirBusqueda;
            axenteNav.isStopped = false;
        }
        else
        {
            cambioDirBusqueda = cambioDirBusqueda - Time.deltaTime;
        }
    }
    /// <summary>
    /// Obten un punto aleatorio dentro dun radio determinado
    /// </summary>
    /// <param name="radio">radio de busqueda</param>
    /// <returns></returns>
    private void DirAleatoria(int radio)
    {
        string destino = "";
        string centro = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro().gameObject.name;
        string[] centroDiv = centro.Split(',');
        int xCoord = int.Parse(centroDiv[1]);
        int yCoord = int.Parse(centroDiv[2]);
        int explrX = Random.Range(-radio, radio);
        int explrY = Random.Range(-radio, radio);
        xCoord += explrX;
        yCoord += explrY;
        destino = "" + xCoord + ',' + yCoord;
        GameObject celaGameObject = GameObject.Find("cela," + destino);
        if (celaGameObject != null) {
            axente.destino = destino;
            axente.camino = buscador.ObterCamino(gameObject);
            axenteMov.ObterDestino();
        }
    }

    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    /// <summary>
    /// Se a mensaxe query pregunta se o axente esta en posicion se responde.
    /// </summary>
    /// <param name="contido">pregunta da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    public void TratarQuery(string contido, string remitente, string id)
    {
        if (contido.Equals("enPosicion"))
        {
            Script_Comunicacion receptor;
            receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "enPosicion" + "\\" + enPosicion.ToString(), gameObject.name, remitente, receptor, id);
        }
    }

    /// <summary>
    /// Recibe a posicion do xogador doutro axente
    /// </summary>
    /// <param name="posXogador">Vector3 Posicion do xogador</param>
    public void TratarInform(string contido, string remitente)
    {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("CoordXogador"))
        {
            string destino = contidoDiv[1];
            axente.destino = destino;
            axente.camino = buscador.ObterCamino(gameObject);
            if (axente.camino == null)
            {
                axenteNav.isStopped = true;
            }
            else
            {
                axenteMov.ObterDestino();
                axenteNav.isStopped = false;
                axente.DispararEvento((int)Eventos_Rol.EventoVerXogador);
            }
        }
    }
    public void TratarRequest(string contido, string remitente, string id)
    {
        if (contido.Equals("equipo"))
        {
            if (axente == null)
            {
                axente = gameObject.GetComponent<Axente_FSM_Rol>();
            }
            //if (!axente.habitacionCorrecta)
            //{
            //    axente.idConversaLider = id;
            //    axente.liderEquipo = remitente;
            //    axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
            //}
        }
    }

    public void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        if (axente != null)
        {
            string[] celasDiv = posXogador.name.Split(',');
            axente.destino = celasDiv[1] + ',' + celasDiv[2];
            foreach (string nome in axente.nomesEquipo)
            {
                Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                axente.EnviarMensaxe("inform", "CoordXogador\\" + celasDiv[1] + ',' + celasDiv[2], gameObject.name, nome, receptor, "-1");
            }
            axente.DispararEvento((int)Eventos_Celas.EventoVerXogador);
        }
    }
}

public class Invocar_XogadorPerdidoPerseguidor : Comportamento
{
    GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_XogadorPerdidoPerseguidor>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_XogadorPerdidoPerseguidor componente = gameObject.AddComponent<Comportamento_XogadorPerdidoPerseguidor>() as Comportamento_XogadorPerdidoPerseguidor;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_XogadorPerdidoPerseguidor>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_XogadorPerdidoPerseguidor>().TratarRequest(contido, remitente, id);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_XogadorPerdidoPerseguidor>().EscoitarXogador(posXogador, habitacionXogador);
        }
    }
}
