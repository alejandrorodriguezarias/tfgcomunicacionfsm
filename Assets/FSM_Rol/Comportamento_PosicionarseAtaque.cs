﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PosicionarseAtaque : MonoBehaviour
{

    //Axente propietario do script
    private Axente_FSM_Rol axente;
    private NavMeshAgent axenteNav;
    private RecorrerCamiñoRol axenteMov;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    private Transform centro;
    private bool enPosicion { get; set; } = false;
    private const float TempoReenvio = 0.8f;
    private float tempoUltimaComprobacion { get; set; } = TempoReenvio;
    private Dictionary<string, bool> equipoPosicion { get; set; }  = new Dictionary<string, bool>();

    /// <summary>
    /// Obtemos o axente propietario do script e comeza o seu comportamento
    /// </summary>
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiñoRol>();
        axente.EstablecerConversas(new List<Conversa>());
        axente.camino = buscador.ObterCamino(gameObject);
        centro = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro().transform;
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
        axenteNav.speed = 4f;
    }

    /// <summary>
    /// Cada frame comproba se xa se enviaron as mensaxes e se foron respondidas
    /// </summary>
    void Update()
    {
        if (axenteMov.finCamino) {
            axenteNav.isStopped = true;
            gameObject.transform.LookAt(centro);
            enPosicion = true;
            InformarEnPosicion();
        }
        if (enPosicion && tempoUltimaComprobacion < 0)
        {
            tempoUltimaComprobacion = TempoReenvio;
            if (ComprobarEquipo())
            {
                InformarEnPosicion();
                axente.DispararEvento((int)Eventos_Rol.EventoEnPosicion);
            }
        }
        else
        {
            tempoUltimaComprobacion -= Time.deltaTime;
        }
    }

    private void InformarEnPosicion()
    {
        foreach (string nome in axente.nomesEquipo)
        {
            Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "enPosicion\\" + enPosicion, gameObject.name, nome, receptor, "-1");
        }
    }

    private bool ComprobarEquipo()
    {
        bool equipoEnPosicion = true;
        if (equipoPosicion.Count != (axente.nomesEquipo.Count - 1))
        {
            equipoEnPosicion = false;
        }
        else
        {
            foreach (KeyValuePair<string, bool> patrulla in equipoPosicion)
            {
                if (!patrulla.Value)
                {
                    equipoEnPosicion = false;
                }
            }
        }
        return equipoEnPosicion;

    }

    public void TratarInform(string contido, string remitente)
    {
        string[] contidoDiv = contido.Split('\\');
        if (axente != null) {
            axente = gameObject.GetComponent<Axente_FSM_Rol>();
        }
        
        if (contidoDiv[0].Equals("enPosicion"))
        {
            try
            {
                equipoPosicion.Add(remitente, contidoDiv[1].Equals("True") ? true : false);
            }
            catch (ArgumentException)
            {
                equipoPosicion[remitente] = contidoDiv[1].Equals("True") ? true : false;
            }
        }else if (contidoDiv[0].Equals("engadirEquipo"))
        {
            if (contidoDiv[1].Equals("atacantes"))
            {
                axente.nomesEquipo = axente.atacantes;
                axente.DispararEvento((int)Eventos_Rol.EventoCambioEquipo);
            }
        }
    }

    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }
}
public class Invocar_PosicionarseAtaque : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        //Comportamento_PosicionarseDefensa aux = ;
        //if (aux != null) {
        //    aux.Eliminar();
        //}    
        gameObject.GetComponent<Comportamento_PosicionarseAtaque>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PosicionarseAtaque componente = gameObject.AddComponent<Comportamento_PosicionarseAtaque>() as Comportamento_PosicionarseAtaque;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PosicionarseAtaque>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
    }
}