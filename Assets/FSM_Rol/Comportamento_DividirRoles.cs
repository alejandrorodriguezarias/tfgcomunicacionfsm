﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.AI;

//public class NewBehaviourScript : MonoBehaviour
//{
//    //Axente propietario do script
//    private Axente_FSM_Rol axente;
//    private NavMeshAgent axenteNav;
//    //Equipo
//    private List<string> nomesEquipo;
//    //Flag control
//    private bool comprobarMensaxes = false;
//    private bool calcularDistancias = false;
//    //Control fluxo de mensaxes
//    private float tempoComprobacions;
//    private const float TempoEntreComprobacions = 0.1f;
//    // Start is called before the first frame update
//    void Start()
//    {
//        axente = gameObject.GetComponent<Axente_FSM_Rol>();
//        axenteNav = axente.GetComponent<NavMeshAgent>();
//        nomesEquipo = new List<string>(axente.nomesEquipo);
//        ComprobarEquipo();
//        axenteNav.isStopped = true;

//    }

//    /// <summary>
//    /// Pregunta ao resto dos axentes do equipo se estan no estado planificar
//    /// </summary>
//    private void ComprobarEquipo()
//    {
//        Script_Comunicacion receptor;
//        foreach (string nome in nomesEquipo)
//        {
//            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
//            axente.EnviarMensaxe("query", "estadoPlanificar", gameObject.name, nome, receptor, "-1");
//        }
//        comprobarMensaxes = true;
//    }
//    /// <summary>
//    /// Comproba se todos os axentes responderon ao mensaxe do estado recorrendo as conversas pendentes
//    /// </summary>
//    /// <returns>true se todos os axentes estan no estado planificar, false noutro caso</returns>
//    private bool MesmoEstado()
//    {
//        bool mesmoEstado = true;
//        List<Conversa> listaConversas = axente.ObterConversas();
//        foreach (Conversa conversa in listaConversas)
//        {
//            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar"))
//            {
//                mesmoEstado = false;
//                break;
//            }
//        }
//        return mesmoEstado;
//    }

//    /// <summary>
//    /// Antes de reenviar as preguntas do estado limpanse das conversas as que quedaran colgando
//    /// </summary>
//    private void LimparConversas()
//    {
//        List<Conversa> listaConversasAux = axente.ObterConversas();
//        List<Conversa> listaConversas = new List<Conversa>(axente.ObterConversas());
//        foreach (Conversa conversa in listaConversasAux)
//        {
//            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar"))
//            {
//                listaConversas.Remove(conversa);
//            }
//        }
//        axente.EstablecerConversas(listaConversas);
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        //Espera a que todo o equipo este neste estado
//        if (comprobarMensaxes)
//        {
//            if (tempoComprobacions <= 0)
//            {
//                if (MesmoEstado())
//                {
//                    comprobarMensaxes = false;
//                    tempoComprobacions = TempoEntreComprobacions;
//                    calcularDistancias = true;

//                }
//                else
//                {
//                    comprobarMensaxes = false;
//                    tempoComprobacions = TempoEntreComprobacions;
//                    LimparConversas();
//                    ComprobarEquipo();
//                }
//            }
//            else
//            {
//                tempoComprobacions -= Time.deltaTime;
//            }
//        }
//        //Calcula a distancia deste axente os diferentes puntos
//        if (calcularDistancias)
//        {
//            entradaActual = entradas[iteradorDistancias];
//            axenteNav.destination = entradaActual.position;
//            if (!axenteNav.pathPending)
//            {
//                distancias.Add(entradaActual.gameObject.name, Vector3.Distance(gameObject.transform.position, entradaActual.position));
//                if (iteradorDistancias < entradas.Count - 1)
//                {
//                    iteradorDistancias += 1;
//                }
//                else
//                {
//                    calcularDistancias = false;
//                    iteradorDistancias = 0;
//                    Planificar();
//                }
//            }
//        }
//    }
//}
