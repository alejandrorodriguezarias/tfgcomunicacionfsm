﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PosicionarseDefensa : MonoBehaviour
{

    //Axente propietario do script
    private Axente_FSM_Rol axente;
    private NavMeshAgent axenteNav;
    private RecorrerCamiñoRol axenteMov;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    private Transform centro;

    /// <summary>
    /// Obtemos o axente propietario do script e comeza o seu comportamento
    /// </summary>
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiñoRol>();
        axente.EstablecerConversas(new List<Conversa>());
        axente.camino = buscador.ObterCamino(gameObject);
        //centro = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro().transform;
        centro = ObterCentro();
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
        axenteNav.speed = 4f;
    }

    /// <summary>
    /// Cada frame comproba se xa se enviaron as mensaxes e se foron respondidas
    /// </summary>
    void Update()
    {
        if (axenteMov.finCamino) {
            axenteNav.isStopped = true;
            gameObject.transform.LookAt(centro);
        }
    }

    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    /// <summary>
    /// Recibe a posicion do xogador doutro axente
    /// </summary>s
    /// <param name="posXogador">Vector3 Posicion do xogador</param>
    public void TratarInform(string contido, string remitente)
    {
        if (axente == null) {
            axente = gameObject.GetComponent<Axente_FSM_Rol>();
        }
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("abandonarEquipo"))
        {
            if (contidoDiv[1].Equals("defensas"))
            {
                axente.nomesEquipo = axente.defensas;
            }
        }
    }

    public Transform ObterCentro() {
        string cela;
        cela = "cela," + axente.destino;
        Transform obxecto = GameObject.Find(cela).transform.parent;
        Sala sala = obxecto.gameObject.GetComponent<Sala>();
        return sala.ObterCentro().transform;
    }
}
public class Invocar_PosicionarseDefensa : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        //Comportamento_PosicionarseDefensa aux = ;
        //if (aux != null) {
        //    aux.Eliminar();
        //}    
        gameObject.GetComponent<Comportamento_PosicionarseDefensa>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PosicionarseDefensa componente = gameObject.AddComponent<Comportamento_PosicionarseDefensa>() as Comportamento_PosicionarseDefensa;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PosicionarseDefensa>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
    }
}