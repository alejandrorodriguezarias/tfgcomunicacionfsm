﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Escoitar_Sensor : MonoBehaviour
{
    public abstract void Escoitar(Cela cela, string habitacionXogador);
}
