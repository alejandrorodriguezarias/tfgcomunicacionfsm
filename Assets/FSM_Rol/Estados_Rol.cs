﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Estados_Rol {

    public const string EstadoPatrullar = "E0";
    public const string EstadoFormarEquipo = "E1";
    public const string EstadoUnirseEquipo = "E2";
    public const string EstadoDividirRol = "E3";
    public const string EstadoDividirPortero = "E16";
    public const string EstadoPatrullarPortero = "E4";
    public const string EstadoPerseguirPortero = "E5";
    public const string EstadoDividirDefensa = "E6";
    public const string EstadoPosicionarseDefensa = "E7";
    public const string EstadoPerseguirDefensa = "E8";
    public const string EstadoDividirAtaque = "E9";
    public const string EstadoPosicionarseAtaque = "E10";
    public const string EstadoEntrarSala = "E11";
    public const string EstadoXogadorPerdidoAtaque = "E12";
    public const string EstadoPerseguirAtaque = "E13";
    public const string EstadoPerseguirPerseguidor = "E14";
    public const string EstadoXogadorPerdidoPerseguidor = "E15";

}
