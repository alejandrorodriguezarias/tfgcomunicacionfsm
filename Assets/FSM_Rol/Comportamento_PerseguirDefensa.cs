﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PerseguirDefensa : MonoBehaviour
{
    private Axente_FSM_Rol axente = null;
    private NavMeshAgent axenteNav;
    private RecorrerCamiñoRol axenteMov;
    //private List<string> nomesEquipo;
    private float tempoSenVerXogador = 2f;
    private const float MaximoSenVerXogador = 2f;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    private string destino { get; set; } = null;
    private string posicion { get; set; } = null;

    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiñoRol>();
        axente.camino = buscador.ObterCamino(gameObject);
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
        posicion = axente.destino;
        axenteNav.speed = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (tempoSenVerXogador < 0)
        {
            axente.destino = posicion;
            axente.DispararEvento((int)Eventos_Rol.EventoXogadorPerdidoDefensa);
        }
        else {
            tempoSenVerXogador -= Time.deltaTime;
        }
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar() {
        Destroy(this);
    }

    /// <summary>
    /// Recibe a posicion do xogador doutro axente
    /// </summary>
    /// <param name="posXogador">Vector3 Posicion do xogador</param>
    public void TratarInform(string contido, string remitente) {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("CoordXogador"))
        {
            tempoSenVerXogador = MaximoSenVerXogador;
            destino = contidoDiv[1];
            if (!axente.destino.Equals(destino))
            {
                axente.destino = destino;
                axente.camino = buscador.ObterCamino(gameObject);
                if (axente.camino == null)
                {
                    axenteNav.isStopped = true;
                }
                else {
                    axenteMov.ObterDestino();
                    axenteNav.isStopped = false;
                }
            } 
        }else if (contidoDiv[0].Equals("abandonarEquipo")){
            if (contidoDiv[1].Equals("defensas"))
            {
                axente.nomesEquipo = axente.defensas;
            }
        }
    }
    /// <summary>
    /// Actualiza a posicion do ladron e informa ao equipo 
    /// </summary>
    /// <param name="posXogador">Cela na que se encontra o xogador</param>
    /// <param name="habitacionXogador">nome da habitacion onde se encontra o xogador</param>
    public void DetectarXogador(Cela posXogador, string habitacionXogador) {
        tempoSenVerXogador = MaximoSenVerXogador;
        if (axente != null)
        {
            string[] celasDiv = posXogador.name.Split(',');
            destino = celasDiv[1] + ',' + celasDiv[2];
            //Se o ladron cambiou de posicion modificase o destino do axente
            if (!axente.destino.Equals(destino))
            {
                axente.destino = destino;
                axente.camino = buscador.ObterCamino(gameObject);
                if (axente.camino == null)
                {
                    axenteNav.isStopped = true;
                }
                else
                {
                    axenteMov.ObterDestino();
                    axenteNav.isStopped = false;
                }
            }
            //Se informa ao equipo sobrea a posicion do ladron
            Script_Comunicacion[] patrullas = FindObjectsOfType<Script_Comunicacion>();
            foreach (Script_Comunicacion patrulla in patrullas)
            {
                axente.EnviarMensaxe("inform", "CoordXogador\\" + celasDiv[1] + ',' + celasDiv[2], gameObject.name, patrulla.name, patrulla, "-1");
            }
        }
    }
}

public class Invocar_PerseguirDefensa : Comportamento
{
    GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_PerseguirDefensa>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PerseguirDefensa componente = gameObject.AddComponent<Comportamento_PerseguirDefensa>() as Comportamento_PerseguirDefensa;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        obxecto.GetComponent<Comportamento_PerseguirDefensa>().TratarInform(contido, remitente);
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        obxecto.GetComponent<Comportamento_PerseguirDefensa>().DetectarXogador(posXogador, habitacionXogador);
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        obxecto.GetComponent<Comportamento_PerseguirDefensa>().DetectarXogador(posXogador, habitacionXogador);
    }
}
