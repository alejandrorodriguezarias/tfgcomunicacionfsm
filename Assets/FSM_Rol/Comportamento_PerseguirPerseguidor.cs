﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PerseguirPerseguidor : MonoBehaviour
{
    private Axente_FSM_Rol axente = null;
    private NavMeshAgent axenteNav;
    private RecorrerCamiñoRol axenteMov;
    //private List<string> nomesEquipo;
    private float tempoSenVerXogador = 2f;
    private const float MaximoSenVerXogador = 2f;
    private bool enPosicion = true;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    private string destino { get; set; } = null;

// Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiñoRol>();
        axente.camino = buscador.ObterCamino(gameObject);
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
        axenteNav.speed = 5f;
        InformarEnPosicion();
    }

    // Update is called once per frame
    void Update()
    {
        if (tempoSenVerXogador < 0)
        {
            axente.DispararEvento((int)Eventos_Rol.EventoXogadorPerdido);
        }
        else {
            tempoSenVerXogador -= Time.deltaTime;
        }
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar() {
        Destroy(this);
    }

    private void InformarEnPosicion()
    {
        foreach (string nome in axente.nomesEquipo)
        {
            Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "enPosicion\\" + enPosicion, gameObject.name, nome, receptor, "-1");
        }
    }

    /// <summary>
    /// Recibe a posicion do xogador doutro axente
    /// </summary>
    /// <param name="posXogador">Vector3 Posicion do xogador</param>
    public void TratarInform(string contido, string remitente) {
        if (axente != null)
        {
            string[] contidoDiv = contido.Split('\\');
            if (contidoDiv[0].Equals("CoordXogador"))
            {
                tempoSenVerXogador = MaximoSenVerXogador;
                destino = contidoDiv[1];
                if (!axente.destino.Equals(destino))
                {
                    axente.destino = destino;
                    axente.camino = buscador.ObterCamino(gameObject);
                    if (axente.camino == null)
                    {
                        axenteNav.isStopped = true;
                    }
                    else
                    {
                        axenteMov.ObterDestino();
                        axenteNav.isStopped = false;
                    }
                }
            }
        }
    }
    /// <summary>
    /// Se ve ao xogador actualiza a sua posicion e informa a todo o equipo
    /// </summary>
    /// <param name="posXogador">Vector3 posicion do xogador</param>
    public void VerXogador(Cela posXogador, string habitacionXogador) {
        tempoSenVerXogador = MaximoSenVerXogador;
        if (axente != null) {
            string[] celasDiv = posXogador.name.Split(',');
            destino = celasDiv[1] + ',' + celasDiv[2];
            if (!axente.destino.Equals(destino)) {
                axente.destino = destino;
                axente.camino = buscador.ObterCamino(gameObject);
                if (axente.camino == null)
                {
                    axenteNav.isStopped = true;
                }
                else
                {
                    axenteMov.ObterDestino();
                    axenteNav.isStopped = false;
                }
            }
            foreach (string nome in axente.nomesEquipo)
            {
                Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                axente.EnviarMensaxe("inform", "CoordXogador\\" + celasDiv[1] + ',' + celasDiv[2], gameObject.name, nome, receptor, "-1");
            }
        }
    }
    //public void TratarRequest(string contido, string remitente, string id)
    //{
    //    if (contido.Equals("equipo"))
    //    {
    //        if (axente == null) {
    //            axente = gameObject.GetComponent<Axente_FSM_Celas>();
    //        }
    //        if (!axente.habitacionCorrecta)
    //        {
    //            axente.idConversaLider = id;
    //            axente.liderEquipo = remitente;
    //            axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
    //        }
    //    }
    //}

    public void EscoitarXogador(Cela posXogador, string habitacionXogador) {
        tempoSenVerXogador = MaximoSenVerXogador;
        if (axente != null)
        {
            string[] celasDiv = posXogador.name.Split(',');
            destino = celasDiv[1] + ',' + celasDiv[2];
            if (!axente.destino.Equals(destino))
            {
                axente.destino = destino;
                axente.camino = buscador.ObterCamino(gameObject);
                if (axente.camino == null)
                {
                    axenteNav.isStopped = true;
                }
                else
                {
                    axenteMov.ObterDestino();
                    axenteNav.isStopped = false;
                }
            }
            foreach (string nome in axente.nomesEquipo)
            {
                Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                axente.EnviarMensaxe("inform", "CoordXogador\\" + celasDiv[1] + ',' + celasDiv[2], gameObject.name, nome, receptor, "-1");
            }
        }
    }
}

public class Invocar_PerseguirPerseguidor : Comportamento
{
    GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_PerseguirPerseguidor>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PerseguirPerseguidor componente = gameObject.AddComponent<Comportamento_PerseguirPerseguidor>() as Comportamento_PerseguirPerseguidor;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PerseguirPerseguidor>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            //obxecto.GetComponent<Comportamento_PerseguirAtaque>().TratarRequest(contido, remitente, id);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PerseguirPerseguidor>().VerXogador(posXogador, habitacionXogador);
        }
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PerseguirPerseguidor>().EscoitarXogador(posXogador,habitacionXogador);
        }
    }
}
