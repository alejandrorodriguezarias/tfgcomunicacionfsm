﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PatrullarPortero : MonoBehaviour
{
    NavMeshAgent axenteNav;
    Axente_FSM_Rol axente;
    private List<string> puntos;
    private int puntoDestino;
    RecorrerCamiñoRol axenteMov;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();

    /// <summary>
    /// La función start se invoca antes del primer update
    /// Obtiene unha referencia ao NavMeshAngent e desactiva o autoBraking
    /// Obten os puntos de patrulla do axente
    /// </summary>
    void Start()
    {
        axenteNav = GetComponent<NavMeshAgent>();
        axente = GetComponent<Axente_FSM_Rol>();
        axenteMov = axente.GetComponent<RecorrerCamiñoRol>();
        //Desactivar autoBraking permite un movemento continuo entre puntos
        //GameObject botin = GameObject.Find("Botín");
        //if (botin != null)
        //{
        //    axente.rutaPatrulla = botin.GetComponent<Botín>().rutaPatrulla;
        //}
        //else
        //{

        //    axente.rutaPatrulla = axente.AdecuarRutaPatrulla(FindObjectOfType<Saida>().rutaPatrullaCruda);
        //}
        GameObject defensa = GameObject.Find(axente.destino);
        if (defensa != null)
        {
            Botín botin = defensa.GetComponent<Botín>();
            if (botin != null)
            {
                axente.rutaPatrulla = botin.rutaPatrulla;
            }
            else {
                Saida saida = defensa.GetComponent<Saida>();
                axente.rutaPatrulla = axente.AdecuarRutaPatrulla(saida.rutaPatrullaCruda);
            } 
        }
        axenteNav.autoBraking = false;
        puntos = axente.rutaPatrulla;
        puntoDestino = 0;
        IrPuntoSeguinte();
    }
    /// <summary>
    /// Este metodo encargase de obter a proxima coordenada da patrulla e ordenar ao axente moverse ata ese punto
    /// </summary>
    /// <param name="inicio">Se estamos no inicio o puntoDestino é 0 pero non rematou a patrulla</param>
    void IrPuntoSeguinte()
    {
        // Se non existen puntos devolvemos unha excepcion
        if (puntos.Count == 0)
        {
            throw new Exception("É necesario ter algun punto de patrulla");
        }
        //O destino do axente e o proximo punto
        axente.destino = puntos[puntoDestino];
        puntoDestino = (puntoDestino + 1) % puntos.Count;
        // Elixe o seguinte punto no array,
        // Regresa o inicio do array se e necesario
        List<string> camiño = buscador.ObterCamino(gameObject);
        axente.camino = camiño;
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
    }
    /// <summary>
    /// Borra este script do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    void Update()
    {
        //Cando se chega ao punto elixese o seguinte.
        if (axenteMov.finCamino)
        {
            IrPuntoSeguinte();
        }
    }
    /// <summary>
    /// Recibe os mensaxes request neste comportamento, se é un request para unirse a un equipo salta o evento
    /// </summary>
    /// <param name="contido">Contido da mensaxe request</param>
    public void TratarRequest(string contido) {
        if (contido.Equals("equipo"))
        {
            axente.DispararEvento((int)Eventos_Rol.EventoFormarEquipo);
        }
    }
}

public class Invocar_PatrullarPortero : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_PatrullarPortero>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PatrullarPortero componente = gameObject.AddComponent<Comportamento_PatrullarPortero>() as Comportamento_PatrullarPortero;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        obxecto.GetComponent<Comportamento_PatrullarRol>().TratarRequest(contido);
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
    }
}

