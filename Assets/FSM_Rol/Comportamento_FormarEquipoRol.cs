﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comportamento_FormarEquipoRol: MonoBehaviour
{

    //Axente propietario do script
    private Axente_FSM_Rol axente;
    private bool solicitudesEnviadas = false;
    //lista dos axentes que se unen ao equipo
    private List<string> nomesEquipo = new List<string>();
    private const float TempoReenvio = 0.8f;
    private float tempoUltimaComprobacion = TempoReenvio;
    private Dictionary<string, bool> equipo = new Dictionary<string, bool>();

    /// <summary>
    /// Obtemos o axente propietario do script e comeza o seu comportamento
    /// </summary>
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axente.nomesEquipo = new List<string>();
        //axente.EstablecerConversas(new List<Conversa>());
        nomesEquipo.Add(gameObject.name);
        SolicitarEquipo();
    }

    /// <summary>
    /// Cada frame comproba se xa se enviaron as mensaxes e se foron respondidas
    /// </summary>
    void Update()
    {
        if (solicitudesEnviadas)
        {
            if (ComprobarRespostas())
            {
                solicitudesEnviadas = false;
                axente.nomesEquipo = nomesEquipo;
                axente.nomesEquipoCompleto = nomesEquipo;
                InformarEquipo();
                axente.DispararEvento((int)Eventos_Rol.EventoEquipoFormado);
            }
            else if (tempoUltimaComprobacion < 0)
            {
                tempoUltimaComprobacion = TempoReenvio;
                SolicitarEquipo();
            }
            else
            {
                tempoUltimaComprobacion -= Time.deltaTime;
            }
        }
    }


    /// <summary>
    /// Pregunta a todos os axentes quen esta dispoñible para o equipo
    /// </summary>
    private void SolicitarEquipo()
    {
        Script_Comunicacion[] patrullas = FindObjectsOfType<Script_Comunicacion>();
        foreach (Script_Comunicacion patrulla in patrullas)
        {
            //Non queremos que se envie o mensaxe a el mesmo
            if (!gameObject.name.Equals(patrulla.name))
            {
                axente.EnviarMensaxe("request", "equipo", gameObject.name, patrulla.name, patrulla, "-1");
            }
        }
        solicitudesEnviadas = true;
    }
    /// <summary>
    /// Os axentes que acepten a invitación rexistranse no equipo
    /// </summary>
    /// <param name="remitente">o nome do axente que acepta a invitacion</param>
    public void TratarAccept(string remitente)
    {
        try
        {
            equipo.Add(remitente, true);
            nomesEquipo.Add(remitente);
        }
        catch (ArgumentException)
        {
        }
    }

    /// <summary>
    /// Os axentes que rexeiten a invitación non se rexistran no equipo
    /// </summary>
    /// <param name="remitente">o nome do axente que rexeita a invitacion</param>
    public void TratarRefuse(string remitente)
    {
        try
        {
            equipo.Add(remitente, false);
        }
        catch (ArgumentException)
        {
        }
    }

    /// <summary>
    /// Comproba se todos os axentes responderon á mensaxe
    /// </summary>
    /// <returns>True en caso afirmativo e false en caso negativo</returns>
    private bool ComprobarRespostas()
    {
        bool respostasRecibidas = false;

        Script_Comunicacion[] patrullas = FindObjectsOfType<Script_Comunicacion>();
        if (equipo.Count == (patrullas.Length-1)) {
            respostasRecibidas = true;
        }
        return respostasRecibidas;
    }
    /// <summary>
    /// Envia unha mensaxe a todos os membros do equipo coa lista dos membros do equipo
    /// </summary>
    private void InformarEquipo()
    {
        Script_Comunicacion receptor;
        string mensaxe = "equipo" + "\\";
        foreach (string nome in nomesEquipo)
        {
            mensaxe = mensaxe + nome + ",";
        }
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", mensaxe, gameObject.name, nome, receptor, "-1");
        }
    }


    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        //axente.DebugEquipo();
        Destroy(this);
    }
}
public class Invocar_FormarEquipoRol : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_FormarEquipoRol>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_FormarEquipoRol componente = gameObject.AddComponent<Comportamento_FormarEquipoRol>() as Comportamento_FormarEquipoRol;
    }

    public override void TratarAccept(string remitente, string id)
    {
        obxecto.GetComponent<Comportamento_FormarEquipoRol>().TratarAccept(remitente);
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        obxecto.GetComponent<Comportamento_FormarEquipoRol>().TratarRefuse(remitente);
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
    }
}