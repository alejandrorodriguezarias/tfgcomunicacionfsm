﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_DividirRol : MonoBehaviour
{
    NavMeshAgent axenteNav;
    Axente_FSM_Rol axente;
    RecorrerCamiñoRol axenteMov;
    private BusquedaCamiñoRol buscador = new BusquedaCamiñoRol();
    List<string> nomesEquipo;
    //Fluxo de mensaxes
    private float tempoComprobacions;
    private const float TempoEntreComprobacions = 0.1f;
    //Flags control
    bool comprobarMensaxes { get; set; } = false;
    bool calcularDistancias { get; set; } = false;
    bool manexarDistancias { get; set; } = false;
    bool portero { get; set; } = true;
    bool atacante { get; set; } = false;
    bool perseguidor { get; set; } = false;
    bool defensa { get; set; } = false;
    bool final { get; set; } = false;
    //Distancias
    List<Transform> entradas;
    Transform entradaActual;
    Transform centroHabitacionXogador;
    int iteradorDistancias = 0;
    int membrosSinAsignar;
    private Dictionary<string, float> distancias = new Dictionary<string, float>();
    private Dictionary<string, Dictionary<string, float>> distanciasEquipo = new Dictionary<string, Dictionary<string, float>>();
    //Equipos
    
    List<string> porteros = new List<string>();
    List<string> defensas = new List<string>();
    List<string> atacantes = new List<string>();
    List<string> perseguidores = new List<string>();
    string rol;


    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Rol>();
        axenteNav = axente.GetComponent<NavMeshAgent>();
        axente.nomesEquipo = axente.nomesEquipoCompleto;
        nomesEquipo = new List<string>(axente.nomesEquipo);
        membrosSinAsignar = nomesEquipo.Count;
        tempoComprobacions = TempoEntreComprobacions;
        entradas = ObterEntradas();
        ComprobarEquipo();
    }

    void Update()
    {
        //Comproba se todo o equipo encontrase neste estado
        if (comprobarMensaxes)
        {
            if (tempoComprobacions <= 0)
            {
                //Se estan no mesmo estado comezan a calcular as distancias
                if (MesmoEstado())
                {
                    comprobarMensaxes = false;
                    tempoComprobacions = TempoEntreComprobacions;
                    calcularDistancias = true;

                }
                //Se non estan no mesmo estado esperan e volven a comprobar
                else
                {
                    comprobarMensaxes = false;
                    tempoComprobacions = TempoEntreComprobacions;
                    LimparConversas();
                    ComprobarEquipo();
                }
            }
            else
            {
                tempoComprobacions -= Time.deltaTime;
            }
        }
        if (calcularDistancias)
        {
            entradaActual = entradas[iteradorDistancias];
            axenteNav.isStopped = true;
            axenteNav.destination = entradaActual.position;
            if (!axenteNav.pathPending)
            {
                distancias.Add(entradaActual.gameObject.name, Vector3.Distance(gameObject.transform.position, entradaActual.position));
                if (iteradorDistancias < entradas.Count - 1)
                {
                    iteradorDistancias += 1;
                }
                else
                {
                    calcularDistancias = false;
                    iteradorDistancias = 0;
                    InformarSobreDistancias();
                }
            }
        }
        if (manexarDistancias)
        {
            if (tempoComprobacions <= 0)
            {
                manexarDistancias = false;
                PedirDestino();
                tempoComprobacions = TempoEntreComprobacions;
            }
            else
            {
                tempoComprobacions -= Time.deltaTime;
            }
        }
        if (final)
        {
            if (tempoComprobacions <= 0)
            {
                if (porteros.Count + defensas.Count + perseguidores.Count + atacantes.Count == nomesEquipo.Count) {
                    AsumirRol();
                    final = false;
                }
            }
            else
            {
                tempoComprobacions -= Time.deltaTime;
            }

        }
    }

    /// <summary>
    /// Selecciona o mellor destino posible para o axente (o mais curto) e o comunica ao resto do equipo
    /// </summary>
    public void PedirDestino()
    {
        if (distanciasEquipo.Count != membrosSinAsignar - 1){
            manexarDistancias = true;
            return;
        }
        ActualizarDistancias();
        bool menor = true;
        if (portero)
        {
            List<Transform> obxectivos = axente.obxectivos;
            //Se hay tantos porteros como obxectivos cambiase de rol a dividir
            if (porteros.Count == obxectivos.Count)
            {
                portero = false;
                perseguidor = true;
                manexarDistancias = true;
                return;
            }
            //Para cada obxectivo
            foreach (Transform obxectivo in obxectivos)
            {
                try
                {
                    menor = true;
                    float distancia = distancias[obxectivo.name];
                    //Calculamos se este é o axente maís cercano
                    foreach (KeyValuePair<string, Dictionary<string, float>> axente in distanciasEquipo)
                    {
                        if (distancia > axente.Value[obxectivo.name])
                        {
                            manexarDistancias = true;
                            menor = false;
                        }
                    }
                    if (menor)
                    {
                        //Informase ao equipo da eleccion
                        foreach (string nome in nomesEquipo)
                        {
                            Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                            axente.EnviarMensaxe("inform", "eleccion\\" + obxectivo.name + "\\" + "portero", gameObject.name, nome, receptor, "-1");
                        }
                        portero = false;
                        porteros.Add(gameObject.name);
                        rol = "portero";
                        manexarDistancias = false;
                        final = true;
                        return;
                    }
                }
                catch (KeyNotFoundException)
                {
                }
            }
        }
        else if (perseguidor)
        {
            if (perseguidores.Count != 0)
            {
                perseguidor = false;
                atacante = true;
                manexarDistancias = true;
                return;
            }
            menor = true;
            float distancia = distancias[centroHabitacionXogador.name];
            //Calculamos se este é o axente maís cercano
            foreach (KeyValuePair<string, Dictionary<string, float>> FSM in distanciasEquipo)
            {
                if (distancia > FSM.Value[centroHabitacionXogador.name])
                {
                    manexarDistancias = true;
                    menor = false;
                }
            }
            if (menor)
            {
                //Informase ao equipo da eleccion
                foreach (string nome in nomesEquipo)
                {
                    Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                    axente.EnviarMensaxe("inform", "eleccion\\" + centroHabitacionXogador.name + "\\" + "perseguidor", gameObject.name, nome, receptor, "-1");
                }
                perseguidor = false;
                perseguidores.Add(gameObject.name);
                rol = "perseguidor";
                manexarDistancias = false;
                final = true;
                return;
            }
        }
        else if (atacante)
        {
            if (atacantes.Count >= ((nomesEquipo.Count - perseguidores.Count - porteros.Count) / 2))
            {
                atacante = false;
                defensa = true;
                manexarDistancias = true;
                return;
            }
            menor = true;
            float distancia = distancias[centroHabitacionXogador.name];
            //Calculamos se este é o axente maís cercano
            foreach (KeyValuePair<string, Dictionary<string, float>> FSM in distanciasEquipo)
            {
                if (distancia > FSM.Value[centroHabitacionXogador.name])
                {
                    manexarDistancias = true;
                    menor = false;
                }
                
            }
            if (menor)
            {
                //Informase ao equipo da eleccion
                foreach (string nome in nomesEquipo)
                {
                    Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                    axente.EnviarMensaxe("inform", "eleccion\\" + centroHabitacionXogador.name + "\\" + "atacante", gameObject.name, nome, receptor, "-1");
                }
                atacante = false;
                atacantes.Add(gameObject.name);
                rol = "atacante";
                manexarDistancias = false;
                final = true;
                return;
            }
        }else if(defensa){
            //Informase ao equipo da eleccion
            foreach (string nome in nomesEquipo)
            {
                Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                axente.EnviarMensaxe("inform", "eleccion\\" + centroHabitacionXogador.name + "\\" + "defensa", gameObject.name, nome, receptor, "-1");
            }
            defensa = false;
            defensas.Add(gameObject.name);
            rol = "defensa";
            manexarDistancias = false;
            final = true;
        }
    }
    /// <summary>
    /// Pregunta ao resto dos axentes do equipo se estan no estado planificar
    /// </summary>
    private void ComprobarEquipo()
    {
        Script_Comunicacion receptor;
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("query", "estadoDividirRol", gameObject.name, nome, receptor, "-1");
        }
        comprobarMensaxes = true;
    }
    /// <summary>
    /// Comportamento cando recibese unha mensaxe query, se esta e unha pregunta por estado planificar respondese true
    /// </summary>
    /// <param name="contido">contido da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    public void TratarQuery(string contido, string remitente, string id)
    {

        if (contido.Equals("estadoDividirRol"))
        {
            Script_Comunicacion receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            if (axente != null)
            {
                axente.EnviarMensaxe("inform", "estadoDividirRol\\true", gameObject.name, remitente, receptor, id);
            }

        }
    }

    /// <summary>
    /// Comproba se todos os axentes responderon ao mensaxe do estado recorrendo as conversas pendentes
    /// </summary>
    /// <returns>true se todos os axentes estan no estado planificar, false noutro caso</returns>
    private bool MesmoEstado()
    {
        bool mesmoEstado = true;
        List<Conversa> listaConversas = axente.ObterConversas();
        foreach (Conversa conversa in listaConversas)
        {
            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar"))
            {
                mesmoEstado = false;
                break;
            }
        }
        return mesmoEstado;
    }
    /// <summary>
    /// Antes de reenviar as preguntas do estado limpanse das conversas as que quedaran colgando
    /// </summary>
    private void LimparConversas()
    {
        List<Conversa> listaConversasAux = axente.ObterConversas();
        List<Conversa> listaConversas = new List<Conversa>(axente.ObterConversas());
        foreach (Conversa conversa in listaConversasAux)
        {
            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar"))
            {
                listaConversas.Remove(conversa);
            }
        }
        axente.EstablecerConversas(listaConversas);
    }
    /// <summary>
    /// Obtense os transform necesarios para calcular a distancias que permiten planificar a reparticion de roles
    /// </summary>
    /// <returns>List<Transforms> os diferentes puntos criticos</returns>
    private List<Transform> ObterEntradas() {

        List<Transform> entradas = new List<Transform>();
        List<Transform> obxectivos = axente.obxectivos;
        //Obtemos as entradas dos obxectivos
        foreach(Transform obxectivo in obxectivos) {
            entradas.Add(obxectivo);
        }
        centroHabitacionXogador = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().centro.transform;
        entradas.Add(centroHabitacionXogador);

        return entradas;
    }

    /// <summary>
    /// Comunica ao resto do equipo a sua posicion respecto as entradas
    /// </summary>
    private void InformarSobreDistancias()
    {
        Script_Comunicacion receptor;
        string contido = "distancias";
        foreach (KeyValuePair<string, float> distancia in distancias)
        {
            contido += "\\" + distancia.Key + "\\" + distancia.Value;
        }
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", contido, gameObject.name, nome, receptor, "-1");
        }
        manexarDistancias = true;
    }

    /// <summary>
    /// Se os axentes informan das suas distancias almacenanse, se un axente solicita unha entrada a eliminamos das posiblidades deste axente
    /// </summary>
    /// <param name="contido">contido da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    public void TratarInform(string contido, string remitente)
    {
        Dictionary<string, float> distancias = new Dictionary<string, float>();
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("distancias"))
        {
            for (int i = 1; i < contidoDiv.Length; i += 2)
            {
                distancias.Add(contidoDiv[i], float.Parse(contidoDiv[i + 1]));
            }
            distanciasEquipo.Add(remitente, distancias);
        }
        else if (contidoDiv[0].Equals("eleccion"))
        {
            distanciasEquipo.Remove(remitente);
            membrosSinAsignar--;
            if (!centroHabitacionXogador.name.Equals(contidoDiv[1])) {
                foreach (Transform entrada in entradas)
                {
                    if (entrada.name.Equals(contidoDiv[1]))
                    {
                        entradas.Remove(entrada);
                        break;
                    }
                }
            }
            switch (contidoDiv[2])
            {
                case "portero":
                    porteros.Add(remitente);
                    break;
                case "defensa":
                    defensas.Add(remitente);
                    break;
                case "atacante":
                    atacantes.Add(remitente);
                    break;
                case "perseguidor":
                    perseguidores.Add(remitente);
                    break;
            }
        }
        else if (contidoDiv[0].Equals("SalirEquipo"))
        {
            axente.nomesEquipo.Remove(remitente);
        }
    }

    /// <summary>
    /// Actualiza as distancias do axente eliminando aqueles puntos que foron seleccionados por outro axente
    /// </summary>
    public void ActualizarDistancias()
    {
        bool borrar;
        Dictionary<string, float> distanciasAux = new Dictionary<string, float>(distancias);
        if (distancias.Count != entradas.Count)
        {
            foreach (KeyValuePair<string, float> distancia in distanciasAux)
            {
                borrar = true;
                foreach (Transform entrada in entradas)
                {
                    if (distancia.Key.Equals(entrada.name))
                    {
                        borrar = false;
                    }
                }
                if (borrar)
                {
                    distancias.Remove(distancia.Key);
                }
            }
        }
    }
    /// <summary>
    /// Produce un evento diferente segundo o rol que corresponda a este axente
    /// </summary>
    public void AsumirRol() {
        axente.porteros = porteros;
        axente.defensas = defensas;
        axente.atacantes = atacantes;
        axente.perseguidores = perseguidores;
        switch (rol)
        {
            case "portero":
                Portero();
                break;
            case "defensa":
                Defensa();
                break;
            case "atacante":
                Atacante();
                break;
            case "perseguidor":
                Perseguidor();
                break;
        }
    }
    /// <summary>
    /// Cambia o color de axente e dispara o evento de rol portero
    /// </summary>
    public void Portero() {
        axente.nomesEquipo = porteros;
        gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = new Color(255, 255, 255);
        gameObject.transform.GetChild(1).gameObject.GetComponent<Renderer>().material.color = new Color(255, 255, 255);
        //gameObject.transform.GetChild(2).gameObject.GetComponent<Renderer>().material.color = new Color(255, 255, 255);
        axente.DispararEvento((int)Eventos_Rol.EventoRolPortero);
    }
    /// <summary>
    /// Cambia o color de axente e dispara o evento de rol defensa
    /// </summary>
    public void Defensa()
    {
        axente.nomesEquipo = defensas;
        gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 255);
        gameObject.transform.GetChild(1).gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 255);
        //gameObject.transform.GetChild(2).gameObject.GetComponent<Renderer>().material.color = new Color(255, 255, 255);
        axente.DispararEvento((int)Eventos_Rol.EventoRolDefensa);
    }
    /// <summary>
    /// Cambia o color de axente e dispara o evento de rol atacante
    /// </summary>
    public void Atacante()
    {
        axente.nomesEquipo = atacantes;
        gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
        gameObject.transform.GetChild(1).gameObject.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
        //gameObject.transform.GetChild(2).gameObject.GetComponent<Renderer>().material.color = new Color(255, 255, 255);
        axente.DispararEvento((int)Eventos_Rol.EventoRolAtacante);
    }
    /// <summary>
    /// Cambia o color de axente e dispara o evento de rol perseguidor
    /// </summary>
    public void Perseguidor()
    {
        axente.nomesEquipo = perseguidores;
        gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
        gameObject.transform.GetChild(1).gameObject.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
        //gameObject.transform.GetChild(2).gameObject.GetComponent<Renderer>().material.color = new Color(255, 255, 255);
        axente.DispararEvento((int)Eventos_Rol.EventoRolPerseguidor);
    }

    /// <summary>
    /// Borra este script do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }
}

public class Invocar_DividirRol : Comportamento
{
    private GameObject obxecto;

    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_DividirRol>().Eliminar();
    }

    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_DividirRol componente = gameObject.AddComponent<Comportamento_DividirRol>() as Comportamento_DividirRol;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_DividirRol>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_DividirRol>().TratarQuery(contido, remitente, id);
        }
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
    }
}

