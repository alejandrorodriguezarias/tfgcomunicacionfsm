﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escoitar_Rol : Escoitar_Sensor
{

    public override void Escoitar(Cela cela, string habitacionXogador)
    {
        GetComponent<Axente_FSM_Rol>().EscoitarXogador(cela, habitacionXogador);
    }
}
