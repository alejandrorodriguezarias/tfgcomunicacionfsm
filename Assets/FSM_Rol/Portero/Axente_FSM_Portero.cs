﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axente_FSM_Portero : FSM
{
    //Movilidad
    public string destino { get; set; } = "";
    public List<string> camino { get; set; }
    public Cela celaActual { get; set; }
    //Patrulla
    public List<Cela> rutaBotínCruda;
    public List<Cela> rutaSaidaCruda;
    public List<string> rutaPatrulla { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        RexistrarEstados();
        if (GameObject.Find("Botín") != null)
        {
            rutaPatrulla = AdecuarRutaPatrulla(rutaBotínCruda);
        }
        else {
            rutaPatrulla = AdecuarRutaPatrulla(rutaSaidaCruda);
        }
       
        Comportamento estado = ObterEstadoInicial();
        nomeEstadoActual = Estados_Rol.EstadoPatrullar;
        estado.Inicio(gameObject);
        //RexistrarTransicions();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Rexistra todos os estados do axente no seu diccionario. Chamase durante a inicializacion do axente
    /// </summary>
    private void RexistrarEstados()
    {
        RexistrarEstadoInicial(new Invocar_PatrullarPortero(), Estados_Rol.EstadoPatrullar);
    }
    /// <summary>
    /// Converte a lista de celas nunha lista de strings manexable por RecorrerCamiño
    /// </summary>
    public List<string> AdecuarRutaPatrulla(List<Cela> rutaPatrullaCruda)
    {
        List<string> rutaAux = new List<string>();
        string[] puntoDiv;
        string puntoAdecuado;
        foreach (Cela punto in rutaPatrullaCruda)
        {
            puntoDiv = punto.gameObject.name.Split(',');
            puntoAdecuado = puntoDiv[1] + ',' + puntoDiv[2];
            rutaAux.Add(puntoAdecuado);
        }
        return rutaAux;
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject obxectoColision = collision.collider.gameObject;

        if (obxectoColision.tag.Equals("cela"))
        {
            if (celaActual != null)
            {
                if (!obxectoColision.transform.gameObject.name.Equals(celaActual.gameObject.name))
                {
                    celaActual = obxectoColision.transform.gameObject.GetComponent<Cela>();
                }
            }
            else
            {
                celaActual = obxectoColision.transform.gameObject.GetComponent<Cela>();
            }

        }
    }

    public void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        Script_Comunicacion[] patrullas = FindObjectsOfType<Script_Comunicacion>();
        foreach (Script_Comunicacion patrulla in patrullas)
        {
            //Non queremos que se envie o mensaxe a el mesmo
            if (!gameObject.name.Equals(patrulla.name))
            {
                EnviarMensaxe("inform", "habitacionXogador\\" + habitacionXogador, gameObject.name, patrulla.name, patrulla, "-1");
            }
        }
        DispararEvento((int)Eventos_Rol.EventoVerXogador);
    }

    public void VerXogador(Cela posXogador, string habitacionXogador)
    {

        Script_Comunicacion[] patrullas = FindObjectsOfType<Script_Comunicacion>();
        foreach (Script_Comunicacion patrulla in patrullas)
        {
            //Non queremos que se envie o mensaxe a el mesmo
            if (!gameObject.name.Equals(patrulla.name))
            {
                EnviarMensaxe("inform", "habitacionXogador\\" + habitacionXogador, gameObject.name, patrulla.name, patrulla, "-1");
            }
        }
        DispararEvento((int)Eventos_Rol.EventoVerXogador);
    }

    protected override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    protected override void TratarInform(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    protected override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    protected override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    protected override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }
}
