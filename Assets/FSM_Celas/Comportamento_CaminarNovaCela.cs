﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_CaminarNovaCela : MonoBehaviour
{

    Axente_FSM_Celas axente;
    private Cela proximaCela;
    int i=0;
    List<string> caminos;
    public float velocidade = 0.04f;
    NavMeshAgent axenteNav;
    private const float RetrasoPortaPechada = 5f;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Celas>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        caminos = axente.camino;
        axenteNav.destination = gameObject.transform.position;
        proximaCela = GameObject.Find(caminos[i]).GetComponent<Cela>();
        i++;
    }

    // Update is called once per frame
    void Update()
    {
        if (axenteNav.remainingDistance < 1)
        {
            axente.celaActual = proximaCela;
            if (i < caminos.Count)
            {
                proximaCela.ocupada = false;
                proximaCela = GameObject.Find(caminos[i]).GetComponent<Cela>();
                if (!proximaCela.ocupada)
                {
                    proximaCela.ocupada = true;
                    //Debug.Log("proximaCela: " + proximaCela.transform.position);
                    axenteNav.destination = proximaCela.transform.position;
                    axenteNav.isStopped = false;
                    if (proximaCela.EPorta())
                    {
                        axenteNav.isStopped = true;
                        Script_Porta porta = proximaCela.transform.GetChild(0).gameObject.GetComponent<Script_Porta>();
                        if (porta.EstaPechada())
                        {
                            porta.Abrir();
                            StartCoroutine("EsperarPorta");
                        }
                        else
                        {
                            porta.Reinicio();
                            axenteNav.isStopped = false;
                        }
                    }
                    i++;
                }
                else {
                    axente.DispararEvento((int)Eventos_Celas.EventoCaminoBloqueado);
                }
            }   
            else
            {
                if (axente.buscandoPosicion)
                {
                    axente.buscandoPosicion = false;
                    gameObject.transform.LookAt(GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro().gameObject.transform);
                    axenteNav.isStopped = true;
                    axente.DispararEvento((int)Eventos_Celas.EventoEnPosicion);
                }
                else {
                    axente.DispararEvento((int)Eventos_Celas.EventoMovementoCompletado);
                }
                    
                }
            }
    }
    IEnumerator EsperarPorta() {
        yield return new WaitForSeconds(0.5f);
        axenteNav.isStopped = false;
    }
    private void Mover() {
        float movementoX = 0;
        float movementoZ = 0;
        if ((gameObject.transform.position.x - proximaCela.transform.position.x) > 0)
        {
            movementoX = -velocidade;
        }
        else if ((gameObject.transform.position.x - proximaCela.transform.position.x) < 0)
        {
            movementoX = velocidade;
        }
        if ((gameObject.transform.position.z - proximaCela.transform.position.z) > 0)
        {
            movementoZ = -velocidade;
        }
        else if ((gameObject.transform.position.z - proximaCela.transform.position.z) < 0)
        {
            movementoZ = velocidade;
        }
        Vector3 movemento = new Vector3(movementoX, 0.0f, movementoZ);
        transform.Translate(movemento, Space.World);
        Vector3 vista = new Vector3(proximaCela.transform.position.x, gameObject.transform.position.y, proximaCela.transform.position.z);
        
        if (Math.Abs(gameObject.transform.position.x - proximaCela.transform.position.x) > 0.2 && Math.Abs(gameObject.transform.position.z - proximaCela.transform.position.z) > 0.2)
        {
            transform.LookAt(vista);

        }
        //transform.rotation.y = 0;
    }
    /// <summary>
    /// Borra este script do axente
    /// </summary>
    public void Eliminar()
    {
        proximaCela.ocupada = false;
        Destroy(this);
    }
}

public class Invocar_CaminarNovaCela : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_CaminarNovaCela>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_CaminarNovaCela componente = gameObject.AddComponent<Comportamento_CaminarNovaCela>() as Comportamento_CaminarNovaCela;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        Axente_FSM_Celas axente;
        if (contido.Equals("equipo"))
        {
            axente = obxecto.GetComponent<Axente_FSM_Celas>();
            axente.idConversaLider = id;
            axente.liderEquipo = remitente;
            axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}
