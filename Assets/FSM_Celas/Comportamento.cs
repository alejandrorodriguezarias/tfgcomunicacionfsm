﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Comportamento{

    abstract public void Inicio(GameObject gameObject);
    abstract public void Ejecutar();
    abstract public void Fin(GameObject gameObject);
    abstract public void TratarAccept(string remitente, string id);
    abstract public void TratarInform(string contido, string remitente, string id);
    abstract public void TratarQuery(string contido, string remitente, string id);
    abstract public void TratarRefuse(string remitente, string id);
    abstract public void TratarRequest(string contido, string remitente, string id);
    abstract public void VerXogador(Cela posXogador, string habitacionXogador);
    abstract public void EscoitarXogador(Cela posXogador, string habitacionXogador);
}
