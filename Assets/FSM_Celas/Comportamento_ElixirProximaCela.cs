﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comportamento_ElixirProximaCela : MonoBehaviour
{
    private string destino;
    private int destinoXCoord;
    private int destinoYCoord;
    Axente_FSM_Celas axente;
    Cela celaActual;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Celas>();
        destino = axente.ObterDestino();
        string[] destinoDiv = destino.Split(',');
        destinoXCoord = int.Parse(destinoDiv[0]);
        destinoYCoord = int.Parse(destinoDiv[1]);
        celaActual = axente.ObterCelaActual();
        if (celaActual.XCoord == destinoXCoord && celaActual.YCoord == destinoYCoord)
        {
            Debug.Log("Fin");
        }
        else {
            if (BusquedaA()) {
                axente.DispararEvento((int)Eventos_Celas.EventoCaminoObtido);
            }
            //IniciarBusquedaVoraz(); 
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void IniciarBusquedaVoraz()
    {
        Dictionary<string, int> abertos = new Dictionary<string, int>(celaActual.ObterTransicions());
        while (abertos.Count != 0)
        {
            Dictionary<string, int> heuristica = ObterHeuristica(abertos);
            List<string> cerrados = new List<string>();
            int menorPeso = -1;
            string mellorCela = null;
            foreach (KeyValuePair<string, int> transicion in heuristica)
            {
                if (menorPeso == -1 || menorPeso > transicion.Value)
                {
                    menorPeso = transicion.Value;
                    mellorCela = transicion.Key;
                }
            }
            if (mellorCela != null)
            {
                cerrados.Add(mellorCela);
                abertos.Remove(mellorCela);
                if (ComprobarMeta(mellorCela))
                {
                    axente.EstablecerProximaCela(GameObject.Find(mellorCela).GetComponent<Cela>());
                    axente.DispararEvento((int)Eventos.EventoEquipoEnPosicion);
                    break;
                }
                else
                {
                    if (BusquedaAvara(mellorCela, new Dictionary<string, int>(GameObject.Find(mellorCela).GetComponent<Cela>().ObterTransicions()))) {
                        axente.DispararEvento((int)Eventos_Celas.EventoCaminoObtido);
                        break;
                    }
                }
            }
            else {
                throw new System.NotImplementedException();
            }
        }
    }

    public bool BusquedaA()
    {
        bool caminoIncompleto = true;
        ColaPrioridad frontera = new ColaPrioridad();
        bool estadoExplorado = false;
        List<NodoPrioridad> explorados= new List<NodoPrioridad>();
        NodoPrioridad nodo;
        nodo = new NodoPrioridad(celaActual.gameObject.name, 0, ObterHeuristicaA(celaActual.gameObject.name), null);
        NodoPrioridad nodoExpl;
        frontera.Insertar(nodo);
        while (caminoIncompleto)
        {
            if (frontera.Tamaño() == 0)
            {
                return false;
            }
            nodo = frontera.ObterNodo();
            nodoExpl = nodo;
            explorados.Add(nodoExpl);
            if (ComprobarMeta(nodo.nomeCela))
            {
                caminoIncompleto = false;
                axente.EstablecerCamino(ConstruirCaminoA(nodo,explorados));
                return true;
            }
            Dictionary<string, int> novos = new Dictionary<string, int>(GameObject.Find(nodo.nomeCela).GetComponent<Cela>().ObterTransicions());
            foreach (KeyValuePair<string, int> novo in novos)
            {
                estadoExplorado = false;
                foreach (NodoPrioridad explorado in explorados)
                {
                    if (explorado.nomeCela.Equals(novo.Key))
                    {
                        estadoExplorado = true;
                    }
                }
                if (!estadoExplorado) {
                    Cela tmp = GameObject.Find(novo.Key).gameObject.GetComponent<Cela>();
                    if (!tmp.ocupada) {
                        nodo = new NodoPrioridad(novo.Key, nodoExpl.costeReal + novo.Value, ObterHeuristicaA(novo.Key), nodoExpl.nomeCela);
                        frontera.Insertar(nodo);
                    }
                    
                }
            }
        }
        return false;
    }
    public List<string> ConstruirCaminoA(NodoPrioridad celaFinal, List<NodoPrioridad> explorados)
    {
        bool construir = true;
        List<string> camino = new List<string>();
        string iterador = celaFinal.nomeCela;
        while (construir)
        {
            camino.Add(iterador);
            iterador = EncontrarPadre(explorados,iterador);
            if (iterador.Equals(celaActual.gameObject.name))
            {
                camino.Add(iterador);
                construir = false;
            }
        }
        camino.Reverse();
        //foreach (string cela in camino)
        //{
        //    Debug.Log("Camino: " + cela);
        //}
        return camino;

    }
    private string EncontrarPadre(List<NodoPrioridad> explorados, string hijo) {

        foreach (NodoPrioridad nodo in explorados) {
            if (nodo.nomeCela.Equals(hijo)) {
                return nodo.padre;
            }
        }
        return null;
    }
    private int ObterHeuristicaA(string nodo)
    {
        int euclidea = -1;
        string[] transicionDiv = nodo.Split(',');
        int XCoord = int.Parse(transicionDiv[1]);
        int YCoord = int.Parse(transicionDiv[2]);
        int distancia = (int)Math.Floor(Math.Sqrt(Math.Pow(destinoXCoord - XCoord, 2) + Math.Pow(destinoYCoord - YCoord, 2)));
        euclidea = distancia;
        return euclidea;
    }

    private Dictionary<string, int> ObterHeuristica(Dictionary<string, int> abertos)
    {
        Dictionary<string, int> euclidea = new Dictionary<string, int>();
        foreach (KeyValuePair<string, int> transicion in abertos)
        {
            string[] transicionDiv = transicion.Key.Split(',');
            int XCoord = int.Parse(transicionDiv[1]);
            int YCoord = int.Parse(transicionDiv[2]);
            int distancia = (int)Math.Floor(Math.Sqrt(Math.Pow(destinoXCoord - XCoord, 2) + Math.Pow(destinoYCoord - YCoord, 2))) + transicion.Value;
            euclidea.Add(transicion.Key, distancia);

        }
        return euclidea;
    }
    private bool BusquedaAvara(string celaInicial, Dictionary<string, int> abertos)
    {

        List<string> cerrados = new List<string>();
        Dictionary<string, string> camino = new Dictionary<string,string>();
        Dictionary<string, string> padres = new Dictionary<string, string>();
        foreach (KeyValuePair<string, int> aberto in abertos) {
            padres.Add(aberto.Key,celaInicial);
        }
        padres.Add(celaInicial, celaActual.gameObject.name);
        //GameObject.Find(mellorCela).GetComponent<Cela>().ObterTransicions()
        while (abertos.Count != 0)
        {
            Dictionary<string, int> heuristica = ObterHeuristica(abertos);
            int menorPeso = -1;
            string mellorCela = null;
            foreach (KeyValuePair<string, int> transicion in heuristica)
            {
                if (menorPeso == -1 || menorPeso > transicion.Value)
                {
                    menorPeso = transicion.Value;
                    mellorCela = transicion.Key;
                }
            }
            if (mellorCela != null)
            {
                cerrados.Add(mellorCela);
                abertos.Remove(mellorCela);
                if (ComprobarMeta(mellorCela))
                {
                    axente.EstablecerCamino(ConstruirCamino(mellorCela, cerrados, padres));
                    return true;
                }
                else
                {
                    Dictionary<string, int> novos = new Dictionary<string, int>(GameObject.Find(mellorCela).GetComponent<Cela>().ObterTransicions());
                    Dictionary<string, int> novosAux = new Dictionary<string, int>(novos);
                    foreach (KeyValuePair<string, int> novo in novosAux)
                    {
                        foreach (string cerrado in cerrados)
                        {
                            if (cerrado.Equals(novo.Key))
                            {
                                novos.Remove(novo.Key);
                            }
                        }
                        foreach (KeyValuePair<string, int> aberto in abertos)
                        {
                            if (aberto.Key.Equals(novo.Key))
                            {
                                novos.Remove(novo.Key);
                            }
                        }
                    }
                    if (novos.Count != 0)
                    {
                        foreach (KeyValuePair<string, int> novo in novos)
                        {
                            abertos.Add(novo.Key, novo.Value);
                            try
                            {
                                padres.Add(novo.Key, mellorCela);
                            }
                            catch(ArgumentException) {

                            }
                            
                        }
                    }
                }
            }
        }
        return false;
    }
    public List<string> ConstruirCamino(string celaFinal, List<string> cerrados, Dictionary<string, string> padres) {
        bool construir = true;
        List<string> camino = new List<string>();
        camino.Add(celaFinal);
        string iterador = celaFinal;
        while (construir) {
            iterador = padres[iterador];
            camino.Add(iterador);
            if (iterador.Equals(celaActual.gameObject.name)) {
                construir = false;
            }
        }
        camino.Reverse();
        foreach (string cela in camino)
        {
            //Debug.Log("Camino: " + cela);
        }
        //List<string> celaUnica = new List<string>();
        return camino;
        
    }
   
    private bool ComprobarMeta(string cela) {
        string[] celaDiv = cela.Split(',');
        int celaXCoord = int.Parse(celaDiv[1]);
        int celaYCoord = int.Parse(celaDiv[2]);
        bool meta = (celaXCoord == destinoXCoord && celaYCoord == destinoYCoord);
        return meta;
    }
    
    /// <summary>
    /// Borra este script do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }
}


public class Invocar_ElixirProximaCela : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_ElixirProximaCela>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_ElixirProximaCela componente = gameObject.AddComponent<Comportamento_ElixirProximaCela>() as Comportamento_ElixirProximaCela;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        Axente_FSM_Celas axente;
        if (contido.Equals("equipo"))
        {
            axente = obxecto.GetComponent<Axente_FSM_Celas>();
            axente.idConversaLider = id;
            axente.liderEquipo = remitente;
            axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}