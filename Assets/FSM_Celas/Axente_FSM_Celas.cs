﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Axente_FSM_Celas : FSM
{
    /// <summary>
    /// La función start se invoca antes del primer update
    /// Rexistra os estados e obten referencias para o resto de axentes
    /// </summary>
    /// 2-19
    public string destino { get; set; } = "";
    public Cela celaActual;
    private Cela proximaCela;
    public List<string> camino { get; set; }
    NavMeshAgent axenteNav;
    public string habitacionXogador { get; set; } = "";
    //Patrulla
    public List<Cela> rutaPatrullaCruda;
    public List<string> rutaPatrulla { get; set; }
    //public int puntoPatrulla { get; set; } = 0;
    public bool finPatrulla { get; set; } = false;
    public List<Cela> rutaPatrullaObxectivoCruda;
    public List<string> rutaPatrullaObxectivo { get; set; }
    //Equipo
    public List<string> nomesEquipo { get; set; }
    public string liderEquipo { get; set; }
    public string idConversaLider { get; set; }
    public bool buscandoPosicion { get; set; } = false;
    public float ultimoMensaxeXogador = 0.8f;
    private const float TempoReenvio = 1f;
    public bool habitacionCorrecta { get; set; }  = false;
    public GameObject habitacionObxectivo;
    private Amanuense amanuense;

    //Debug
    public Transform exclamacion;
    public bool debug;

    void Start()
    {
        RexistrarEstados();
        RexistrarTransicions();
        AdecuarRutaPatrulla();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        amanuense = FindObjectOfType<Amanuense>();
        Comportamento estado = ObterEstadoInicial();
        estado.Inicio(gameObject);
        nomeEstadoActual = Estados.EstadoPatrullar;
        //nomeEstadoActual = Estados.EstadoPerseguir;
    }

    // Update is called once per frame
    void Update()
    {
        if (ultimoMensaxeXogador >= 0) {
            ultimoMensaxeXogador -= Time.deltaTime;
        }
    }
    /// <summary>
    /// Rexistra todos os estados do axente no seu diccionario
    /// </summary>
    private void RexistrarEstados()
    {
        //RexistrarEstadoInicial(new Invocar_PerseguirCela(), Estados_Celas.EstadoPerseguir);
        RexistrarEstadoInicial(new Invocar_PatrullarCela(), Estados_Celas.EstadoPatrullar);
        RexistrarEstado(new Invocar_FormarEquipoCelas(), Estados_Celas.EstadoFormarEquipo);
        RexistrarEstado(new Invocar_UnirseEquipoCelas(), Estados_Celas.EstadoUnirseEquipo);
        RexistrarEstado(new Invocar_DividirTareasCelasB(), Estados_Celas.EstadoDividirTareas);
        RexistrarEstado(new Invocar_PosicionarseCela(), Estados_Celas.EstadoPosicionarse);
        RexistrarEstado(new Invocar_EntrarSalaCela(), Estados_Celas.EstadoEntrarSala);
        RexistrarEstado(new Invocar_PerseguirCela(), Estados_Celas.EstadoPerseguir);
        RexistrarEstado(new Invocar_XogadorPerdidoCela(), Estados_Celas.EstadoXogadorPerdido);
        RexistrarEstado(new Invocar_PatrullarObxectivoCela(), Estados_Celas.EstadoPatrullarObxectivo);
        
    }

    /// <summary>
    /// Rexistra todas as transicions do axente
    /// </summary>
    private void RexistrarTransicions()
    {
        
        RexistrarTransicion(Estados_Celas.EstadoPatrullar, Estados_Celas.EstadoFormarEquipo, (int)Eventos_Celas.EventoLider);
        RexistrarTransicion(Estados_Celas.EstadoPatrullar, Estados_Celas.EstadoUnirseEquipo, (int)Eventos_Celas.EventoPeticionEquipo);
        RexistrarTransicion(Estados_Celas.EstadoFormarEquipo, Estados_Celas.EstadoDividirTareas, (int)Eventos_Celas.EventoEquipoFormado);
        RexistrarTransicion(Estados_Celas.EstadoUnirseEquipo, Estados_Celas.EstadoDividirTareas, (int)Eventos_Celas.EventoEquipoFormado);
        RexistrarTransicion(Estados_Celas.EstadoDividirTareas, Estados_Celas.EstadoPosicionarse, (int)Eventos_Celas.EventoFinPlanificar);
        RexistrarTransicion(Estados_Celas.EstadoPosicionarse, Estados_Celas.EstadoEntrarSala, (int)Eventos_Celas.EventoEnPosicion);
        RexistrarTransicion(Estados_Celas.EstadoPosicionarse, Estados_Celas.EstadoPerseguir, (int)Eventos_Celas.EventoVerXogador);
        RexistrarTransicion(Estados_Celas.EstadoEntrarSala, Estados_Celas.EstadoPerseguir, (int)Eventos_Celas.EventoVerXogador);
        RexistrarTransicion(Estados_Celas.EstadoEntrarSala,Estados_Celas.EstadoXogadorPerdido, (int)Eventos_Celas.EventoXogadorPerdido);
        RexistrarTransicion(Estados_Celas.EstadoPerseguir, Estados_Celas.EstadoXogadorPerdido, (int)Eventos_Celas.EventoXogadorPerdido);
        RexistrarTransicion(Estados_Celas.EstadoXogadorPerdido, Estados_Celas.EstadoPerseguir, (int)Eventos_Celas.EventoVerXogador);
        RexistrarTransicion(Estados_Celas.EstadoDividirTareas, Estados_Celas.EstadoPatrullarObxectivo, (int)Eventos_Celas.EventoPatrullarObxectivo);
        RexistrarTransicion(Estados_Celas.EstadoPatrullarObxectivo, Estados_Celas.EstadoPerseguir, (int)Eventos_Celas.EventoVerXogador);
        //Lider de Equipo
        RexistrarTransicion(Estados_Celas.EstadoPosicionarse, Estados_Celas.EstadoFormarEquipo, (int)Eventos_Celas.EventoLider);
        RexistrarTransicion(Estados_Celas.EstadoEntrarSala, Estados_Celas.EstadoFormarEquipo, (int)Eventos_Celas.EventoLider);
        RexistrarTransicion(Estados_Celas.EstadoPerseguir, Estados_Celas.EstadoFormarEquipo, (int)Eventos_Celas.EventoLider);
        RexistrarTransicion(Estados_Celas.EstadoXogadorPerdido, Estados_Celas.EstadoFormarEquipo, (int)Eventos_Celas.EventoLider);

        //Unirse a Equipo
        RexistrarTransicion(Estados_Celas.EstadoPosicionarse, Estados_Celas.EstadoUnirseEquipo, (int)Eventos_Celas.EventoPeticionEquipo);
        RexistrarTransicion(Estados_Celas.EstadoEntrarSala, Estados_Celas.EstadoUnirseEquipo, (int)Eventos_Celas.EventoPeticionEquipo);
        RexistrarTransicion(Estados_Celas.EstadoPerseguir, Estados_Celas.EstadoUnirseEquipo, (int)Eventos_Celas.EventoPeticionEquipo);
        RexistrarTransicion(Estados_Celas.EstadoXogadorPerdido, Estados_Celas.EstadoUnirseEquipo, (int)Eventos_Celas.EventoPeticionEquipo);
        RexistrarTransicion(Estados_Celas.EstadoPatrullarObxectivo, Estados_Celas.EstadoUnirseEquipo, (int)Eventos_Celas.EventoPeticionEquipo);
    }
    public string ObterDestino() {
        return destino;
    }
    public Cela ObterCelaActual() {
        return celaActual;
    }

    public void EstablecerProximaCela(Cela proximaCela) {
        this.proximaCela = proximaCela;
    }
    public Cela ObterProximaCela() {
        return proximaCela;
    }
    public void EstablecerCamino(List<string> caminos) {
        this.camino = caminos;

    }

    private void AdecuarRutaPatrulla(){
        rutaPatrulla = new List<string>();
        rutaPatrullaObxectivo = new List<string>();
        string[] puntoDiv;
        string puntoAdecuado;
        foreach (Cela punto in rutaPatrullaCruda) {
            puntoDiv = punto.gameObject.name.Split(',');
            puntoAdecuado = puntoDiv[1] + ',' + puntoDiv[2];
            rutaPatrulla.Add(puntoAdecuado);
        }
        foreach (Cela punto in rutaPatrullaObxectivoCruda)
        {
            puntoDiv = punto.gameObject.name.Split(',');
            puntoAdecuado = puntoDiv[1] + ',' + puntoDiv[2];
            rutaPatrullaObxectivo.Add(puntoAdecuado);
        }
    }

    public void CambioObxectivo(List<string> obxectivo) {
        rutaPatrullaObxectivo = obxectivo;
        habitacionObxectivo = GameObject.Find("salaE");
    }
    /// <summary>
    /// Obten a lista de conversas do axente
    /// </summary>
    /// <returns>lista conversas do axente</returns>
    public List<Conversa> ObterConversas()
    {
        return listaConversas;
    }

    public void EstablecerConversas(List<Conversa> conversas)
    {
        listaConversas = conversas;
    }

    public void VerXogador(Cela posXogador, string habitacionXogador)
    {
        if (ultimoMensaxeXogador < 0) {
            ultimoMensaxeXogador = TempoReenvio;
            Script_Comunicacion[] patrullas = FindObjectsOfType<Script_Comunicacion>();
            if (!this.habitacionXogador.Equals(habitacionXogador))
            {
                this.habitacionXogador = habitacionXogador;

                bool lider = true;
                foreach (Script_Comunicacion patrulla in patrullas)
                {
                    if (patrulla.gameObject.GetComponent<Comportamento_FormarEquipo>() != null)
                    {
                        lider = false;
                    }
                    EnviarMensaxe("inform", "xogador\\" + habitacionXogador, gameObject.name, patrulla.name, patrulla, "-1");
                }
                if (lider)
                {
                    DispararEvento((int)Eventos_Celas.EventoLider);
                }
            }
            string[] celasDiv = posXogador.name.Split(',');
            foreach (Script_Comunicacion patrulla in patrullas)
            {
                EnviarMensaxe("inform", "CoordXogador\\" + celasDiv[1] + ',' + celasDiv[2], gameObject.name, patrulla.name, patrulla, "-1");
            }
            destino = celasDiv[1] + ',' + celasDiv[2];
            DispararEvento((int)Eventos_Celas.EventoVerXogador);
            if (debug)
            {
                Instantiate(exclamacion, new Vector3(posXogador.transform.position.x, 0.6f, posXogador.transform.position.z), exclamacion.rotation);
            }
        }
        amanuense.Cambio(gameObject.name,posXogador.name);
        ObterEstadoActual().VerXogador(posXogador, habitacionXogador);
    }


    void OnCollisionEnter(Collision collision)
    {
        GameObject obxectoColision = collision.collider.gameObject;

        if (obxectoColision.tag.Equals("cela"))
        {
            if (celaActual != null)
            {
                if (!obxectoColision.transform.gameObject.name.Equals(celaActual.gameObject.name))
                {
                    celaActual = obxectoColision.transform.gameObject.GetComponent<Cela>();
                    //Debug.Log(obxectoColision.transform.gameObject.name);
                }
            }
            else {
                celaActual = obxectoColision.transform.gameObject.GetComponent<Cela>();
                //Debug.Log(obxectoColision.transform.gameObject.name);
            }
            
        }
    }


    protected override void TratarAccept(string remitente, string id)
    {
        ObterEstadoActual().TratarAccept(remitente, id);
    }

    protected override void TratarInform(string contido, string remitente, string id)
    {
        if (contido.Split('\\')[0].Equals("xogador"))
        {
            string habitacion = contido.Split('\\')[1];
            if (!habitacionXogador.Equals(habitacion))
            {
                habitacionCorrecta = false;
                habitacionXogador = habitacion;
            }
        }
        else {
            ObterEstadoActual().TratarInform(contido, remitente, id);
        }
    }

    protected override void TratarQuery(string contido, string remitente, string id)
    {
        ObterEstadoActual().TratarQuery(contido, remitente, id);
    }

    protected override void TratarRefuse(string remitente, string id)
    {
        ObterEstadoActual().TratarRefuse(remitente, id);
    }

    protected override void TratarRequest(string contido, string remitente, string id)
    {
        ObterEstadoActual().TratarRequest(contido, remitente, id);
    }

    public void EscoitarXogador(Cela posXogador, string habitacionXogador) {
        Instantiate(exclamacion, new Vector3(posXogador.transform.position.x, 0.6f, posXogador.transform.position.z), exclamacion.rotation);
        amanuense.Cambio(gameObject.name, posXogador.name);
        ObterEstadoActual().EscoitarXogador(posXogador, habitacionXogador);
    }
}
