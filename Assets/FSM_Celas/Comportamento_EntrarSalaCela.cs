﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_EntrarSalaCela : MonoBehaviour
{
    private Axente_FSM_Celas axente;
    private NavMeshAgent axenteNav;
    private RecorrerCamiño axenteMov;
    private List<string> nomesEquipo;
    private Dictionary<string, bool> respostasEquipoPosicion = new Dictionary<string, bool>();
    private BusquedaCamiño buscador = new BusquedaCamiño();
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Celas>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiño>();
        PrepararCamiñoCela();
        axenteNav.isStopped = false;
    }

    private void PrepararCamiñoCela() {
        Cela cela = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro();
        string[] celaDiv = cela.gameObject.name.Split(',');
        axente.destino = celaDiv[1] + ',' + celaDiv[2];
        axente.camino = buscador.ObterCamino(gameObject);
        axenteMov.ObterDestino();
        
    }

    public void TratarInform(string contido, string remitente) {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("CoordXogador"))
        {
            axente.destino = contidoDiv[1];
            axente.DispararEvento((int)Eventos_Celas.EventoVerXogador);
        }
    }
    void Update()
    {
        if (axenteMov.finCamino) {
            axente.DispararEvento((int)Eventos_Celas.EventoXogadorPerdido);
        }
    }

    public void TratarRequest(string contido, string remitente, string id)
    {
        if (contido.Equals("equipo"))
        {
            if (!axente.habitacionCorrecta)
            {
                axente.idConversaLider = id;
                axente.liderEquipo = remitente;
                axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
            }
        }
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }
}

public class Invocar_EntrarSalaCela : Comportamento
{
    GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_EntrarSalaCela>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_EntrarSalaCela componente = gameObject.AddComponent<Comportamento_EntrarSalaCela>() as Comportamento_EntrarSalaCela;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_EntrarSalaCela>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            //obxecto.GetComponent<Comportamento_CerrarSala>().TratarQuery(contido, remitente, id);
        }
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_EntrarSalaCela>().TratarRequest(contido, remitente, id);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}
