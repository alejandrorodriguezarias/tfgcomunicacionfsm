﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escoitar_Celas : MonoBehaviour
{
    public void Escoitar(Cela cela, string habitacionXogador) {
        GetComponent<Axente_FSM_Celas>().EscoitarXogador(cela, habitacionXogador);
    }
}
