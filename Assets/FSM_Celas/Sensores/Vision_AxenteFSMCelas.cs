﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision_AxenteFSMCelas : Script_CampoVision
{
    protected override void comportamentoVerObxetivo(List<Transform> listaObxectivos)
    {
        foreach (Transform obxectivo in listaObxectivos)
        {
            if (obxectivo.gameObject.name.Equals("Xogador"))
            {
                Script_ControladorXogador xogador = obxectivo.gameObject.GetComponent<Script_ControladorXogador>();
                GetComponent<Axente_FSM_Celas>().VerXogador(xogador.celaActual, xogador.habitacionActual);
            }
        }
    }
}
