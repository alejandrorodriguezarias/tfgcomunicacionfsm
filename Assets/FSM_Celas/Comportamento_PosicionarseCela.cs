﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PosicionarseCela : MonoBehaviour
{

    //Axente propietario do script
    private Axente_FSM_Celas axente;
    private NavMeshAgent axenteNav;
    private RecorrerCamiño axenteMov;
    //lista dos axentes que se unen ao equipo
    private const float TempoReenvio = 0.8f;
    private float tempoUltimaComprobacion = TempoReenvio;
    private BusquedaCamiño buscador = new BusquedaCamiño();
    private Transform centro;
    private bool enPosicion = false;
    private Dictionary<string, bool> equipoPosicion = new Dictionary<string, bool>();

    /// <summary>
    /// Obtemos o axente propietario do script e comeza o seu comportamento
    /// </summary>
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM_Celas>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        axenteMov = gameObject.GetComponent<RecorrerCamiño>();
        axente.EstablecerConversas(new List<Conversa>());
        axente.camino = buscador.ObterCamino(gameObject);
        centro = GameObject.Find(axente.habitacionXogador).GetComponent<Sala>().ObterCentro().transform;
        if (ComprobarDestinoCentro()) {
            enPosicion = true;
            InformarEnPosicion();
        }
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
        axenteNav.speed = 4f;
    }

    /// <summary>
    /// Cada frame comproba se xa se enviaron as mensaxes e se foron respondidas
    /// </summary>
    void Update()
    {
        if (axenteMov.finCamino && !enPosicion) {
            axenteNav.isStopped = true;
            gameObject.transform.LookAt(centro);
            enPosicion = true;
            InformarEnPosicion();
        }

        if (enPosicion && tempoUltimaComprobacion < 0)
        {
            tempoUltimaComprobacion = TempoReenvio;
            if (ComprobarEquipo())
            {
                InformarEnPosicion();
                axente.DispararEvento((int)Eventos_Celas.EventoEnPosicion);
            }
        }
        else {
            tempoUltimaComprobacion -= Time.deltaTime;
        }
    }
    private bool ComprobarDestinoCentro() {
        string[] centroNome = centro.gameObject.name.Split(',');
        if (axente.destino.Equals(centroNome[1] + ',' + centroNome[2]))
        {
            return true;
        }
        else {
            return false;
        }
    }
    private void InformarEnPosicion() {
        foreach (string nome in axente.nomesEquipo) {
            Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "enPosicion\\" + enPosicion, gameObject.name, nome, receptor, "-1");
        }
    }
    private bool ComprobarEquipo() {
        bool equipoEnPosicion = true;
        if (equipoPosicion.Count != (axente.nomesEquipo.Count-1))
        {
            equipoEnPosicion = false;
        }
        else {
            foreach (KeyValuePair<string, bool> patrulla in equipoPosicion)
            {
                if (!patrulla.Value) {
                    equipoEnPosicion = false;
                }
            }
        }
        return equipoEnPosicion;
        
    }
    public void TratarInform(string contido, string remitente) {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("enPosicion"))
        {
            try
            {
                equipoPosicion.Add(remitente, contidoDiv[1].Equals("True") ? true : false);
            }
            catch (ArgumentException)
            {
                equipoPosicion[remitente] = contidoDiv[1].Equals("True") ? true : false;
            }
        }else if (contidoDiv[0].Equals("SalirEquipo"))
        {
            axente = gameObject.GetComponent<Axente_FSM_Celas>();
            axente.nomesEquipo.Remove(remitente);
        }
    }

    public void TratarRequest(string contido, string remitente, string id) {
        if (contido.Equals("equipo"))
        {
            if (!axente.habitacionCorrecta) {
                axente.idConversaLider = id;
                axente.liderEquipo = remitente;
                axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
            }
        }
    }

    public void EscoitarXogador(Cela posXogador, string habitacionXogador) {
        string[] celasDiv = posXogador.name.Split(',');
        axente.destino = celasDiv[1] + ',' + celasDiv[2];
        axente.DispararEvento((int)Eventos_Celas.EventoVerXogador);
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }
}
public class Invocar_PosicionarseCela : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_PosicionarseCela>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PosicionarseCela componente = gameObject.AddComponent<Comportamento_PosicionarseCela>() as Comportamento_PosicionarseCela;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PosicionarseCela>().TratarInform(contido, remitente);
        }
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PosicionarseCela>().TratarRequest(contido, remitente, id);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PosicionarseCela>().EscoitarXogador(posXogador, habitacionXogador);
        }
    }
}