﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PatrullarCela : MonoBehaviour
{
    NavMeshAgent axenteNav;
    Axente_FSM_Celas axente;
    private List<string> puntos;
    private int puntoDestino;
    RecorrerCamiño axenteMov;
    private BusquedaCamiño buscador = new BusquedaCamiño();

    /// <summary>
    /// La función start se invoca antes del primer update
    /// Obtiene unha referencia ao NavMeshAngent e desactiva o autoBraking
    /// Obten os puntos de patrulla do axente
    /// </summary>
    void Start()
    {
        axenteNav = GetComponent<NavMeshAgent>();
        axente = GetComponent<Axente_FSM_Celas>();
        axenteMov = axente.GetComponent<RecorrerCamiño>();
        //Desactivar autoBraking permite un movemento continuo entre puntos
        axenteNav.autoBraking = false;
        puntos = axente.rutaPatrulla;
        puntoDestino = 0;
        IrPuntoSeguinte();
    }
    /// <summary>
    /// Este metodo encargase de obter a proxima coordenada da patrulla e ordenar ao axente moverse ata ese punto
    /// </summary>
    /// <param name="inicio">Se estamos no inicio o puntoDestino é 0 pero non rematou a patrulla</param>
    void IrPuntoSeguinte()
    {
        // Se non existen puntos devolvemos unha excepcion
        if (puntos.Count == 0)
        {
            throw new Exception("É necesario ter algun punto de patrulla");
        }
        //O destino do axente e o proximo punto
        axente.destino = puntos[puntoDestino];
        puntoDestino = (puntoDestino + 1) % puntos.Count;
        // Elixe o seguinte punto no array,
        // Regresa o inicio do array se e necesario
        List<string> camiño = buscador.ObterCamino(gameObject);
        axente.camino = camiño;
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
        //axente.DispararEvento((int)Eventos_Celas.EventoPuntoPatrulla);
    }
    /// <summary>
    /// Borra este script do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    void Update()
    {
        //Cando se chega ao punto elixese o seguinte.
        if (axenteMov.finCamino)
        {
            IrPuntoSeguinte();
        }
    }

    public void EscoitarXogador(Cela posXogador, string habitacionXogador) {
        Script_Comunicacion[] patrullas = FindObjectsOfType<Script_Comunicacion>();
        if (!axente.habitacionXogador.Equals(habitacionXogador))
        {
            axente.habitacionXogador = habitacionXogador;
            bool lider = true;
            foreach (Script_Comunicacion patrulla in patrullas)
            {
                if (patrulla.gameObject.GetComponent<Comportamento_FormarEquipo>() != null)
                {
                    lider = false;
                }
                axente.EnviarMensaxe("inform", "xogador\\" + habitacionXogador, gameObject.name, patrulla.name, patrulla, "-1");
            }
            if (lider)
            {
                axente.DispararEvento((int)Eventos_Celas.EventoLider);
            }
        }
        string[] celasDiv = posXogador.name.Split(',');
        axente.destino = celasDiv[1] + ',' + celasDiv[2];
    }
}

public class Invocar_PatrullarCela : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_PatrullarCela>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PatrullarCela componente = gameObject.AddComponent<Comportamento_PatrullarCela>() as Comportamento_PatrullarCela;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        Axente_FSM_Celas axente;
        if (contido.Equals("equipo")) {
            axente = obxecto.GetComponent<Axente_FSM_Celas>();
            axente.idConversaLider = id;
            axente.liderEquipo = remitente;
            axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PatrullarCela>().EscoitarXogador(posXogador, habitacionXogador);
        }
    }
}

