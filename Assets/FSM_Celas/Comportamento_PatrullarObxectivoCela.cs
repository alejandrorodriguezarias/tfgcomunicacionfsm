﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_PatrullarObxectivoCela : MonoBehaviour
{
    NavMeshAgent axenteNav;
    Axente_FSM_Celas axente;
    private List<string> puntos;
    private int puntoDestino;
    RecorrerCamiño axenteMov;
    private BusquedaCamiño buscador = new BusquedaCamiño();

    /// <summary>
    /// La función start se invoca antes del primer update
    /// Obtiene unha referencia ao NavMeshAngent e desactiva o autoBraking
    /// Obten os puntos de patrulla do axente
    /// </summary>
    void Start()
    {
        axenteNav = GetComponent<NavMeshAgent>();
        axente = GetComponent<Axente_FSM_Celas>();
        axenteMov = axente.GetComponent<RecorrerCamiño>();
        //Desactivar autoBraking permite un movemento continuo entre puntos
        axenteNav.autoBraking = false;
        puntos =axente.rutaPatrullaObxectivo;
        puntoDestino = 0;
        IrPuntoSeguinte();
    }
    /// <summary>
    /// Este metodo encargase de obter a proxima coordenada da patrulla e ordenar ao axente moverse ata ese punto
    /// </summary>
    /// <param name="inicio">Se estamos no inicio o puntoDestino é 0 pero non rematou a patrulla</param>
    void IrPuntoSeguinte()
    {
        // Se non existen puntos devolvemos unha excepcion
        if (puntos.Count == 0)
        {
            throw new Exception("É necesario ter algun punto de patrulla");
        }
        //O destino do axente e o proximo punto
        axente.destino = puntos[puntoDestino];
        puntoDestino = (puntoDestino + 1) % puntos.Count;
        // Elixe o seguinte punto no array,
        // Regresa o inicio do array se e necesario
        List<string> camiño = buscador.ObterCamino(gameObject);
        axente.camino = camiño;
        axenteMov.ObterDestino();
        axenteNav.isStopped = false;
        foreach (string nome in axente.nomesEquipo)
        {
            Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "SalirEquipo\\" + true, gameObject.name, nome, receptor, "-1");
        }
        //axente.DispararEvento((int)Eventos_Celas.EventoPuntoPatrulla);
    }
    /// <summary>
    /// Borra este script do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    void Update()
    {
        //Cando se chega ao punto elixese o seguinte.
        if (axenteMov.finCamino)
        {
            IrPuntoSeguinte();
        }
    }
    public void TratarRequest(string contido, string remitente, string id) {
        if (contido.Equals("equipo"))
        {
            axente.idConversaLider = id;
            axente.liderEquipo = remitente;
            axente.DispararEvento((int)Eventos_Celas.EventoPeticionEquipo);
            //Script_Comunicacion receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            //axente.EnviarMensaxe("refuse", null, gameObject.name, remitente, receptor, axente.idConversaLider);
        }
    }

    public void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        string[] celasDiv = posXogador.name.Split(',');
        axente.destino = celasDiv[1] + ',' + celasDiv[2];
        axente.DispararEvento((int)Eventos_Celas.EventoVerXogador);
    }
}

public class Invocar_PatrullarObxectivoCela : Comportamento
{
    private GameObject obxecto;
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_PatrullarObxectivoCela>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        obxecto = gameObject;
        Comportamento_PatrullarObxectivoCela componente = gameObject.AddComponent<Comportamento_PatrullarObxectivoCela>() as Comportamento_PatrullarObxectivoCela;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PatrullarObxectivoCela>().TratarRequest(contido, remitente, id);
        }
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        if (obxecto != null)
        {
            obxecto.GetComponent<Comportamento_PatrullarObxectivoCela>().EscoitarXogador(posXogador, habitacionXogador);
        }
    }
}

