﻿using UnityEngine;
using UnityEngine.AI;

public class Script_ControladorXogador : MonoBehaviour
{
    public float velocidade;
    // Referencia al transform de la camaraPrincipal
    private Transform camaraPrincipal; 
    public float distAbrirPorta;
    public float velocidadeRotacion;
    
    public string habitacionActual { get; set; }  = "";
    public  bool botinObtido { get; set; } = false;
    public Cela celaActual { get; set; }
    private string nomeCelaActual { get; set; } = "";
    //Son
    private const float TempoEntreEmisions = 1.5f;
    private const int RadioSon = 7;
    private float tempoUltimaEmision = 0f;
    bool caminar = true;
    //float delay = 0.5f;

    //Debug
    public bool debug;
    public float tempoDebug = 1f;
    private bool visible = false;
    //Debug
    public Transform son;

    NavMeshAgent nav;

    void Start()
    {
        //Se existe camera a obtemos para que o movemento do xogador sexa relativa á 
        //camera e non ao mundo. Debe ter tag de main camera
        if (Camera.main != null)
        {
            camaraPrincipal = Camera.main.transform;
        }

        nav = GetComponent<NavMeshAgent>();
        nav.isStopped = true;
    }
    //void FixedUpdate()
    //{
    //    float movementoHorizontal = Input.GetAxis("Horizontal")*velocidade;
    //    float movementoVertical = Input.GetAxis("Vertical")*velocidade;
    //    Vector3 dirCamara;
    //    Vector3 movemento;
    //    if (camaraPrincipal != null)
    //    {
    //        //calculamos a direccion relativa da camara
    //        //Normalizamos a dirección no eixe Z
    //        //Non termino de entender xk cp.forward non da unha dirección directamente.
    //        dirCamara = Vector3.Scale(camaraPrincipal.forward, new Vector3(0, 0, 1)).normalized;
    //        movemento = movementoVertical * dirCamara + movementoHorizontal * camaraPrincipal.right;
    //    }
    //    else {
    //        movemento = new Vector3(movementoHorizontal, 0.0f, movementoVertical);
    //    }
    //    //Son
    //    if ((movementoHorizontal != 0 || movementoVertical != 0) && tempoUltimaEmision < 0)
    //    {
    //        ParticleSystem.EmissionModule son = GetComponent<ParticleSystem>().emission;
    //        son.enabled = true;
    //        tempoUltimaEmision = TempoEntreEmisions;
    //        EmitirSon();
    //    }
    //    else if(movementoHorizontal == 0 && movementoVertical == 0){
    //        ParticleSystem.EmissionModule son = GetComponent<ParticleSystem>().emission;
    //        son.enabled = false;
    //    }

    //    tempoUltimaEmision -= Time.deltaTime;
    //    //Movemos ao xogador
    //    transform.Translate(movemento, Space.World);

    //    //O xogador mira hacia onde se move
    //    if (movementoHorizontal < 0){
    //        transform.eulerAngles = new Vector3(transform.eulerAngles.x, 90, transform.eulerAngles.z);
    //    }else
    //        if (movementoHorizontal > 0){
    //            transform.eulerAngles = new Vector3(transform.eulerAngles.x, -90, transform.eulerAngles.z);
    //        }else
    //            if (movementoVertical > 0){
    //                transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z);
    //            }else
    //                if (movementoVertical < 0){
    //                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
    //                }
    //    //Abrir portas
    //    if (Input.GetButtonDown("Interactuar")){
    //        AbrirPorta();
    //    }
    //}

    void FixedUpdate()
    {
        if (nav.remainingDistance < 1 && !nav.pathPending)
        {
            nav.isStopped = true;
            //ParticleSystem.EmissionModule son = GetComponent<ParticleSystem>().emission;
            //son.enabled = false;
            if (celaActual != null)
            {
                float movementoHorizontal = Input.GetAxis("Horizontal");
                float movementoVertical = Input.GetAxis("Vertical");
                string[] coord = nomeCelaActual.Split(',');
                int XCoord = int.Parse(coord[1]);
                int YCoord = int.Parse(coord[2]);
                //O xogador mira hacia onde se move
                if (movementoHorizontal < 0)
                {
                    YCoord++;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 90, transform.eulerAngles.z);
                }
                else if (movementoHorizontal > 0)
                {
                    YCoord--;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, -90, transform.eulerAngles.z);
                }
                if (movementoVertical > 0)
                {
                    XCoord--;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z);
                }
                else if (movementoVertical < 0)
                {
                    XCoord++;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
                }

                if ((movementoVertical != 0 || movementoHorizontal != 0) && caminar)
                {
                    //caminar = false;
                    GameObject tmp = GameObject.Find("cela," + XCoord + ',' + YCoord);
                    if (tmp == null)
                    {
                        tmp = GameObject.Find("porta," + XCoord + ',' + YCoord);
                        if (tmp != null)
                        {
                            if (tmp.transform.GetChild(0).gameObject.GetComponent<Script_Porta>().EstaPechada())
                            {
                                tmp = null;
                            }
                        }
                    }
                    //Debug.Log(tmp.name);
                    if (tmp != null)
                    {
                        //transform.position = new Vector3(tmp.transform.position.x, 0.5f, tmp.transform.position.z);
                        nav.destination = new Vector3(tmp.transform.position.x, 0.6f, tmp.transform.position.z);
                        nomeCelaActual = tmp.name;
                        nav.isStopped = false;
                    }
                }
            }

        }
        else {
            if (tempoUltimaEmision < 0)
            {
                EmitirSon();
                //ParticleSystem.EmissionModule son = GetComponent<ParticleSystem>().emission;
                //son.enabled = true;
                if (!debug) {
                    Instantiate(son, transform.position, son.rotation); //MANTENER
                }
                tempoUltimaEmision = TempoEntreEmisions;
            }
            else {
                tempoUltimaEmision -= Time.deltaTime;
            }
        }
        if (tempoDebug < 0)
        {
            visible = false;
        }
        else
        {
            tempoDebug -= Time.deltaTime;
        }

        if (Input.GetButtonDown("Interactuar"))
        {
            AbrirPorta();
        }

        if (debug) {
            if (!visible) {
                MeshRenderer cubo = transform.GetChild(0).gameObject.GetComponent<MeshRenderer>();
                cubo.enabled = false;
                cubo = transform.GetChild(1).gameObject.GetComponent<MeshRenderer>();
                cubo.enabled = false;
            }
        }
    }
    private void EmitirSon()
    {
        Escoitar_Sensor[] axentes = FindObjectsOfType<Escoitar_Sensor>();
        foreach (Escoitar_Sensor axente in axentes)
        {
            if ((RadioSon - Vector3.Distance(transform.position, axente.transform.position)) > 0)
            {
                axente.Escoitar(celaActual,habitacionActual);
            }
        }
        if (debug) {
            Axente_Humano xogador = FindObjectOfType<Axente_Humano>();
            if ((RadioSon - Vector3.Distance(transform.position, xogador.transform.position)) > 0){
                Instantiate(son, transform.position, son.rotation);
            }
        }
    }

    //void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.yellow;
    //    Gizmos.DrawSphere(transform.position, radioAbrirPorta);
    //}

    void AbrirPorta() {
        RaycastHit impacto;
        //Comprobamos se o xogador esta frente a porta
        if (Physics.Raycast(transform.position, transform.TransformDirection(new Vector3(0f,0.5f,1f)), out impacto, distAbrirPorta))
        {
            Debug.Log(impacto.collider.gameObject.name);
            if (impacto.collider.gameObject.gameObject.GetComponent<Script_Porta>())
            {
                //abre a porta o a cerra dependendo do estado da porta
                Script_Porta porta = impacto.collider.gameObject.GetComponent<Script_Porta>();
                if (porta.EstaPechada())
                {
                    porta.Abrir();
                }
                else
                {
                    porta.Pechar();
                }
            }
        }
    }
    void OnCollisionEnter(Collision collision) {
        GameObject obxectoColision = collision.collider.gameObject;

        if (obxectoColision.tag.Equals("cela")) {
            if (!obxectoColision.transform.parent.gameObject.name.Equals(habitacionActual)) {
                habitacionActual = obxectoColision.transform.parent.gameObject.name;
                Debug.Log(habitacionActual);
            }
            celaActual = obxectoColision.transform.gameObject.GetComponent<Cela>();
        }

        if (nomeCelaActual.Equals("")) {

            nomeCelaActual = celaActual.name;
        }
    }

    public void MostrarCuerpo() {

        MeshRenderer cubo = transform.GetChild(0).gameObject.GetComponent<MeshRenderer>();
        cubo.enabled = true;
        cubo = transform.GetChild(1).gameObject.GetComponent<MeshRenderer>();
        cubo.enabled = true;
        visible = true;
        tempoDebug = 1f;
    }
}