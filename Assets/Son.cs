﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Son : MonoBehaviour
{
    private float tempoVida = 2f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (tempoVida < 0)
        {
            Destroy(gameObject);
        }
        else {
            tempoVida -= Time.deltaTime;
        }
    }
}
