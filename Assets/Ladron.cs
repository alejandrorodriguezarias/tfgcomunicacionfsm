﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ladron : MonoBehaviour
{
    public Transform botin;
    public Transform saida;
    private RecorrerCamiño axenteMov;
    int i;
    public List<string> camino { get; set; }
    private BusquedaCamiñoLadron buscador;
    private RecorrerCamiñoLadron ladronMov;
    public string destino { get; set; } = "";
    private Transform transformDestino;
    public Cela cela;
    public Cela celaActual { get; set; }
    public string habitacionActual { get; set; } = "";
    public string nomeCelaActual { get; set; } = "";
    private bool mov = false;
    private bool calcularDistancias = false;
    private int iteradorDistancias = 0;
    private Transform entradaActual;
    private NavMeshAgent nav;
    private Transform metaLocal = null;
    private float menorDistancia;
    private Amanuense amanuense;
    public bool debug = false;
    private bool visible = false;
    public float tempoDebug = 1f;

    // Start is called before the first frame update
    void Start()
    {
        ladronMov = GetComponent<RecorrerCamiñoLadron>();
        buscador = GetComponent<BusquedaCamiñoLadron>();
        nav = GetComponent<NavMeshAgent>();
        amanuense = FindObjectOfType<Amanuense>();
        celaActual = cela;
        nomeCelaActual = cela.name;
        destino = botin.gameObject.name;
        transformDestino = botin;
        AdaptarDestino();
        //camino = buscador.ObterCamino(gameObject);
        //if (camino == null)
        //{
        //    mov = true;
        //}
        //else {
        //    ladronMov.ObterDestino();
        //}

    }

    // Update is called once per frame
    void Update()
    {
        //if (calcularDistancias)
        //{
        //    entradaActual = destinos[i];
        //    nav.destination = entradaActual.position;
        //    if (!nav.pathPending)
        //    {
        //        Debug.Log("hey");
        //        if (metaLocal == null)
        //        {
        //            metaLocal = entradaActual;
        //            menorDistancia = Vector3.Distance(gameObject.transform.position,entradaActual.position);
        //        }
        //        else {
        //            float distancia = Vector3.Distance(gameObject.transform.position, entradaActual.position);
        //            Debug.Log(distancia);
        //            Debug.Log(menorDistancia);
        //            if (distancia < menorDistancia) {
        //                metaLocal = entradaActual;
        //                menorDistancia = distancia;
        //            }
        //        }
        //        if (iteradorDistancias < destinos.Count - 1)
        //        {
        //            iteradorDistancias += 1;
        //        }
        //        else
        //        {
        //            calcularDistancias = false;
        //            iteradorDistancias = 0;
        //            Debug.Log(metaLocal);
        //            destino = metaLocal.name;
        //            string[] destinoDiv = destino.Split(',');
        //            destino = destinoDiv[1] + "," + destinoDiv[2];
        //            mov = true;
        //        }
        //    }
        //}
        if (mov)
        {
            camino = buscador.ObterCamino(gameObject);
            if (camino != null)
            {
                Debug.Log(camino.Count);
                Debug.Log("go");
                mov = false;
                nav.isStopped = false;
                ladronMov.ObterDestino();
            }
        }
        if (ladronMov.finCamino)
        {
            AdaptarDestino();
        }

        if (debug)
        {
            if (!visible)
            {
                MeshRenderer cubo = transform.GetChild(0).gameObject.GetComponent<MeshRenderer>();
                cubo.enabled = false;
                cubo = transform.GetChild(1).gameObject.GetComponent<MeshRenderer>();
                cubo.enabled = false;
            }
        }

        if (tempoDebug < 0)
        {
            visible = false;
        }
        else
        {
            tempoDebug -= Time.deltaTime;
        }

    }
    public void Escapar()
    {
        destino = saida.gameObject.name;
        transformDestino = saida;
        AdaptarDestino();
        camino = buscador.ObterCamino(gameObject);
        if (camino == null)
        {
            mov = true;
        }
        else
        {
            ladronMov.ObterDestino();
        }

    }
    void OnCollisionEnter(Collision collision)
    {
        GameObject obxectoColision = collision.collider.gameObject;

        if (obxectoColision.tag.Equals("cela"))
        {
            if (!obxectoColision.transform.parent.gameObject.name.Equals(habitacionActual))
            {
                habitacionActual = obxectoColision.transform.parent.gameObject.name;
                Debug.Log(habitacionActual);
            }
            if (celaActual != null)
            {
                if (obxectoColision.transform.gameObject.GetComponent<Cela>() != celaActual)
                {
                    //FindObjectOfType<Amanuense>().Cambio(gameObject.name, obxectoColision.transform.gameObject.name);
                }
            }
            celaActual = obxectoColision.transform.gameObject.GetComponent<Cela>();
            nomeCelaActual = celaActual.name;
        }
    }
    private void AdaptarDestino()
    {
        metaLocal = null;
        menorDistancia = 0;
        DirAleatoria(5);
        destino = metaLocal.name;
        string[] destinoDiv = destino.Split(',');
        destino = destinoDiv[1] + "," + destinoDiv[2];
        nav.isStopped = true;
        mov = true;
    }

    private void DirAleatoria(int radio)
    {
        string destino = "";
        string centro = nomeCelaActual;
        Debug.Log("CAMBIO" + centro);
        string[] centroDiv = centro.Split(',');
        int xCoord = int.Parse(centroDiv[1]);
        int yCoord = int.Parse(centroDiv[2]);
        int xCoordTmp = xCoord;
        int yCoordTmp = yCoord;
        HashSet<Transform> tmp = new HashSet<Transform>();
        while (tmp.Count < 8)
        {
            Random.InitState(System.DateTime.Now.Millisecond);
            int explrX = Random.Range(-radio, radio);
            int explrY = Random.Range(-radio, radio);
            xCoordTmp += explrX;
            yCoordTmp += explrY;
            destino = "" + xCoordTmp + ',' + yCoordTmp;
            GameObject celaGameObject = GameObject.Find("cela," + destino);
            if (celaGameObject != null)
            {
                if (tmp.Add(celaGameObject.transform))
                {
                    Debug.Log(destino);
                    float distancia = Vector3.Distance(celaGameObject.transform.position, transformDestino.position);
                    if (metaLocal == null)
                    {
                        metaLocal = celaGameObject.transform;
                        menorDistancia = distancia;
                    }
                    else
                    {
                        if (distancia < menorDistancia)
                        {
                            metaLocal = celaGameObject.transform;
                            menorDistancia = distancia;
                        }
                    }
                }
            }
            xCoordTmp = xCoord;
            yCoordTmp = yCoord;
        }
    }

    public void MostrarCuerpo()
    {
        MeshRenderer cubo = transform.GetChild(0).gameObject.GetComponent<MeshRenderer>();
        cubo.enabled = true;
        cubo = transform.GetChild(1).gameObject.GetComponent<MeshRenderer>();
        cubo.enabled = true;
        visible = true;
        tempoDebug = 1f;
    }
}
