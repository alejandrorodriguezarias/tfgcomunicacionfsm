﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Axente_Humano : MonoBehaviour
{
    NavMeshAgent nav;
    public Cela celaActual { get; set; }
    private string nomeCelaActual { get; set; } = "";
    bool caminar = true;
    public float distAbrirPorta;
    // Start is called before the first frame update
    void Start()
    {
        nav = gameObject.GetComponent<NavMeshAgent>();

    }

void FixedUpdate()
    {
        if (nav.remainingDistance < 1 && !nav.pathPending)
        {
            nav.isStopped = true;
            if (celaActual != null)
            {
                float movementoHorizontal = Input.GetAxis("HorizontalAxente");
                float movementoVertical = Input.GetAxis("VerticalAxente");
                string[] coord = nomeCelaActual.Split(',');
                int XCoord = int.Parse(coord[1]);
                int YCoord = int.Parse(coord[2]);
                //O xogador mira hacia onde se move
                if (movementoHorizontal < 0)
                {
                    YCoord++;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 90, transform.eulerAngles.z);
                }
                else if (movementoHorizontal > 0)
                {
                    YCoord--;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, -90, transform.eulerAngles.z);
                }
                if (movementoVertical > 0)
                {
                    XCoord--;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z);
                }
                else if (movementoVertical < 0)
                {
                    XCoord++;
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
                }

                if ((movementoVertical != 0 || movementoHorizontal != 0) && caminar)
                {
                    //caminar = false;
                    GameObject tmp = GameObject.Find("cela," + XCoord + ',' + YCoord);
                    if (tmp == null)
                    {
                        tmp = GameObject.Find("porta," + XCoord + ',' + YCoord);
                        if (tmp != null)
                        {
                            if (tmp.transform.GetChild(0).gameObject.GetComponent<Script_Porta>().EstaPechada())
                            {
                                tmp = null;
                            }
                        }
                    }
                    //Debug.Log(tmp.name);
                    if (tmp != null)
                    {
                        //transform.position = new Vector3(tmp.transform.position.x, 0.5f, tmp.transform.position.z);
                        nav.destination = new Vector3(tmp.transform.position.x, 0.6f, tmp.transform.position.z);
                        nomeCelaActual = tmp.name;
                        nav.isStopped = false;
                    }
                }
            }

        }
        if (Input.GetButtonDown("Interactuar"))
        {
            AbrirPorta();
        }
    }

    void AbrirPorta()
    {
        RaycastHit impacto;
        //Comprobamos se o xogador esta frente a porta
        if (Physics.Raycast(transform.position + new Vector3(0f,0.5f,0f), transform.TransformDirection(new Vector3(0f, 0.7f, 1f)), out impacto, distAbrirPorta))
        {
            Debug.Log(impacto.collider.gameObject.name);
            if (impacto.collider.gameObject.gameObject.GetComponent<Script_Porta>())
            {
                //abre a porta o a cerra dependendo do estado da porta
                Script_Porta porta = impacto.collider.gameObject.GetComponent<Script_Porta>();
                if (porta.EstaPechada())
                {
                    porta.Abrir();
                }
                else
                {
                    porta.Pechar();
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject obxectoColision = collision.collider.gameObject;

        if (obxectoColision.tag.Equals("cela"))
        {
            if (celaActual != null) {
                if (obxectoColision.transform.gameObject.GetComponent<Cela>() != celaActual)
                {
                    FindObjectOfType<Amanuense>().Cambio(gameObject.name, obxectoColision.transform.gameObject.name);
                }
            }
            celaActual = obxectoColision.transform.gameObject.GetComponent<Cela>();
        }

        if (nomeCelaActual.Equals(""))
        {
            nomeCelaActual = celaActual.name;
        }
    }
}
