﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Amanuense : MonoBehaviour
{
    Dictionary<string, string> posiciones;

    void Start()
    {
        posiciones = new Dictionary<string, string>();
        posiciones.Add("Xogador", "");
        posiciones.Add("FSM0", "");
        posiciones.Add("FSM1", "");
        posiciones.Add("FSM2", "");
        posiciones.Add("AxenteFalso", "");

    }
    public void Cambio(string nombre, string cela) {
        posiciones[nombre] = cela;
        foreach(KeyValuePair<string,string> dato in posiciones) {
            if (dato.Value.Equals("")) {
                Debug.Log(dato.Key);
                return;
            }
        }
        Escribir();
        
    }

    public void Escribir() {
        string path = "Prueba.txt";
        StreamWriter writer = new StreamWriter(path, true);
        string data = posiciones["Xogador"] + "\t" + posiciones["FSM0"] + "\t" + posiciones["FSM1"] + "\t" + posiciones["FSM2"] + "\t" + posiciones["AxenteFalso"];
        writer.WriteLine(data);
        writer.Close();
    }
}
