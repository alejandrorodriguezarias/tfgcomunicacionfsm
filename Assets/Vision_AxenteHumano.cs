﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision_AxenteHumano : Script_CampoVision
{
    protected override void comportamentoVerObxetivo(List<Transform> listaObxectivos)
    {
        foreach (Transform obxectivo in listaObxectivos)
        {
            if (obxectivo.gameObject.name.Equals("Xogador"))
            {
                Ladron xogador = obxectivo.gameObject.GetComponent<Ladron>();
                xogador.MostrarCuerpo();
            }
        }
    }
}
