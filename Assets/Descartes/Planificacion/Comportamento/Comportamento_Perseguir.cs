﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_Perseguir : MonoBehaviour
{
    private Axente_FSM axente;
    private NavMeshAgent IaAxente;
    private List<string> nomesEquipo;
    private float tempoSenVerXogador;
    private const float MaximoSenVerXogador = 3f;
    private bool enPosicion = true;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM>();
        IaAxente = axente.GetComponent<NavMeshAgent>();
        IaAxente.destination = axente.ObterPosicionXogadorCoord();
        IaAxente.isStopped = false;
        IaAxente.speed = 8.5f;
        nomesEquipo = axente.ObterNomesEquipo();
        tempoSenVerXogador = MaximoSenVerXogador;
        Axente_FSM receptor;
        //Informa ao equipo de que toca perseguir en conxunto
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Axente_FSM>();
            receptor.DispararEvento((int)Eventos.EventoEquipoPerseguir);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tempoSenVerXogador < 0)
        {
            axente.DispararEvento((int)Eventos.EventoXogadorPerdido);
        }
        else {
            tempoSenVerXogador -= Time.deltaTime;
        }
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar() {
        Destroy(this);
    }

    /// <summary>
    /// Recibe a posicion do xogador doutro axente
    /// </summary>
    /// <param name="posXogador">Vector3 Posicion do xogador</param>
    public void TratarInformXogador(Vector3 posXogador) {
        if (IaAxente != null) {
            IaAxente.destination = posXogador;
            tempoSenVerXogador = MaximoSenVerXogador;
        }
    }
    /// <summary>
    /// Se ve ao xogador actualiza a sua posicion e informa a todo o equipo
    /// </summary>
    /// <param name="posXogador">Vector3 posicion do xogador</param>
    public void VerXogador(Vector3 posXogador) {
        if (IaAxente != null)
        {
            IaAxente.destination = posXogador;
            tempoSenVerXogador = MaximoSenVerXogador;
            Script_Comunicacion receptor;
            foreach (string nome in nomesEquipo)
            {
                receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                axente.EnviarMensaxe("inform", "xogadorCoord\\" + posXogador.x + "\\" + posXogador.y + "\\" + posXogador.z, gameObject.name, nome, receptor, "-1");
            }
        }  
    }

    /// <summary>
    /// Se a mensaxe query pregunta se o axente esta en posicion se responde.
    /// </summary>
    /// <param name="contido">pregunta da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    public void TratarQuery(string contido, string remitente, string id)
    {
        if (contido.Equals("enPosicion"))
        {
            Script_Comunicacion receptor;
            receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "enPosicion" + "\\" + enPosicion.ToString(), gameObject.name, remitente, receptor, id);
        }
    }

}

public class Invocar_Perseguir : Comportamento
{
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_Perseguir>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        Comportamento_Perseguir componente = gameObject.AddComponent<Comportamento_Perseguir>() as Comportamento_Perseguir;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}
