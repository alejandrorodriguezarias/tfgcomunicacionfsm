﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comportamento_FormarEquipo : MonoBehaviour{

    //Axente propietario do script
    private Axente_FSM axente;
    private bool solicitudesEnviadas = false;
    //lista dos axentes que se unen ao equipo
    private List<string> nomesEquipo = new List<string>();
    private const float TempoReenvio = 0.8f;
    private float tempoUltimaComprobacion = TempoReenvio;

    /// <summary>
    /// Obtemos o axente propietario do script e comeza o seu comportamento
    /// </summary>
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM>();
        axente.EstablecerNomesEquipo(new List<string>());
        axente.EstablecerConversas( new List<Conversa>());
        nomesEquipo.Add(gameObject.name);
        SolicitarEquipo();
    }

    /// <summary>
    /// Cada frame comproba se xa se enviaron as mensaxes e se foron respondidas
    /// </summary>
    void Update()
    {
        if (solicitudesEnviadas) {
            if (ComprobarRespostas())
            {
                solicitudesEnviadas = false;
                axente.EstablecerNomesEquipo(nomesEquipo);
                axente.EstablecerLiderEquipo(gameObject.name);
                InformarEquipo();
                axente.DispararEvento((int)Eventos.EventoEquipoFormado);

            }
            else if (tempoUltimaComprobacion < 0)
            {
                ReenviarMensaxes();
            }
            else {
                tempoUltimaComprobacion -= Time.deltaTime;
            }
        }
    }

    
    /// <summary>
    /// Pregunta a todos os axentes quen esta dispoñible para o equipo
    /// </summary>
    private void SolicitarEquipo() {
        Script_Comunicacion[] patrullas = axente.ObterAxentes();
        foreach (Script_Comunicacion patrulla in patrullas)
        {
            //Non queremos que se envie o mensaxe a el mesmo
            if (!gameObject.name.Equals(patrulla.name)) {
                axente.EnviarMensaxe("request", "equipo", gameObject.name, patrulla.name, patrulla, "-1");
            }
            
        }
        solicitudesEnviadas = true;
    }
    /// <summary>
    /// Os axentes que acepten a invitación rexistranse no equipo
    /// </summary>
    /// <param name="remitente">o nome do axente que acepta a invitacion</param>
    public void TratarAccept(string remitente) {
        nomesEquipo.Add(remitente);
    }

    /// <summary>
    /// Comproba se todos os axentes responderon á mensaxe
    /// </summary>
    /// <returns>True en caso afirmativo e false en caso negativo</returns>
    private bool ComprobarRespostas() {
        bool respostasRecibidas = true;
        List<Conversa> listaConversas = axente.ObterConversas();
        foreach (Conversa conversa in listaConversas) {
            if (conversa.GetMensaxe().GetContido().Equals("equipo")) {
                respostasRecibidas = false;
                break;
            }
        }
        return respostasRecibidas;
    }
    /// <summary>
    /// Envia unha mensaxe a todos os membros do equipo coa lista dos membros do equipo
    /// </summary>
    private void InformarEquipo() {
        Script_Comunicacion receptor;
        string mensaxe = "equipo" + "\\";
        foreach (string nome in nomesEquipo){
            mensaxe = mensaxe + nome + ",";
        }
        foreach (string nome in nomesEquipo) {
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", mensaxe, gameObject.name, nome, receptor, "-1");
        }
    }

    private void ReenviarMensaxes()
    {
        List<Conversa> listaConversas = axente.ObterConversas();
        List<Conversa> listaConversasAux = new List<Conversa>(listaConversas);
        foreach (Conversa conversa in listaConversasAux)
        {
            if (conversa.GetMensaxe().GetContido().Equals("equipo"))
            {
                Script_Comunicacion receptor = GameObject.Find(conversa.GetNomeReceptor()).GetComponent<Script_Comunicacion>();
                listaConversas.Remove(conversa);
                axente.EnviarMensaxe("request", "equipo" , gameObject.name, conversa.GetNomeReceptor(), receptor, "-1");
                Debug.Log("ALERTA");

            }
        }
    }


    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar() {
        //axente.DebugEquipo();
        Destroy(this);
    }
}
public class Invocar_FormarEquipo : Comportamento
{

    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_FormarEquipo>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        Comportamento_FormarEquipo componente = gameObject.AddComponent<Comportamento_FormarEquipo>() as Comportamento_FormarEquipo;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}