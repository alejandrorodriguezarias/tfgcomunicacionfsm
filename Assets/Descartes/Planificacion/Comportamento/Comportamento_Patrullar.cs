﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_Patrullar : MonoBehaviour{
    NavMeshAgent axente;
    private Transform[] puntos;
    private int puntoDestino;

    /// <summary>
    /// La función start se invoca antes del primer update
    /// Obtiene unha referencia ao NavMeshAngent e desactiva o autoBraking
    /// Obten os puntos de patrulla do axente
    /// </summary>
    void Start()
    {
        axente = GetComponent<NavMeshAgent>();

        //Desactivar autoBraking permite un movemento continuo entre puntos
        axente.autoBraking = false;
        puntos = gameObject.GetComponent<Axente_FSM>().ObterRutaPatrulla();
        IrPuntoSeguinte(true);
    }
    /// <summary>
    /// Este metodo encargase de obter a proxima coordenada da patrulla e ordenar ao axente moverse ata ese punto
    /// </summary>
    /// <param name="inicio">Se estamos no inicio o puntoDestino é 0 pero non rematou a patrulla</param>
    void IrPuntoSeguinte(bool inicio)
    {
        bool fin = false;
        // Se non existen puntos devolvemos unha excepcion
        if (puntos.Length == 0)
        {
            throw new Exception("É necesario ter algun punto de patrulla");
        }
        //O destino do axente e o proximo punto
        axente.destination = puntos[puntoDestino].position;

        //Mira se é o final da ronda
        if (puntoDestino == 0 && (!inicio))
        {
            fin = true;
        }
        // Elixe o seguinte punto no array,
        // Regresa o inicio do array se e necesario
        puntoDestino = (puntoDestino + 1) % puntos.Length;
        //Ao final de cada ronda espera un pouco
        if (fin)
        {
            axente.isStopped = true;
            StartCoroutine("RetomarPatrulla", 2f);
        }

    }
    /// <summary>
    /// Cando o axente termina a patrulla tomase un pequeño descanso antes de volver a empezar
    /// </summary>
    /// <param name="retraso">duracion da parada do axente</param>
    /// <returns></returns>
    IEnumerator RetomarPatrulla(float retraso)
    {
        yield return new WaitForSeconds(retraso);
        axente.isStopped = false;
    }


    void Update()
    {
        // Cando se chega ao punto elixese o seguinte.
        if (!axente.pathPending && axente.remainingDistance < 0.5f)
        {
            IrPuntoSeguinte(false);
            
        }
    }
    /// <summary>
    /// Borra este script do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }
}

public class Invocar_Patrullar : Comportamento {

    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_Patrullar>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        Comportamento_Patrullar componente = gameObject.AddComponent<Comportamento_Patrullar>() as Comportamento_Patrullar;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}

