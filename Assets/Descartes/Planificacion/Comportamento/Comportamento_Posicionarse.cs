﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_Posicionarse : MonoBehaviour
{
    private Axente_FSM axente;
    private NavMeshAgent IaAxente;
    private bool enPosicion = false;
    private const float TempoEntreComprobacions = 0.2f;
    private float tempoComprobacions;
    private List<string> nomesEquipo;
    private Dictionary<string, bool> respostasEquipoPosicion;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM>();
        IaAxente = axente.GetComponent<NavMeshAgent>();
        nomesEquipo = axente.ObterNomesEquipo();
        respostasEquipoPosicion = new Dictionary<string,bool>();
        IaAxente.isStopped = false;
        Dictionary<string, Transform> centroHabitacions = axente.ObterCentroHabitacions();
        foreach(KeyValuePair<string,Transform> centro in centroHabitacions)
        {
            if (IaAxente.destination.x == centro.Value.position.x  && IaAxente.destination.z == centro.Value.position.z) {
                enPosicion = true;
                break;
            }
        }
        IaAxente.speed = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IaAxente.pathPending)
        {
            if (IaAxente.remainingDistance < 1)
            {
                Transform centroHabitacion = GameObject.Find(axente.ObterPosicionXogador()).transform;
                axente.transform.LookAt(centroHabitacion);
                IaAxente.isStopped = true;
                enPosicion = true;
            }
        }
        if (enPosicion) {
            if (tempoComprobacions < 0){
                PreguntarEquipo();
                tempoComprobacions = TempoEntreComprobacions;
            }else {
                tempoComprobacions -= Time.deltaTime;
            }
            if (ComprobarEquipo()) {
                Script_Comunicacion receptor;
                foreach (string nome in nomesEquipo)
                {
                    receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                    axente.EnviarMensaxe("inform", "enPosicion\\True", gameObject.name, nome, receptor, "-1");
                }
                axente.DispararEvento((int) Eventos.EventoEquipoEnPosicion);
            }
        }
    }

    /// <summary>
    /// Pregunta a posición do resto do equipo
    /// </summary>
    private void PreguntarEquipo()
    {
        Script_Comunicacion receptor;
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("query", "enPosicion", gameObject.name, nome, receptor, "-1");
        }
    }
    /// <summary>
    /// Comproba as respostas de todo o equipo a pregunta de se estan en posicion
    /// </summary>
    /// <returns>true se todos estan en posicion false en caso contrario</returns>
    private bool ComprobarEquipo() {
        bool posicionEquipo = false;
        if (respostasEquipoPosicion.Count == nomesEquipo.Count-1) {
            posicionEquipo = true;
            foreach (KeyValuePair<string,bool> resposta in respostasEquipoPosicion) {
                if (resposta.Value == false) {
                    posicionEquipo = false;
                }
            }
        }
        return posicionEquipo;
    }
    /// <summary>
    /// Cando se recibe un inform sobre se un xogador esta en posicion almacenase
    /// </summary>
    /// <param name="contido">contido da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    public void TratarInform(string contido, string remitente) {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("enPosicion")){
            try
            {
                respostasEquipoPosicion.Add(remitente, contidoDiv[1].Equals("True") ? true : false);
            }
            catch (ArgumentException) {
                respostasEquipoPosicion[remitente] = contidoDiv[1].Equals("True") ? true : false;
            }
        }
    }
    /// <summary>
    /// Se a mensaxe query pregunta se o axente esta en posicion se responde.
    /// </summary>
    /// <param name="contido">pregunta da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    public void TratarQuery(string contido, string remitente, string id) {
        if (contido.Equals("enPosicion")){
            Script_Comunicacion receptor;
            receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "enPosicion" + "\\" + enPosicion.ToString(), gameObject.name, remitente, receptor, id);
        }
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar() {
        Destroy(this);
    }
}

public class Invocar_Posicionarse : Comportamento
{
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_Posicionarse>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        Comportamento_Posicionarse componente = gameObject.AddComponent<Comportamento_Posicionarse>() as Comportamento_Posicionarse;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}
