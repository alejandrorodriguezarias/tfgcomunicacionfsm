﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_Planificar : MonoBehaviour
{
    //Axente propietario do script
    private Axente_FSM axente;
    private NavMeshAgent IaAxente;
    private List<string> nomesEquipo;
    private bool comprobarMensaxes;
    private bool manexarDistancias;
    private float tempoComprobacions;
    private const float TempoEntreComprobacions = 0.1f;
    private Dictionary<string,float> distancias = new Dictionary<string,float>();
    private Dictionary<string, Dictionary<string, float>> distanciasEquipo = new Dictionary<string, Dictionary<string, float>>();
    List<Transform> entradas;
    
    //Prueba
    bool calcularDistancias = false;
    int iteradorDistancias = 0;
    Transform entradaActual = null;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM>();
        IaAxente = axente.GetComponent<NavMeshAgent>();
        Dictionary<string, List<Transform>> escenario = new Dictionary<string, List<Transform>>(axente.ObterEscenario());
        entradas = new List<Transform>(escenario[axente.ObterPosicionXogador()]);
        nomesEquipo = new List<string>(axente.ObterNomesEquipo());
        tempoComprobacions = TempoEntreComprobacions;
        ComprobarEquipo();
}

    // Update is called once per frame
    void Update()
    {
        if (comprobarMensaxes) {
            if (tempoComprobacions <= 0) {
                if (MesmoEstado())
                {
                    comprobarMensaxes = false;
                    tempoComprobacions = TempoEntreComprobacions;
                    //Planificar();

                    //Prueba
                    calcularDistancias = true;
                    IaAxente.isStopped = true;
                }
                else {
                    comprobarMensaxes = false;
                    tempoComprobacions = TempoEntreComprobacions;
                    LimparConversas();
                    ComprobarEquipo();
                }
            } 
            else {
                tempoComprobacions -= Time.deltaTime;
            }
        }
        if (manexarDistancias) {
            if (tempoComprobacions <= 0)
            {
                manexarDistancias = false;
                PedirDestino();
                tempoComprobacions = TempoEntreComprobacions;
            }
            else {
                tempoComprobacions -= Time.deltaTime;
            }
        }

        if (calcularDistancias) {
            entradaActual = entradas[iteradorDistancias];
            IaAxente.destination = entradaActual.position;
            if (!IaAxente.pathPending) {
                distancias.Add(entradaActual.gameObject.name, Vector3.Distance(gameObject.transform.position, entradaActual.position));
                if (iteradorDistancias < entradas.Count - 1)
                {
                    iteradorDistancias += 1;
                }
                else {
                    calcularDistancias = false;
                    iteradorDistancias = 0;
                    Planificar();
                }
            }
        }
    }
    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    /// <summary>
    /// Pregunta ao resto dos axentes do equipo se estan no estado planificar
    /// </summary>
    private void ComprobarEquipo() {
        Script_Comunicacion receptor;
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("query", "estadoPlanificar", gameObject.name, nome, receptor, "-1");
        }
        comprobarMensaxes = true;
    }
    /// <summary>
    /// Comproba se todos os axentes responderon ao mensaxe do estado recorrendo as conversas pendentes
    /// </summary>
    /// <returns>true se todos os axentes estan no estado planificar, false noutro caso</returns>
    private bool MesmoEstado() {
        bool mesmoEstado = true;
        List<Conversa> listaConversas = axente.ObterConversas();
        foreach (Conversa conversa in listaConversas)
        {
            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar")) {
                mesmoEstado = false;
                break;
            }
        }
        return mesmoEstado;
    }
    /// <summary>
    /// Comportamento cando recibese unha mensaxe query, se esta e unha pregunta por estado planificar respondese true
    /// </summary>
    /// <param name="contido">contido da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    public void TratarQuery(string contido, string remitente, string id) {

        if (contido.Equals("estadoPlanificar")){
            Script_Comunicacion receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            if (axente != null) {
                axente.EnviarMensaxe("inform", "estadoPlanificar\\true", gameObject.name, remitente, receptor, id);
            }
            
        }
    }
    /// <summary>
    /// Antes de reenviar as preguntas do estado limpanse das conversas as que quedaran colgando
    /// </summary>
    private void LimparConversas() {
        List<Conversa> listaConversasAux = axente.ObterConversas();
        List<Conversa> listaConversas = new List<Conversa>(axente.ObterConversas());
        foreach (Conversa conversa in listaConversasAux)
        {
            if (conversa.GetMensaxe().GetContido().Equals("estadoPlanificar"))
            {
                listaConversas.Remove(conversa);
            }
        }
        axente.EstablecerConversas(listaConversas);
    }
    /// <summary>
    /// Funcionalidad principal do comportamento
    /// </summary>
    private void Planificar() {
        //CalcularDistanciaPuntos();
        InformarSobreDistancias();
    }
    /// <summary>
    /// Calcula a distancia do axente a todas as entradas da habitacion do xogador
    /// </summary>
    private void CalcularDistanciaPuntos() {  
        IaAxente.isStopped = true;
        List<Transform> entradasAux = new List<Transform>(entradas);
        foreach (Transform entrada in entradasAux) {
            //IaAxente.destination = entrada.position;
            //NO CALCULA DISTANCIA REAL
            //Debug.Log("CALCULAR DISTANCIA PUNTOS " + gameObject.name + entrada.position + "DISTANCIA" + Vector3.Distance(gameObject.transform.position, entrada.position));
            distancias.Add(entrada.gameObject.name, Vector3.Distance(gameObject.transform.position, entrada.position));
        }
    }
    /// <summary>
    /// Comunica ao resto do equipo a sua posicion respecto as entradas
    /// </summary>
    private void InformarSobreDistancias() {
        Script_Comunicacion receptor;
        string contido = "distancias";
        foreach (KeyValuePair<string, float> distancia in distancias)
        {
            contido +=  "\\" + distancia.Key + "\\" + distancia.Value;
        }
        foreach (string nome in nomesEquipo){
            receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", contido, gameObject.name, nome, receptor, "-1");
        }
        manexarDistancias = true;
    }

    /// <summary>
    /// Se os axentes informan das suas distancias almacenanse, se un axente solicita unha entrada a eliminamos das posiblidades deste axente
    /// </summary>
    /// <param name="contido">contido da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    public void TratarInform(string contido, string remitente) {
        Dictionary<string, float> distancias = new Dictionary<string, float>();
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("distancias"))
        {
            for (int i = 1; i < contidoDiv.Length; i += 2)
            {
                distancias.Add(contidoDiv[i], float.Parse(contidoDiv[i + 1]));
            }
            distanciasEquipo.Add(remitente, distancias);
        }
        else if (contidoDiv[0].Equals("eleccion")) {
            distanciasEquipo.Remove(remitente);
            nomesEquipo.Remove(remitente);
            foreach (Transform entrada in entradas) {
                if (entrada.name.Equals(contidoDiv[1])) {
                    entradas.Remove(entrada);
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Selecciona o mellor destino posible para o axente (o mais curto) e o comunica ao resto do equipo
    /// </summary>
    public void PedirDestino() {
        List<Tuple<string, float>> melloresDestinos;
        string mellorDestino = null;
        float menorDistancia = -1;
        if (distanciasEquipo.Count == nomesEquipo.Count - 1)
        {
            melloresDestinos = ObterMelloresDestinos();
            if (melloresDestinos != null) {
                if (melloresDestinos.Count > 0)
                {
                    foreach (Tuple<string, float> destino in melloresDestinos)
                    {
                        if ((menorDistancia == -1) || (menorDistancia > destino.Item2))
                        {
                            mellorDestino = destino.Item1;
                            menorDistancia = destino.Item2;
                        }
                    }
                    if (mellorDestino != null)
                    {
                        foreach (string nome in nomesEquipo)
                        {
                            Script_Comunicacion receptor = GameObject.Find(nome).GetComponent<Script_Comunicacion>();
                            axente.EnviarMensaxe("inform", "eleccion\\" + mellorDestino, gameObject.name, nome, receptor, "-1");

                            Dictionary<string, List<Transform>> escenario = axente.ObterEscenario();
                            foreach (Transform entrada in entradas)
                            {
                                if (entrada.gameObject.name.Equals(mellorDestino))
                                {
                                    IaAxente.destination = entrada.position;
                                    axente.DispararEvento((int)Eventos.EventoFinPlanificar);
                                }
                            }
                        }
                    }
                }
                else
                {
                    manexarDistancias = true;
                }
            }
        }
        else {
           manexarDistancias = true;
        }
    }
    /// <summary>
    /// Funcion auxiliar que calcula as entradas mais cercanas ao axente que ao resto do equipo
    /// </summary>
    /// <returns>A lista coas entradas cercanas</returns>
    public List<Tuple<string,float>> ObterMelloresDestinos() {
        List<Tuple<string, float>> melloresDestinos = new List<Tuple<string, float>>();
        bool menor;
        Dictionary<string, Dictionary<string, float>> distanciasEquipo = new Dictionary<string, Dictionary<string, float>>(this.distanciasEquipo);
        ActualizarDistancias();
        Dictionary<string, float> distancias = new Dictionary<string, float>(this.distancias);
        if (distancias.Count == 0) {
            Transform habitacion = axente.ObterCentroHabitacions()[axente.ObterPosicionXogador()];
            IaAxente.destination = habitacion.position;
            axente.DispararEvento((int)Eventos.EventoFinPlanificar);
            return null;
        }
        foreach (KeyValuePair<string, float> distancia in distancias)
        {
            menor = true;
            foreach (KeyValuePair<string, Dictionary<string, float>> distAxente in distanciasEquipo) {
                if (distAxente.Value[distancia.Key] < distancia.Value) {
                    menor = false;
                    //if ((menorDistancia > distAxente.Value[distancia.Key]) || menorDistancia == -1) {
                        //menorDistancia = distAxente.Value[distancia.Key];
                    //}
                }
            }
            if (menor) {
                //diferencia = menorDistancia - distancia.Value;
                //Debug.Log(gameObject.name + " " + distancia.Key);
                melloresDestinos.Add(new Tuple<string, float>(distancia.Key,distancia.Value)); //CAMBIAR para que vaya al menor de las suyas
            }
        }
        return melloresDestinos;
    }
    /// <summary>
    /// Actualiza as distancias do axente eliminando aqueles puntos que foron seleccionados por outro axente
    /// </summary>
    public void ActualizarDistancias() {
        bool borrar;
        Dictionary<string, float> distanciasAux = new Dictionary<string, float>(distancias);
        if (distancias.Count != entradas.Count){
            foreach (KeyValuePair<string, float> distancia in distanciasAux){
                borrar = true;
                foreach (Transform entrada in entradas) {
                    if (distancia.Key.Equals(entrada.name)) {
                        borrar = false;
                    }
                }
                if (borrar){
                    distancias.Remove(distancia.Key);
                }
            }
        }

    }
}
public class Invocar_Planificar : Comportamento
{
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_Planificar>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        Comportamento_Planificar componente = gameObject.AddComponent<Comportamento_Planificar>() as Comportamento_Planificar;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new NotImplementedException();
    }
    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}
