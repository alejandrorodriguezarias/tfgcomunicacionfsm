﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Comportamento_XogadorPerdido : MonoBehaviour
{
    private Axente_FSM axente;
    private NavMeshAgent IaAxente;
    private float cambioDirBusqueda = 0f;
    private float radioBusqueda = 15;
    private const float TempoDirBusqueda = 15.0f;
    private bool enPosicion = true;
    // Start is called before the first frame update
    void Start()
    {
        axente = gameObject.GetComponent<Axente_FSM>();
        IaAxente = axente.GetComponent<NavMeshAgent>();
        List<string> nomesEquipo = axente.ObterNomesEquipo();
        Axente_FSM receptor;
        foreach (string nome in nomesEquipo)
        {
            receptor = GameObject.Find(nome).GetComponent<Axente_FSM>();
            receptor.DispararEvento((int)Eventos.EventoEquipoPerseguir);
        }
    }

    // Update is called once per frame
    void Update()
    {
        AtoparXogador();
    }

    /// <summary>
    /// Funcion de control para os cambios de direccion do axente, gradualmente vai aumentando o radio de busqueda
    /// </summary>
    private void AtoparXogador()
    {
        if ((IaAxente.remainingDistance < 1) || (cambioDirBusqueda <= 0))
        {
            //se podría aumentar la dir de busqueda a cada cambio de dirección
            Vector3 puntoAleatorio = DirAleatoria(radioBusqueda);
            radioBusqueda += 0.5f;
            cambioDirBusqueda = TempoDirBusqueda;
            IaAxente.destination = puntoAleatorio;
        }
        else
        {
            cambioDirBusqueda = cambioDirBusqueda - Time.deltaTime;
        }
    }
    /// <summary>
    /// Obten un punto aleatorio dentro dun radio determinado
    /// </summary>
    /// <param name="radio">radio de busqueda</param>
    /// <returns></returns>
    private Vector3 DirAleatoria(float radio)
    {
        //Punto aleatorio dunha esfera
        Vector3 puntoAleatorio = Random.insideUnitSphere * radio;
        //A centramos o seu orixe no axente
        puntoAleatorio += axente.ObterPosicionXogadorCoord();
        NavMeshHit puntoNavMesh;
        //Nav mesh e en 2d mentras que Vector3 e en 3d necesitamos encontrar o punto mais cercano ao 3d en 2d
        NavMesh.SamplePosition(puntoAleatorio, out puntoNavMesh, radio, -1);

        return puntoNavMesh.position;
    }

    /// <summary>
    /// Elimina o comportamento do axente
    /// </summary>
    public void Eliminar()
    {
        Destroy(this);
    }

    /// <summary>
    /// Se a mensaxe query pregunta se o axente esta en posicion se responde.
    /// </summary>
    /// <param name="contido">pregunta da mensaxe</param>
    /// <param name="remitente">nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    public void TratarQuery(string contido, string remitente, string id)
    {
        if (contido.Equals("enPosicion"))
        {
            Script_Comunicacion receptor;
            receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
            axente.EnviarMensaxe("inform", "enPosicion" + "\\" + enPosicion.ToString(), gameObject.name, remitente, receptor, id);
        }
    }
}

public class Invocar_XogadorPerdido : Comportamento
{
    public override void Ejecutar()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Borra o script de comportamento do axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Fin(GameObject gameObject)
    {
        gameObject.GetComponent<Comportamento_XogadorPerdido>().Eliminar();
    }
    /// <summary>
    /// Engade o script de comportamento ao axente
    /// </summary>
    /// <param name="gameObject">o axente</param>
    public override void Inicio(GameObject gameObject)
    {
        Comportamento_XogadorPerdido componente = gameObject.AddComponent<Comportamento_XogadorPerdido>() as Comportamento_XogadorPerdido;
    }

    public override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarInform(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    public override void VerXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }

    public override void EscoitarXogador(Cela posXogador, string habitacionXogador)
    {
        throw new System.NotImplementedException();
    }
}
