﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Script_CampoVision : MonoBehaviour {

    public float radioVision;
    [Range(0,360)]
    public float anguloVision;

    public LayerMask MascaraObxetivos;
    public LayerMask MascaraObstaculos;

    public MeshFilter filtroMallaVision;
    private Mesh mallaVision;

 

    public float resolucionMalla;
    public int iteracionesResolverBorde;

    public List<Transform> obxetivosVisibles = new List<Transform>();

    void Start(){
        mallaVision = new Mesh();
        mallaVision.name = "Malla Vision";
        filtroMallaVision.mesh = mallaVision; 
        StartCoroutine ("EncontrarObxetivosRetraso", .2f);
    }

    //Cada milisegundo redebuxa o campo de vision usase LateUpdate para que se actualice despois do axente
    void LateUpdate() {
        DebuxarCampoVision();
    }
    //Metodo que chama ao metodo de encontrar obxetivos visibles cun pequeno retraso
    IEnumerator EncontrarObxetivosRetraso(float retraso){
        while(true){
            yield return new WaitForSeconds(retraso);
            EncontrarObxetivosVisibles();
        }
    }

    
    //Metodo que comproba se existen obxetivos visibles para o axente.
    void EncontrarObxetivosVisibles(){
        obxetivosVisibles.Clear();
        Collider[] ObxetivosNoRadioVista = Physics.OverlapSphere( transform.position, radioVision, MascaraObxetivos);
        for (int i = 0 ; i < ObxetivosNoRadioVista.Length; i++){
            Transform obxetivo = ObxetivosNoRadioVista[i].transform;
            //Angulo entre a dir de vista do axente e o seu obxetivo
            Vector3 dirObxetivo = (obxetivo.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirObxetivo)< anguloVision/ 2){
                //Comprobamos se existe un obstaculo no camino
                float distObxetivo = Vector3.Distance(transform.position, obxetivo.position);
                if (!Physics.Raycast(transform.position, dirObxetivo, distObxetivo, MascaraObstaculos))
                {
                    obxetivosVisibles.Add(obxetivo);
                }
                else {
                    //Se existe un obstaculo entre o obxetivo e o axente comprobamos que o obstaculo non sexa o propio obxectivo
                    RaycastHit colision;
                    Ray rayo = new Ray(transform.position, dirObxetivo);
                    if (Physics.Raycast(rayo, out colision, distObxetivo))
                    {
                        if (colision.collider.gameObject.name == obxetivo.gameObject.name)
                        {
                            obxetivosVisibles.Add(obxetivo);
                        }
                    }
                }
            }
        }
        comportamentoVerObxetivo(obxetivosVisibles);
    }


    void DebuxarCampoVision(){
        int contadorRayos = Mathf.RoundToInt(anguloVision * resolucionMalla);
        float tamanoAnguloIteracion = anguloVision / contadorRayos;
        List<Vector3> puntosVista = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();
        for(int i = 0; i <= contadorRayos; i++){
            //transform.eulerAngles.y rotacion do obxeto 
            //Calculase o  angulo xusto no punto no que lle toca debuxar
            float angulo = transform.eulerAngles.y - anguloVision / 2 + tamanoAnguloIteracion * i;
            //Debug.DrawLine(transform.position, transform.position + DirFromAngle (angle, true) * viewRadius, Color.red);
            ViewCastInfo newViewCast = ViewCast(angulo);
            //Primera iteracion o oldViewCast non estara declarado
            if (i>0){
                if (oldViewCast.impacto != newViewCast.impacto){
                    BordeInfo borde = EncontrarBorde (oldViewCast, newViewCast);
                    if (borde.puntoA != Vector3.zero){
                        puntosVista.Add(borde.puntoA);
                    }
                    if (borde.puntoB != Vector3.zero){
                        puntosVista.Add(borde.puntoB);
                    }
                }
            }
            puntosVista.Add(newViewCast.punto);
            oldViewCast = newViewCast;
        }
        //Numero vertices = puntosVista + punto origen
        int numeroVertices = puntosVista.Count + 1;
        Vector3[] vertices = new Vector3[numeroVertices];
        //indices dos vertices
        int[] triangulos = new int[(numeroVertices-2) * 3];

        //punto origen
        vertices[0] = new Vector3(0f,0.1f,0f);
        //iteramos por todos os vertices restantes
        for (int i=0; i < numeroVertices -1 ; i++){
            //Convertimos os vertices a posiciones locales
            vertices[i+1] = transform.InverseTransformPoint(puntosVista [i]);

            if (i< numeroVertices-2){
                //asignamos os vertices de cada triangulo (origen,a,b)
                triangulos[i*3] = 0;
                triangulos[i*3 + 1] = i + 1;
                triangulos[i*3 + 2] = i + 2;
            }
        }
        mallaVision.Clear();
        mallaVision.vertices = vertices;
        mallaVision.triangles = triangulos;
        mallaVision.RecalculateNormals();

    }

    //Angulos en unity no sentido do reloxo e o 0 no Norte 0-90-180-270
    public Vector3 DirAngulo(float anguloGrados, bool anguloGlobal){
        if(!anguloGlobal){
            anguloGrados += transform.eulerAngles.y;
        }
    //Vector3 recibe x,y,z como parametros
        return new Vector3(Mathf.Sin(anguloGrados * Mathf.Deg2Rad),0.1f,Mathf.Cos(anguloGrados*Mathf.Deg2Rad));
    }

    ViewCastInfo ViewCast(float anguloGlobal) {
        Vector3 dir = DirAngulo(anguloGlobal , true);
        RaycastHit impacto;

        if(Physics.Raycast(transform.position, dir , out impacto, radioVision, MascaraObstaculos)){
            return new ViewCastInfo(true, impacto.point, impacto.distance, anguloGlobal);
        }else {
            return new ViewCastInfo(false, transform.position + dir * radioVision, radioVision, anguloGlobal);
        }
    }

    BordeInfo EncontrarBorde(ViewCastInfo minViewCast, ViewCastInfo maxViewCast){
        float anguloMin = minViewCast.angulo;
        float anguloMax = maxViewCast.angulo;
        Vector3 puntoMin = Vector3.zero;
        Vector3 puntoMax = Vector3.zero;

        for (int i = 0; i< iteracionesResolverBorde; i++){
            float angulo = (anguloMin+anguloMax)/2;
            ViewCastInfo newViewCast = ViewCast(angulo);

            if (newViewCast.impacto == minViewCast.impacto){
                anguloMin = angulo;
                puntoMin = newViewCast.punto;

            }else{
                anguloMax = angulo;
                puntoMax = newViewCast.punto;
            }
        }
        return new BordeInfo(puntoMin,puntoMax);
    }



    public struct ViewCastInfo {
        public bool impacto;
        public Vector3 punto;
        public float dist;
        public float angulo;

        public ViewCastInfo(bool _impacto, Vector3 _punto, float _dist, float _angulo){
            impacto = _impacto;
            punto = _punto;
            dist = _dist;
            angulo = _angulo;
        }
    }

    public struct BordeInfo {
        public Vector3 puntoA;
        public Vector3 puntoB;

        public BordeInfo(Vector3 _puntoA,Vector3 _puntoB){
            puntoA = _puntoA;
            puntoB = _puntoB;
        }
    }

    //Trata a vision dun obxetivo
    abstract protected void comportamentoVerObxetivo(List<Transform> contido);
}
