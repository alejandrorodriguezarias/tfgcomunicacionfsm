﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision_AxenteFSM : Script_CampoVision
{
    protected override void comportamentoVerObxetivo(List<Transform> listaObxectivos)
    {
        foreach (Transform obxectivo in listaObxectivos)
        {
            if (obxectivo.gameObject.name.Equals("Xogador"))
            {
                GetComponent<Axente_FSM>().VerXogador(obxectivo, obxectivo.gameObject.GetComponent<Script_ControladorXogador>().habitacionActual);
            }
        }
    }
}
