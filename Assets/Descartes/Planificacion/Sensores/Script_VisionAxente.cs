﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_VisionAxente : Script_CampoVision
{
    protected override void comportamentoVerObxetivo(List<Transform> listaObxectivos)
    {
        foreach (Transform obxectivo in listaObxectivos)
        {
            if (obxectivo.gameObject.name.Equals("Xogador"))
            {
                GetComponent<Script_Axente>().VerXogador(obxectivo);
            }
        }
    }
}
