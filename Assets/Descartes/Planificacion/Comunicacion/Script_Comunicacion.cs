﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class Script_Comunicacion: MonoBehaviour{
	// Conversas abertas cos diferentes axentes
	protected List<Conversa> listaConversas = new List<Conversa>();
	//Flags de resposta

    /// <summary>
    /// Crea unha mensaxe
    /// </summary>
    /// <param name="performativa">String tipo da performativa a enviar </param>
    /// <param name="contido">String contido da mensaxe</param>
    /// <param name="remitente">String nome de quen envía a mensaxe</param>
    /// <param name="nomeReceptor">String a quen vai dirixida a mensaxe</param>
    /// <param name="id">string id da conversa se é unha resposta ou -1 noutro caso</param>
    /// <returns>Unha instancia de clase Mensaxe cos datos adecuados</returns>
    private Mensaxe CrearMensaxe(string performativa, string contido, string remitente, string nomeReceptor, string id){
        Mensaxe tmp = null;
        String posibleResposta = null;
        bool resposta = id.Equals("-1") ? false : true;

        if (performativa !=null){      
			switch(performativa){
                case "inform":
					tmp = new Inform(contido,remitente,nomeReceptor,id);
                    break;
				case "query":
					tmp =  new Query(contido,remitente,nomeReceptor,id);
                    posibleResposta = "inform";
                    break;
				case "request":
					tmp =  new Request(contido,remitente,nomeReceptor,id);
                    posibleResposta = "accept\\refuse";
                    break;
				case "accept":
					tmp = new Accept(contido,remitente,nomeReceptor,id);
                    break;
                case "refuse":
                    tmp = new Refuse(contido, remitente, nomeReceptor, id);
                    break;
            }
            if (!resposta && posibleResposta != null){
                //Se finalmente obtemos unha mensaxe engadese unha conversa e enviase a id da mesma no mensaxe
                if (tmp != null && nomeReceptor != gameObject.name) {
                    Conversa aux = new Conversa(tmp, posibleResposta);
                    tmp.SetId(aux.GetId());
                    this.listaConversas.Add(aux);
                }
            }   
        }
		return tmp;
	}
    /// <summary>
    /// Crea e envia un mensaxe a un axente
    /// </summary>
    /// <param name="performativa">String tipo da performativa a enviar </param>
    /// <param name="contido">String contido da mensaxe</param>
    /// <param name="remitente">String nome de quen envía a mensaxe</param>
    /// <param name="nomeReceptor">String a quen vai dirixida a mensaxe</param>
    /// <param name="receptor"> Script_Comunicacion compoñente do axente ao que enviaselle a mensaxe</param>
    /// <param name="id">string id da conversa se é unha resposta ou -1 noutro caso</param>
    public void EnviarMensaxe(string performativa, string contido,string remitente, string nomeReceptor, Script_Comunicacion receptor,string id ){

        if (!remitente.Equals(nomeReceptor)) {
            Mensaxe mensaxe = CrearMensaxe(performativa, contido, remitente, nomeReceptor, id);
            if (mensaxe != null){
                receptor.RecibirMensaxe(mensaxe);
            }
            else{
                throw new Exception("Creada unha mensaxe null");
            }
        }
		
		
	}

    /// <summary>
    /// Valida a mensaxe recibida e delega o tratamento do seu contido a funcion correspondente
    /// </summary>
    /// <param name="mensaxe">Mensaxe a mensaxe recibida</param>
	public void  RecibirMensaxe(Mensaxe mensaxe){
		string performativa = mensaxe.GetPerformativa();
		string contido = mensaxe.GetContido();
		string remitente = mensaxe.GetRemitente();
		string receptor = mensaxe.GetReceptor();
        string id = mensaxe.GetIdConversa();
		//So atendera a mensaxe se non a enviou el mesmo e se a mensaxe e para o axente
		if (remitente != gameObject.name){
			if (receptor == gameObject.name || receptor == "todos"){
				//Se e unha resposta validamos que sexa correcta
				if (mensaxe.IsResposta()){
					if (!ValidarResposta(performativa, mensaxe)){
						new Exception("Resposta incorrecta");
					}
				}
			    //Debug.Log(" Son: " + gameObject.name + " recibin de " + remitente + " a mensaxe :" + performativa + " " + contido + " id:" + id);
				switch (performativa){
					case "inform":
						TratarInform(contido, remitente, id);
						break;
					case "query":	
						TratarQuery(contido, remitente, id);
						break;
					case "request":	
						TratarRequest(contido, remitente, id);
						break;
                    case "accept":
                        TratarAccept(remitente, id);
                        break;
                    case "refuse":
                        TratarRefuse(remitente, id);
                        break;
                }
			}
		}
	}
    /// <summary>
    /// Funcion auxiliar que comproba se o mensaxe de resposta responde a algunha conversa existente
    /// e se corresponde con tipo de resposta esperada
    /// </summary>
    /// <param name="performativa"></param>
    /// <param name="mensaxe"></param>
    /// <returns></returns>
	private bool ValidarResposta(string performativa, Mensaxe mensaxe){
		bool correcta = false;
        List<Conversa> listaConversasAux = new List<Conversa>(listaConversas);
		foreach(Conversa conversa in listaConversasAux){
            //Comparten id e destinatario
            if ((conversa.GetId().Equals(mensaxe.GetIdConversa())) && (conversa.GetNomeReceptor().Equals(mensaxe.GetRemitente()))){
                //A resposta é unha das esperadas
				string [] posiblesRespostas = conversa.GetPosibleResposta().Split('\\');
				foreach(string posibleResposta in posiblesRespostas){
					if (posibleResposta.Equals(performativa)){
					    correcta = true;
                        this.listaConversas.Remove(conversa);
                        break;
                    }
				}
			}
		}
		return correcta;
	}

	//Trata a informacion recibida por unha mensaxe inform
	abstract protected void TratarInform(string contido, string remitente, string id); 
	//Trata a informacion recibida por unha mensaxe query
	abstract protected void TratarQuery(string contido, string remitente, string id); 
	//Trata a informacion recibida por unha mensaxe request
	abstract protected void TratarRequest(string contido, string remitente, string id);
    //Trata a informacion recibida por unha mensaxe accept
    abstract protected void TratarAccept(string remitente, string id);
    //Trata a informacion recibida por unha mensaxe refuse
    abstract protected void TratarRefuse(string remitente, string id);
}
