﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Mensaxe{

	protected int RESPOSTA = 1;
	protected int INICIO_CONVERSA = 0;

	protected string performativa;
	protected string contido; 
	protected string remitente;
	protected string receptor;
    private string idConversa;

	
	public Mensaxe(string remitente, string receptor, string idConversa){
		if (remitente ==null){
			throw new Exception("O remitente e  nulo na creacion do mensaxe"); 
		}
		this.remitente = remitente;
		if (receptor ==null){
			throw new Exception("O receptor e nulo na creacion do mensaxe"); 
		}	
		this.receptor = receptor;
        this.idConversa = idConversa;
			
	}

	public string GetPerformativa() {
		return this.performativa;
	}

	public string GetContido() {
		return this.contido;
	}
	public string GetRemitente() {
		return this.remitente;
	}
	public string GetReceptor() {
		return this.receptor;
	}
    public void SetId(string id){
        this.idConversa = id;
    }
    public bool IsResposta() {
        return idConversa.Equals("-1") ? false : true;
    }
    public string GetIdConversa() {
        return idConversa;
    }
}

public class Inform : Mensaxe{

	public Inform (string contido,string remitente, string receptor, string id):base(remitente,receptor,id){
		if (contido ==null){
			throw new Exception("Formato invalido"); 
		}
        string[] contidoDiv = contido.Split('\\');
		if (contidoDiv.Length < 2){ 
			throw new Exception("Formato invalido"); 
		}
		this.performativa = "inform";
		this.contido = contido;
	}
	
}


public class Query: Mensaxe{

	public Query (string contido,string remitente, string receptor, string id):base(remitente,receptor,id){
		if (contido ==null){
			throw new Exception("Formato invalido"); 
		}
		this.performativa = "query";
		this.contido = contido;
	}
	
}

public class Request : Mensaxe{

	public Request(string contido,string remitente, string receptor, string id):base(remitente,receptor,id){
		if (contido ==null){
			throw new Exception("Formato invalido"); 
		}
		this.performativa = "request";
		this.contido = contido;
	}
	
}

public class Accept: Mensaxe{

	public Accept (string contido,string remitente, string receptor, string id):base(remitente,receptor, id){
		if (contido !=null){
			throw new Exception("Formato invalido"); 
		}
		this.contido = null;
		this.performativa = "accept";
	}
	
}

public class Refuse : Mensaxe
{

    public Refuse(string contido, string remitente, string receptor, string id) : base(remitente, receptor,  id)
    {
        if (contido != null)
        {
            throw new Exception("Formato invalido");
        }
        this.contido = null;
        this.performativa = "refuse";
    }

}