﻿using System;
using System.Collections.Generic;
using UnityEngine;
//Almacena a informacion sobre unha conversa activa
public class Conversa{

	private string posibleResposta;
	private Mensaxe mensaxe;
	private float tempo;
	private Script_Comunicacion axente;
    private string id;
    private const float TempoEspera = 2.0f;

    public Conversa( Mensaxe mensaxe, string posibleResposta){
		this.posibleResposta = posibleResposta;
		this.mensaxe = mensaxe;
		tempo = TempoEspera;
        id = DateTime.Now.TimeOfDay.ToString();
	}	


	public string GetNomeReceptor(){
		return this.mensaxe.GetReceptor();
	}
	public string GetPosibleResposta(){
		return this.posibleResposta;
	}

	public float GetTempo(){
		return tempo;
	}
    public Mensaxe GetMensaxe() {
        return mensaxe;
    }
	public void SetTempo(float tempo){
		this.tempo = tempo;
	}
    
    public string GetId(){

        return id;
    }
}
