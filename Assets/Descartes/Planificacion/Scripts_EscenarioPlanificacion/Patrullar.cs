﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;


public class Patrullar : MonoBehaviour {

    public Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent axente;


    void Start () {
        axente = GetComponent<NavMeshAgent>();

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the axente doesn't slow down as it
        // approaches a destination point).
        axente.autoBraking = false;

        IrPuntoSeguinte(true);
    }

    public void ReStart() {
        destPoint = 0;
        IrPuntoSeguinte(true);      
    }
    void IrPuntoSeguinte(bool inicio) {
        bool fin = false;
        // Se non existen puntos 
        if (points.Length == 0)
        	return;

        //O destino do axente e o proximo punto
        axente.destination = points[destPoint].position;

        //Mira se é o final da ronda
        if (destPoint == 0 && (!inicio)) {
            fin = true;
        }
        // Elixe o seguinte punto no array,
        // Regresa o inicio do array se e necesario
        destPoint = (destPoint + 1) % points.Length;
        //Ao final de cada ronda espera un pouco
        if (fin) {
            axente.isStopped = true;
            StartCoroutine("Camiñar", 2f);
        }
        
    }
    void Update () {
        // Cando se chega ao punto elixese o seguinte.
        if (!axente.pathPending && axente.remainingDistance < 0.5f)
        {
            IrPuntoSeguinte(false);
            
        }
    }
    IEnumerator Camiñar(float retraso)
    {
        yield return new WaitForSeconds(retraso);
        axente.isStopped = false;
    }
}    

