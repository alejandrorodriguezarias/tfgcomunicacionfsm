﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Porta : MonoBehaviour{

    private bool estaPechada = true;
    private float TEMPOAUTOCIERRE = 5f;
    private float autocierre;
    public Animator controlAnimacions;

    void Start() {
        autocierre = TEMPOAUTOCIERRE;
    }
    void Update() {
        if (!estaPechada)
        {
            autocierre = autocierre - Time.deltaTime;
        }
        if (autocierre <= 0){
            Pechar();
        }
    }

    public void Abrir() {
        if (estaPechada) {
            this.estaPechada = false;
            controlAnimacions.SetBool("pechar", false);
            controlAnimacions.SetBool("abrir", true);
        }
    }
    public void Pechar() {
        if (!estaPechada){
            this.estaPechada = true;
            Reinicio();
            controlAnimacions.SetBool("abrir", false);
            controlAnimacions.SetBool("pechar", true);
            
        }   
    }
    public void Reinicio() {
        autocierre = TEMPOAUTOCIERRE;
    }
    public bool EstaPechada() {
        if (estaPechada) {
            return true;
        } else return false;
    }
}
