﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Script_Axente : Script_Comunicacion {
    //Constantes
    //Tempo que o axente tarda en abrir a porta
    private float RETRASOPORTAPECHADA = 1.5f;
    //Tempo sen recibir información sobre o axente para consider que ha escapado
    private float TEMPOSENINFORMACION = 3f;
    //Retraso no envio dunha nova ronda de informs polo axente
    public float TEMPOENTREINFORM = 1f;
    //Variables de control para cruzar portas
    private bool atravesandoPorta = false;
    private bool situadoNaPorta = false;
    //O propio NavMeshAgent do axente
    private NavMeshAgent axente;
    private Patrullar ScriptPatrullar;
    private float espaciadoInformarPosXogador = -1f;
    private bool alertaIntruso = false;
    private float xogadorPerdido = 3f;
    private float cambioDirBusqueda = 0f;
    private Vector3 ultimaPosicionXogador;
    private float radioBusqueda = 15;

    // Use this for initialization
    void Start () {
        axente = GetComponent<NavMeshAgent>();
        ScriptPatrullar = GetComponent<Patrullar>();
    }
	
	// Update is called once per frame
	void Update () {
        if (axente.isOnOffMeshLink)
        {
            ManexarCruzarPorta();
        }
        if (espaciadoInformarPosXogador > 0) {
            espaciadoInformarPosXogador = espaciadoInformarPosXogador - Time.deltaTime;
        }
        if (alertaIntruso){
            if (xogadorPerdido > 0){
                xogadorPerdido = xogadorPerdido - Time.deltaTime;
            }
            else {
                AtoparXogador();
            }
        }
    }

    public void  VerXogador(Transform xogador) {
        if (ScriptPatrullar != null) {
            ScriptPatrullar.enabled = false;
        }
        
        axente.destination = xogador.position;
        axente.speed = 3.5f;

        if (espaciadoInformarPosXogador < 0) {
            Script_Comunicacion[] axentes = FindObjectsOfType<Script_Comunicacion>();
            foreach (Script_Comunicacion axente in axentes)
            {
                string contido = "PosicionXogador\\" + xogador.position.ToString();
                EnviarMensaxe("inform", contido, gameObject.name, axente.gameObject.name, axente, "-1");
            }
            espaciadoInformarPosXogador = TEMPOENTREINFORM;
        }
        //O axente cambia o seu estado de alerta
        alertaIntruso = true;
        //Reinicia o contador de tempo sen informacion do xogador
        xogadorPerdido = TEMPOSENINFORMACION;
        ultimaPosicionXogador = xogador.position;
    }

    private void AtoparXogador() {
        if ((axente.remainingDistance < 1) || (cambioDirBusqueda <= 0)){
            //se podría aumentar la dir de busqueda a cada cambio de dirección
            Vector3 puntoAleatorio = dirAleatoria(radioBusqueda);
            radioBusqueda += 0.5f;
            cambioDirBusqueda = 15f;
            axente.destination = puntoAleatorio;
        }else {
            cambioDirBusqueda = cambioDirBusqueda - Time.deltaTime;
        }
    }

    private Vector3 dirAleatoria(float radio)
    {
        //Punto aleatorio dunha esfera
        Vector3 puntoAleatorio = Random.insideUnitSphere * radio;
        //A centramos o seu orixe no axente
        puntoAleatorio += ultimaPosicionXogador;
        NavMeshHit puntoNavMesh;
        //Nav mesh e en 2d mentras que Vector3 e en 3d necesitamos encontrar o punto mais cercano ao 3d en 2d
        NavMesh.SamplePosition(puntoAleatorio, out puntoNavMesh, radio,-1);

        return puntoNavMesh.position;
    }
    protected override void TratarInform(string contido, string remitente, string id)
    {
        string[] contidoDiv = contido.Split('\\');   
        if (contidoDiv[0].Equals("PosicionXogador")){
            //Recuperamos a posicion en string e a convertemos a Vector3
            string[] cifras = contidoDiv[1].Replace("(", "").Replace(")", "").Split(',');
            Vector3 posicion = new Vector3(float.Parse(cifras[0]), float.Parse(cifras[1]), float.Parse(cifras[2]));
            //Se todavia esta na sua ruta de patrulla debe deterse
            if (ScriptPatrullar !=null) {
                ScriptPatrullar.enabled = false;
            }
            //O axente sempre vai a por o xogador MELLORAR
            axente.destination = posicion;
            //O axente cambia o seu estado de alerta
            alertaIntruso = true;
            xogadorPerdido = TEMPOSENINFORMACION;
            ultimaPosicionXogador = posicion;
        }
    }

    protected override void TratarQuery(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    protected override void TratarRequest(string contido, string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    protected override void TratarAccept(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }

    protected override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }
    //***COMPORTAMENTOS PARA MANEXAR O PASO POLAS PORTAS***

    //Comportamento para controlar a velocidade e vista do axente cando atravesa un offmeshlink de porta
    public void ManexarCruzarPorta()
    {
        OffMeshLinkData datos = axente.currentOffMeshLinkData;
        Vector3 fin = new Vector3(datos.endPos.x, axente.transform.position.y, datos.endPos.z);
        if (!situadoNaPorta)
        {
            SituarseNaPorta(datos);
        }
        else
        {
            if (Vector3.Distance(axente.transform.position, fin) > 1.5)
            {
                axente.transform.LookAt(datos.endPos);
            }
        }
        if (atravesandoPorta)
        {
            AtravesarPorta(datos, fin);
        }

    }
    //Comportamento para situarse xusto enfronte da porta ao encontrarse nun offmeshlink de porta
    public void SituarseNaPorta(OffMeshLinkData datos)
    {

        Vector3 inicio = new Vector3(datos.startPos.x, axente.transform.position.y, datos.startPos.z);
        if (axente.transform.position != inicio)
        {
            axente.transform.position = Vector3.MoveTowards(axente.transform.position, inicio, axente.speed * Time.deltaTime);
        }
        else
        {
            situadoNaPorta = true;
            StartCoroutine("AbrirPorta", RETRASOPORTAPECHADA);
        }


    }
    //Inicia a animacion de abrirse a porta
    IEnumerator AbrirPorta(float retraso)
    {
        Script_Porta porta = EncontrarPorta();
        if (porta != null)
        {
            if (porta.EstaPechada())
            {
                porta.Abrir();
            }
            else {
                porta.Reinicio();
                retraso = 0f;
            }
            
        }
        else {
            throw new System.NotImplementedException();
        }
        yield return new WaitForSeconds(retraso);
        atravesandoPorta = true;
    }
    //Encontra o script de comportamento da porta mas cercana
    private Script_Porta EncontrarPorta()
    {
        Script_Porta actual = null;

        Script_Porta[] portas = FindObjectsOfType<Script_Porta>();
        foreach (Script_Porta porta in portas)
        {
            if (actual == null)
            {
                actual = porta;
            }
            else if (Vector3.Distance(axente.transform.position, porta.gameObject.transform.position) < Vector3.Distance(axente.transform.position, actual.gameObject.transform.position))
            {
                actual = porta;
            }
        }
        return actual;
    }

    //Comportamento para cruzar a porta. Tamen indica se xa terminou a tarefa.
    public void AtravesarPorta(OffMeshLinkData datos, Vector3 fin)
    {
        if (axente.transform.position != fin)
        {
            axente.transform.position = Vector3.MoveTowards(axente.transform.position, fin, axente.speed * Time.deltaTime);
        }
        else
        {
            atravesandoPorta = false;
            situadoNaPorta = false;
            axente.CompleteOffMeshLink();
        }
    }

    //***FIN COMPORTAMENTOS PARA MANEXAR O PASO POLAS PORTAS***
}
