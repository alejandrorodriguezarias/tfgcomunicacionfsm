﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Eventos : int {

    EventoVerXogador = 0,
    EventoEquipoFormado = 1,
    EventoPeticionFormarEquipo = 2,
    EventoFinPlanificar = 3,
    EventoEquipoEnPosicion = 4,
    EventoXogadorPerdido = 5,
    EventoEquipoPerseguir = 6,
    EventoXogadorCambioSala = 7,
    EventoVerXogadorLider = 8,

}
