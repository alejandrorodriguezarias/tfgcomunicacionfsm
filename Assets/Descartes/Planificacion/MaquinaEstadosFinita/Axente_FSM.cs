﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Axente_FSM : FSM
{
    Script_Comunicacion[] patrullas = null;
    public Transform[] puntosPatrulla;
    private List<string> nomesEquipo;
    private string liderEquipo;
    private string idConversaLider;
    private Dictionary<string, Transform> centroHabitacions = new Dictionary<string, Transform>();
    private Dictionary<string, List<Transform>> escenario = new Dictionary<string, List<Transform>>();
    private NavMeshAgent IaAxente;
    string posicionXogador = "";
    Vector3 posicionXogadorCoord;
    //Variables de control para cruzar portas
    private bool atravesandoPorta = false;
    private bool situadoNaPorta = false;
    //Tempo que o axente tarda en abrir a porta
    private float RETRASOPORTAPECHADA = 1.5f;


    /// <summary>
    /// La función start se invoca antes del primer update
    /// Rexistra os estados e obten referencias para o resto de axentes
    /// </summary>
    void Start()
    {
        ObterInformacionEscenario();
        patrullas = FindObjectsOfType<Script_Comunicacion>();
        IaAxente = GetComponent<NavMeshAgent>();
        RexistrarEstados();
        RexistrarTransicions();
        Comportamento estado = ObterEstadoInicial();
        estado.Inicio(gameObject);
        nomeEstadoActual = Estados.EstadoPatrullar;
    }
    /// <summary>
    /// Rexistra todos os estados do axente no seu diccionario
    /// </summary>
    private void RexistrarEstados() {
        RexistrarEstadoInicial(new Invocar_Patrullar(), Estados.EstadoPatrullar);
        RexistrarEstado(new Invocar_FormarEquipo(), Estados.EstadoFormarEquipo);
        RexistrarEstado(new Invocar_UnirseEquipo(), Estados.EstadoUnirseEquipo);
        RexistrarEstado(new Invocar_Planificar(), Estados.EstadoPlanificar);
        RexistrarEstado(new Invocar_Posicionarse(), Estados.EstadoPosicionarse);
        RexistrarEstado(new Invocar_Executar(), Estados.EstadoExecutar);
        RexistrarEstado(new Invocar_Perseguir(), Estados.EstadoPerseguir);
        RexistrarEstado(new Invocar_XogadorPerdido(), Estados.EstadoXogadorPerdido);
        RexistrarEstado(new Invocar_PerseguirIndividual(), Estados.EstadoPerseguirIndividual);
        RexistrarEstado(new Invocar_EsperarNovoEquipo(), Estados.EstadoEsperarNovoEquipo);
    }
    /// <summary>
    /// Rexistra todas as transicions do axente
    /// </summary>
    private void RexistrarTransicions() {
        RexistrarTransicion(Estados.EstadoPatrullar, Estados.EstadoFormarEquipo, (int) Eventos.EventoVerXogadorLider);
        RexistrarTransicion(Estados.EstadoPatrullar, Estados.EstadoUnirseEquipo, (int)Eventos.EventoPeticionFormarEquipo);
        RexistrarTransicion(Estados.EstadoFormarEquipo, Estados.EstadoPlanificar, (int)Eventos.EventoEquipoFormado);
        RexistrarTransicion(Estados.EstadoUnirseEquipo, Estados.EstadoPlanificar, (int)Eventos.EventoEquipoFormado);
        RexistrarTransicion(Estados.EstadoPlanificar, Estados.EstadoPosicionarse, (int)Eventos.EventoFinPlanificar);
        RexistrarTransicion(Estados.EstadoPosicionarse, Estados.EstadoExecutar, (int)Eventos.EventoEquipoEnPosicion);
        RexistrarTransicion(Estados.EstadoPosicionarse, Estados.EstadoPerseguirIndividual, (int)Eventos.EventoVerXogador);
        RexistrarTransicion(Estados.EstadoExecutar, Estados.EstadoPerseguir, (int)Eventos.EventoVerXogador);
        RexistrarTransicion(Estados.EstadoExecutar, Estados.EstadoXogadorPerdido, (int)Eventos.EventoXogadorPerdido);
        RexistrarTransicion(Estados.EstadoPerseguir, Estados.EstadoXogadorPerdido, (int)Eventos.EventoXogadorPerdido);
        RexistrarTransicion(Estados.EstadoXogadorPerdido, Estados.EstadoPerseguir, (int)Eventos.EventoVerXogador);
        RexistrarTransicion(Estados.EstadoPerseguirIndividual, Estados.EstadoPerseguir, (int)Eventos.EventoEquipoPerseguir);
        RexistrarTransicion(Estados.EstadoPerseguirIndividual, Estados.EstadoXogadorPerdido, (int)Eventos.EventoXogadorPerdido);
        RexistrarTransicion(Estados.EstadoEsperarNovoEquipo, Estados.EstadoUnirseEquipo, (int)Eventos.EventoPeticionFormarEquipo);
        //Novo lider por cambio de sala
        RexistrarTransicion(Estados.EstadoPosicionarse, Estados.EstadoFormarEquipo, (int)Eventos.EventoVerXogadorLider);
        RexistrarTransicion(Estados.EstadoExecutar, Estados.EstadoFormarEquipo, (int)Eventos.EventoVerXogadorLider);
        RexistrarTransicion(Estados.EstadoPerseguir, Estados.EstadoFormarEquipo, (int)Eventos.EventoVerXogadorLider);
        RexistrarTransicion(Estados.EstadoXogadorPerdido, Estados.EstadoFormarEquipo, (int)Eventos.EventoVerXogadorLider);
        RexistrarTransicion(Estados.EstadoPerseguirIndividual, Estados.EstadoFormarEquipo, (int)Eventos.EventoVerXogadorLider);

        //Cancelar plan por cambio de sala
        RexistrarTransicion(Estados.EstadoPosicionarse, Estados.EstadoEsperarNovoEquipo, (int)Eventos.EventoXogadorCambioSala);
        RexistrarTransicion(Estados.EstadoExecutar, Estados.EstadoEsperarNovoEquipo, (int)Eventos.EventoXogadorCambioSala);
        RexistrarTransicion(Estados.EstadoPerseguir, Estados.EstadoEsperarNovoEquipo, (int)Eventos.EventoXogadorCambioSala);
        RexistrarTransicion(Estados.EstadoXogadorPerdido, Estados.EstadoEsperarNovoEquipo, (int)Eventos.EventoXogadorCambioSala);
        RexistrarTransicion(Estados.EstadoPerseguirIndividual, Estados.EstadoEsperarNovoEquipo, (int)Eventos.EventoXogadorCambioSala);
    }

    /// <summary>
    /// Engade as habitacions e entradas do escenario aos dicionarios correspondentes
    /// </summary>
    private void ObterInformacionEscenario() {
        List<Transform> habitaciones = new List<Transform>();
        GameObject[] temp;
        List<Transform> entradas;
        //Obtemos as habitacions
        temp = GameObject.FindGameObjectsWithTag("habitacion");
        if (temp.Length == 0){
            throw new System.NotImplementedException();
        }else {
            //Obtemos a lista de transforms das habitacions
            foreach (GameObject habitacion in temp)
            {
                habitaciones.Add(habitacion.transform);
                centroHabitacions.Add(habitacion.name, habitacion.transform);
            }
            //Para cada habitacion obtemos as suas entradas
            foreach (Transform habitacion in habitaciones) {
                entradas = new List<Transform>();
                temp = GameObject.FindGameObjectsWithTag(habitacion.gameObject.name);
                //Obtemos a lista de transforms das entradas
                foreach (GameObject entrada in temp)
                {
                    entradas.Add(entrada.transform);
                }
                //Engadimos ao escenario a habitacion coas suas entradas
                escenario.Add(habitacion.name, entradas);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (IaAxente.isOnOffMeshLink)
        {
            ManexarCruzarPorta();
        }
    }

    /// <summary>
    /// Devolve a lista de puntos necesarios para establecer a ruta de patrulla do axente
    /// </summary>
    /// <returns>Transform[] dos puntos da patrulla</returns>
    public Transform[] ObterRutaPatrulla() {
        return puntosPatrulla;
    }
    /// <summary>
    /// Recupera os axentes do escenario
    /// </summary>
    /// <returns>Array cos scripts dos axentes</returns>
    public Script_Comunicacion[] ObterAxentes() {
        return patrullas;
    }
    /// <summary>
    /// Comportamento do axente cando recibe unha mensaxe accept
    /// </summary>
    /// <param name="remitente">Nome do axente que envia a mensaxe</param>
    /// <param name="id">Id da conversa</param>
    protected override void TratarAccept(string remitente, string id)
    {
        Comportamento_FormarEquipo estadoActual = GetComponent<Comportamento_FormarEquipo>();
        if (estadoActual != null){
            estadoActual.TratarAccept(remitente);
        }
    }

    /// <summary>
    /// Obten a lista de conversas do axente
    /// </summary>
    /// <returns>lista conversas do axente</returns>
    public List<Conversa> ObterConversas() {
        return listaConversas;
    }

    public void EstablecerConversas(List<Conversa> conversas) {
        listaConversas = conversas;
    }
    /// <summary>
    /// Establece a lista dos compoñentes do equipo do axente
    /// </summary>
    /// <param name="nomesEquipo">A lista de nomes dos membros do equipo</param>
    public void EstablecerNomesEquipo(List<string> nomesEquipo) {
        this.nomesEquipo = nomesEquipo;
    }
    /// <summary>
    /// Devolve os nomes do equipo
    /// </summary>
    /// <returns>unha lista cos nomes do equipo</returns>
    public List<string> ObterNomesEquipo() {
        return nomesEquipo;
    }
    /// <summary>
    /// Modifica o lider do equipo do axente
    /// </summary>
    /// <param name="liderEquipo">Novo lider de equipo</param>
    public void EstablecerLiderEquipo(string liderEquipo) {
        this.liderEquipo = liderEquipo;
    }
    /// <summary>
    /// Devolver o nome do lider do equipo
    /// </summary>
    /// <returns>String lider do equipo</returns>
    public string ObterLiderEquipo() {
        return liderEquipo;
    }
    /// <summary>
    /// Devolve a id de conversa co lider
    /// </summary>
    /// <returns></returns>
    public string ObterIdConversaLider() {
        return idConversaLider;
    }

    /// <summary>
    /// Obten o escenario do axente
    /// </summary>
    /// <returns>Dictionary coa lista de entradas as habitacions do escenario</returns>
    public Dictionary<string, List<Transform>> ObterEscenario() {
        return escenario;
    }

    public string ObterPosicionXogador() {
        return posicionXogador;
    }

    public Vector3 ObterPosicionXogadorCoord() {
        return posicionXogadorCoord;
    }
    public Dictionary<string, Transform> ObterCentroHabitacions() {
        return centroHabitacions;
    }
    /// <summary>
    /// Se o mensaxe e a posicion do xogador almacena a informacion noutro caso
    /// entrega o mensaxe inform dependendo do comportamento actual
    /// </summary>
    /// <param name="contido">string contido da mensaxe</param>
    /// <param name="remitente">string nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    protected override void TratarInform(string contido, string remitente, string id) {
        string[] contidoDiv = contido.Split('\\');
        if (contidoDiv[0].Equals("xogador"))
        {
            if (!posicionXogador.Equals(contidoDiv[1])){
                posicionXogador = contidoDiv[1];
                DispararEvento((int)Eventos.EventoXogadorCambioSala);
            }
        }
        else {
            if (contidoDiv[0].Equals("xogadorCoord"))
            {
                posicionXogadorCoord = new Vector3 (float.Parse(contidoDiv[1]), float.Parse(contidoDiv[2]), float.Parse(contidoDiv[3]));
                Comportamento_Perseguir estadoPerseguir = GetComponent<Comportamento_Perseguir>();
                if (estadoPerseguir != null)
                {
                    estadoPerseguir.TratarInformXogador(posicionXogadorCoord);
                }
                DispararEvento((int) Eventos.EventoVerXogador);


            }else{
                Comportamento_UnirseEquipo estadoUnirse = GetComponent<Comportamento_UnirseEquipo>();
                if (estadoUnirse != null)
                {
                    estadoUnirse.TratarInform(contido, remitente);
                }
                else
                {
                    Comportamento_Planificar estadoPlanificar = GetComponent<Comportamento_Planificar>();
                    if (estadoPlanificar != null)
                    {
                        estadoPlanificar.TratarInform(contido, remitente);
                    }
                    else
                    {
                        Comportamento_Posicionarse estadoPosicionarse = GetComponent<Comportamento_Posicionarse>();
                        if (estadoPosicionarse != null)
                        {
                            estadoPosicionarse.TratarInform(contido, remitente);
                        }
                    }

                }
            }
        }
        
        
    }

    protected override void TratarQuery(string contido, string remitente, string id)
    {
        Comportamento_Planificar estadoPlanificar = GetComponent<Comportamento_Planificar>();
        if (estadoPlanificar != null)
        {
            estadoPlanificar.TratarQuery(contido, remitente, id);
        }
        else {
            Comportamento_Posicionarse estadoPosicionarse = GetComponent<Comportamento_Posicionarse>();
            if (estadoPosicionarse != null)
            {
                estadoPosicionarse.TratarQuery(contido, remitente, id);
            }
            else {
                Comportamento_PerseguirIndividual estadoPerseguirIndividual = GetComponent<Comportamento_PerseguirIndividual>();
                if (estadoPerseguirIndividual != null)
                {
                    estadoPerseguirIndividual.TratarQuery(contido, remitente, id);
                }
                else
                {
                    Comportamento_XogadorPerdido estadoXogadorPerdido = GetComponent<Comportamento_XogadorPerdido>();
                    if (estadoXogadorPerdido != null)
                    {
                        estadoXogadorPerdido.TratarQuery(contido, remitente, id);
                    }
                }
            }
        }
        
    }

    protected override void TratarRefuse(string remitente, string id)
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// Entrega o mensaxe request dependendo do comportamento actual
    /// </summary>
    /// <param name="contido">string contido da mensaxe</param>
    /// <param name="remitente">string nome do axente que envia a mensaxe</param>
    /// <param name="id">id da conversa</param>
    protected override void TratarRequest(string contido, string remitente, string id)
    {
        if (contido.Equals("equipo")){
            if (GetComponent<Comportamento_Patrullar>() != null || GetComponent<Comportamento_EsperarNovoEquipo>() != null)
            {
                EstablecerLiderEquipo(remitente);
                idConversaLider = id;
                DispararEvento((int)Eventos.EventoPeticionFormarEquipo);
            }
            else {
                Script_Comunicacion receptor = GameObject.Find(remitente).GetComponent<Script_Comunicacion>();
                EnviarMensaxe("refuse", null, gameObject.name, liderEquipo, receptor, id);
            }
        }
    }

    public void VerXogador(Transform posXogador, string habitacionXogador) {
        if (!posicionXogador.Equals(habitacionXogador)) {
            bool primerAxenteVerXogador = true;
            posicionXogador = habitacionXogador;
            foreach (Script_Comunicacion patrulla in patrullas) {
                if (patrulla.gameObject.GetComponent<Comportamento_FormarEquipo>() != null){
                    primerAxenteVerXogador = false;
                }
            }
            foreach (Script_Comunicacion patrulla in patrullas)
            {
                EnviarMensaxe("inform", "xogador\\" + habitacionXogador, gameObject.name, patrulla.name, patrulla, "-1");
            }
            if (primerAxenteVerXogador)
            {
                DispararEvento((int)Eventos.EventoVerXogadorLider);
            }

            //DispararEvento((int)Eventos.EventoXogadorCambioSala);
        }
        posicionXogadorCoord = posXogador.position;
        Comportamento_Perseguir estadoPerseguir = GetComponent<Comportamento_Perseguir>();
        if (estadoPerseguir != null)
        {
            estadoPerseguir.VerXogador(posXogador.position);
        }
        Comportamento_PerseguirIndividual estadoPerseguirIndividual = GetComponent<Comportamento_PerseguirIndividual>();
        if (estadoPerseguirIndividual != null)
        {
            estadoPerseguirIndividual.VerXogador(posXogador.position);
        }
        DispararEvento((int)Eventos.EventoVerXogador);

        

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name.Equals("Xogador")){
            Debug.Log("CAPTURADO");
        }
    }

    //***COMPORTAMENTOS PARA MANEXAR O PASO POLAS PORTAS***

    //Comportamento para controlar a velocidade e vista do axente cando atravesa un offmeshlink de porta
    public void ManexarCruzarPorta()
    {
        OffMeshLinkData datos = IaAxente.currentOffMeshLinkData;
        Vector3 fin = new Vector3(datos.endPos.x, IaAxente.transform.position.y, datos.endPos.z);
        if (!situadoNaPorta)
        {
            SituarseNaPorta(datos);
        }
        else
        {
            if (Vector3.Distance(IaAxente.transform.position, fin) > 1.5)
            {
                IaAxente.transform.LookAt(datos.endPos);
            }
        }
        if (atravesandoPorta)
        {
            AtravesarPorta(datos, fin);
        }

    }
    //Comportamento para situarse xusto enfronte da porta ao encontrarse nun offmeshlink de porta
    public void SituarseNaPorta(OffMeshLinkData datos)
    {

        Vector3 inicio = new Vector3(datos.startPos.x, IaAxente.transform.position.y, datos.startPos.z);
        if (IaAxente.transform.position != inicio)
        {
            IaAxente.transform.position = Vector3.MoveTowards(IaAxente.transform.position, inicio, IaAxente.speed * Time.deltaTime);
        }
        else
        {
            situadoNaPorta = true;
            StartCoroutine("AbrirPorta", RETRASOPORTAPECHADA);
        }


    }
    //Inicia a animacion de abrirse a porta
    IEnumerator AbrirPorta(float retraso)
    {
        Script_Porta porta = EncontrarPorta();
        if (porta != null)
        {
            if (porta.EstaPechada())
            {
                porta.Abrir();
            }
            else
            {
                porta.Reinicio();
                retraso = 0f;
            }

        }
        else
        {
            throw new System.NotImplementedException();
        }
        yield return new WaitForSeconds(retraso);
        atravesandoPorta = true;
    }
    //Encontra o script de comportamento da porta mas cercana
    private Script_Porta EncontrarPorta()
    {
        Script_Porta actual = null;

        Script_Porta[] portas = FindObjectsOfType<Script_Porta>();
        foreach (Script_Porta porta in portas)
        {
            if (actual == null)
            {
                actual = porta;
            }
            else if (Vector3.Distance(IaAxente.transform.position, porta.gameObject.transform.position) < Vector3.Distance(IaAxente.transform.position, actual.gameObject.transform.position))
            {
                actual = porta;
            }
        }
        return actual;
    }

    //Comportamento para cruzar a porta. Tamen indica se xa terminou a tarefa.
    public void AtravesarPorta(OffMeshLinkData datos, Vector3 fin)
    {
        if (IaAxente.transform.position != fin)
        {
            IaAxente.transform.position = Vector3.MoveTowards(IaAxente.transform.position, fin, IaAxente.speed * Time.deltaTime);
        }
        else
        {
            atravesandoPorta = false;
            situadoNaPorta = false;
            IaAxente.CompleteOffMeshLink();
        }
    }

    //***FIN COMPORTAMENTOS PARA MANEXAR O PASO POLAS PORTAS***


    //FUNCIONES DEBUG
    public void DebugEquipo() {
        string nomes = "";
        foreach (string nome in nomesEquipo)
        {
            nomes = nomes + "\\" +  "" + nome + " ";
        }
        Debug.Log(gameObject.name + " " + nomes + " total " +nomesEquipo.Count);
    }
    public void DebugEscenario() {
        string entradas = "";
        foreach (KeyValuePair<string, List<Transform>> habitacion in escenario) {
            entradas = habitacion.Key + ": ";
            foreach (Transform entrada in habitacion.Value)
            {
                entradas = entradas + " " + entrada.gameObject.name + entrada.position + " //";
                
            }
            Debug.Log(entradas);
            Debug.Log(centroHabitacions[habitacion.Key].position);
        }
    }


}
