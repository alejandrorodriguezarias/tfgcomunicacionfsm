﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FSM : Script_Comunicacion
{
    Dictionary<string, Comportamento> estados = new Dictionary<string, Comportamento>();
    string estadoInicial;
    string estadoFinal;
    protected string nomeEstadoActual;
    //<<estadoActual, evento>, siguiente estado>
    Dictionary<Tuple<string,int>,string> transicions = new Dictionary<Tuple<string, int>, string>();

    /// <summary>
    /// Indica o estado inicial da FSM
    /// </summary>
    /// <param name="estado">Clase cos comportamentos da FSM no estado inicial</param>
    /// <param name="id">identificador do estado</param>
    public void RexistrarEstadoInicial(Comportamento estado, string id) {
        if (estado != null && id != null && id != "")
        {
            estados.Add(id, estado);
            estadoInicial = id;
        }
        else {
            throw new Exception("Formato invalido");
        }
        
    }
    /// <summary>
    /// Rexistra o estado final da FSM
    /// </summary>
    /// <param name="estado">Clase cos comportamentos da FSM no estado final</param>
    /// <param name="id">identificador do estado</param>
    public void RexistrarEstadoFinal(Comportamento estado, string id)
    {
        if (estado != null && id != null && id != "")
        {
            estados.Add(id, estado);
            estadoFinal = id;
        }
        else
        {
            throw new Exception("Formato invalido");
        }

    }
    /// <summary>
    /// Rexistra un estado da FSM
    /// </summary>
    /// <param name="estado">Clase cos comportamentos da FSM nun estado concreto</param>
    /// <param name="id">identificador do estado</param>
    public void RexistrarEstado(Comportamento estado, string id)
    {
        if (estado != null && id != null && id != "")
        {
            estados.Add(id, estado);
        }
        else
        {
            throw new Exception("Formato invalido");
        }

    }
    /// <summary>
    /// Rexistra unha nova transición na FSM
    /// </summary>
    /// <param name="estadoActual">estado inicial da transicion</param>
    /// <param name="proximoEstado">estado final da transicion</param>
    /// <param name="evento">evento que dispara a transición</param>
    public void RexistrarTransicion(string estadoActual, string proximoEstado, int evento) {
        if (estadoActual != "" && estadoActual != null && proximoEstado != "" && proximoEstado != null) {
            transicions.Add(new Tuple<string, int>(estadoActual, evento), proximoEstado);
        }
    }
    /// <summary>
    /// Devolve a clase de comportamento rexistrada como estadoInicial
    /// </summary>
    /// <returns></returns>
    public Comportamento ObterEstadoInicial() {
        return estados[estadoInicial];
    }
    /// <summary>
    /// Devolve o comportamento do estado actual
    /// </summary>
    /// <returns>Comportamento do estado actual </returns>
    public Comportamento ObterEstadoActual() {
        return estados[nomeEstadoActual];
    }
    /// <summary>
    /// En caso de que algun factor dispare un evento comprobamos se existe algunha transicion de estado
    /// co estado actual do axente e o evento disparado. En caso afirmativo cambiamos o estado do axente.
    /// </summary>
    /// <param name="evento"></param>
    public void DispararEvento(int evento) {      
        Tuple<string, int> estadoEvento = Tuple.Create(nomeEstadoActual, evento);
        try
        {
            string seguinteEstado = transicions[estadoEvento];
            //Terminamos o anterior comportamento;
            ObterEstadoActual().Fin(gameObject);
            //Obtemos o novo estado e o iniciamos
            Comportamento estado = estados[seguinteEstado];
            estado.Inicio(gameObject);
            nomeEstadoActual = seguinteEstado;
        }
        catch (KeyNotFoundException) {
            
        }
    }
}
