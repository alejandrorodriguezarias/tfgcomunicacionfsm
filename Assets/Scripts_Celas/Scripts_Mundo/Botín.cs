﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botín : MonoBehaviour
{
    public List<Cela> rutaPatrullaCruda;
    public List<string> rutaPatrulla { get; set; }
    public bool rol = false;
    public GameObject habitacionSaida;
    public List<Transform> obxectivosSaida;

    // Start is called before the first frame update
    void Start()
    {
        AdecuarRutaPatrulla();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        GameObject obxectoColision = collision.collider.gameObject;
        if (obxectoColision.tag.Equals("Xogador"))
        {
            Script_ControladorXogador xogador = obxectoColision.transform.gameObject.GetComponent<Script_ControladorXogador>();
            xogador.botinObtido = true;
            if (rol) {
                AlarmaRol();
            }
            else {
                Alarma();
            }
            
            Destroy(gameObject);
        }
    }

    private void AdecuarRutaPatrulla()
    {
        rutaPatrulla = new List<string>();
        string[] puntoDiv;
        string puntoAdecuado;
        foreach (Cela punto in rutaPatrullaCruda)
        {
            puntoDiv = punto.gameObject.name.Split(',');
            puntoAdecuado = puntoDiv[1] + ',' + puntoDiv[2];
            rutaPatrulla.Add(puntoAdecuado);
        }
    }
    private void Alarma() {

        Axente_FSM_Celas[] axentes = FindObjectsOfType<Axente_FSM_Celas>();
        foreach (Axente_FSM_Celas axente in axentes) {
            axente.CambioObxectivo(rutaPatrulla);
        }
        //Ladron ladron = FindObjectOfType<Ladron>();
        //if (ladron != null)
        //{
        //    ladron.Escapar();
        //}
    }

    private void AlarmaRol() {
        Axente_FSM_Rol[] axentes = FindObjectsOfType<Axente_FSM_Rol>();
        foreach (Axente_FSM_Rol axente in axentes)
        {
            axente.habitacionObxectivo = habitacionSaida;
            axente.obxectivos = obxectivosSaida;
            axente.DispararEvento((int)Eventos_Rol.EventoBotinRobado);
        }
    }
}
