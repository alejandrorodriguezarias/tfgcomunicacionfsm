﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saida : MonoBehaviour
{
    public List<Cela> rutaPatrullaCruda;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject obxectoColision = collision.collider.gameObject;
        if (obxectoColision.tag.Equals("Xogador"))
        {
            Script_ControladorXogador xogador = obxectoColision.transform.gameObject.GetComponent<Script_ControladorXogador>();
            if (xogador.botinObtido)
            {
                Time.timeScale = 0;
                Debug.Log("Fin Escenario");
            }
            else {
                Debug.Log("Falta botín");
            }
        }
    }
}
