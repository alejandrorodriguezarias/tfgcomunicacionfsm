﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaboleiroDebug : MonoBehaviour
{
    public int tamañoX;
    public int tamañoZ;
    public int tamañoCasilla;
    private int numColumnas;
    private int numFilas;
    public Transform celaVacia;
    public Transform celaParede;
    public Transform porta;
    // Start is called before the first frame update
    void Start()
    {
        //numColumnas = tamañoX / tamañoCasilla;
        //numFilas = tamañoZ / tamañoCasilla;
        //float posicionX = 1.5f;
        //float posicionZ = 1.5f;
        //for (int i = 0; i < numFilas; i++)
        //{
        //    //Instantiate(cela, new Vector3(posicionX, 0, 2.5f), Quaternion.identity);
        //    for (int j = 0; j < numColumnas; j++)
        //    {
        //        //Borde escenario
        //        if (i == 0 || i == numFilas - 1 || j == 0 || j == numColumnas - 1)
        //        {
        //            Transform cela = Instantiate(celaParede, new Vector3(posicionX, 1.5f, posicionZ), Quaternion.identity);
        //            cela.gameObject.name = "celaParede" + "," + i + "," + j;

        //        }
        //        //else if (i == 1 && (j == numColumnas - 14 || j == numColumnas - 17 || j == numColumnas - 25 || j == numColumnas - 26 || j == numColumnas - 27 || j == numColumnas - 28 || j == numColumnas - 29))
        //        //{
        //        //    Transform cela = Instantiate(celaParede, new Vector3(posicionX, 1.5f, posicionZ), Quaternion.identity);
        //        //    cela.gameObject.name = "celaParede" + "," + i + "," + j;
        //        //}
        //        else
        //        {
        //            Transform cela = Instantiate(celaVacia, new Vector3(posicionX, 0, posicionZ), Quaternion.identity);
        //            cela.gameObject.name = "cela" + "," + i + "," + j;
        //        }

        //        posicionX += 3f;
        //    }
        //    posicionZ += 3f;
        //    posicionX = 1.5f;
        //}
    }

    private void OnDrawGizmos()
    {
        numColumnas = tamañoX / tamañoCasilla;
        numFilas = tamañoZ / tamañoCasilla;
        Gizmos.color = Color.white;
        for (int i = 0; i <= numFilas; i++)
        {

            Gizmos.DrawLine(new Vector3(0, 0, i * tamañoCasilla), new Vector3(tamañoX, 0, i * tamañoCasilla));
        }
        for (int i = 0; i <= numColumnas; i++)
        {
            Gizmos.DrawLine(new Vector3(i * tamañoCasilla, 0, 0), new Vector3(i * tamañoCasilla, 0, tamañoZ));
        }
    }
    // Update is called once per frame
    void Update()
    {
        //for (int i = 0; i <= numFilas; i++) {
        //    Debug.DrawLine(new Vector3(i*tamañoCasilla,0,0), new Vector3(i * tamañoCasilla, 0, tamañoX), Color.white, 2f);
        //    Debug.Log(new Vector3(i * tamañoCasilla, 0, 0));
        //}
        //for (int i = 0; i <= numColumnas; i++)
        //{
        //    Debug.DrawLine(new Vector3(0, 0, i * tamañoCasilla), new Vector3(tamañoZ, 0, i * tamañoCasilla), Color.white, 2f);
        //}
    }
}
