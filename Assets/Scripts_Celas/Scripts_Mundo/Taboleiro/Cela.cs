﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cela : MonoBehaviour
{
    public int XCoord;
    public int YCoord;
    private Dictionary<string,int> transicions = new Dictionary<string,int>();
    public bool ocupada { get; set; } = false;
    public bool porta = false;
    public Script_Porta scriptPorta;
    // Start is called before the first frame update
    void Start()
    {
        string[] contidoDiv = gameObject.name.Split(',');
        XCoord = int.Parse(contidoDiv[1]);
        YCoord = int.Parse(contidoDiv[2]);
        GameObject cela;
        for (int i = XCoord - 1; i < XCoord + 2; i++)
        {
            for (int j = YCoord - 1; j < YCoord + 2; j++)
            {
                if (!(i == XCoord && j == YCoord))
                {
                    cela = GameObject.Find("cela," + i.ToString() + "," + j.ToString());
                    if (cela != null)
                    {
                        transicions.Add(cela.gameObject.name, 1);
                    }
                    else
                    {
                        cela = GameObject.Find("porta," + i.ToString() + "," + j.ToString());
                        if (cela != null)
                        {
                            transicions.Add(cela.gameObject.name, 2);
                        }
                    }
                }
            }
        }


        //for (int i = XCoord - 1; i < XCoord + 2; i++)
        //{
        //    int j = YCoord;
        //    if (!(i == XCoord && j == YCoord))
        //    {
        //        cela = GameObject.Find("cela," + i.ToString() + "," + j.ToString());
        //        if (cela != null)
        //        {
        //            transicions.Add(cela.gameObject.name, 1);
        //        }
        //        else
        //        {
        //            cela = GameObject.Find("porta," + i.ToString() + "," + j.ToString());
        //            if (cela != null)
        //            {
        //                transicions.Add(cela.gameObject.name, 2);
        //            }
        //        }
        //    }

        //}
        //for (int j = YCoord - 1; j < YCoord + 2; j++)
        //{
        //    int i = XCoord;
        //    if (!(i == XCoord && j == YCoord))
        //    {
        //        cela = GameObject.Find("cela," + i.ToString() + "," + j.ToString());
        //        if (cela != null)
        //        {
        //            transicions.Add(cela.gameObject.name, 1);
        //        }
        //        else
        //        {
        //            cela = GameObject.Find("porta," + i.ToString() + "," + j.ToString());
        //            if (cela != null)
        //            {
        //                transicions.Add(cela.gameObject.name, 2);
        //            }
        //        }
        //    }
        //}

    }

    public bool EPorta() {
        return porta;
    }
    public Dictionary<string, int> ObterTransicions() {
        return transicions;
    }
    // Update is called once per frame
    void Update()
    {
        //string[] contidoDiv = gameObject.name.Split(',');
        //if (contidoDiv[0].Equals("porta"))
        //{
        //}
        //else {
        //    if (ocupada)
        //    {
        //        //Fetch the Renderer from the GameObject
        //        Renderer rend = gameObject.GetComponent<Renderer>();

        //        //Set the main Color of the Material to green
        //        rend.material.shader = Shader.Find("Standard");
        //        rend.material.SetColor("_Color", Color.green);
        //    }
        //    else
        //    {
        //        //Fetch the Renderer from the GameObject
        //        Renderer rend = gameObject.GetComponent<Renderer>();
        //        rend.material.shader = Shader.Find("Standard");
        //        rend.material.SetColor("_Color", Color.blue);
        //    }
        //}
        
    }
    //void onmousedown()
    //{
    //    posicionautomatica amanuense = gameobject.find("creadortaboleiro").getcomponent<posicionautomatica>();
    //    amanuense.rexistrar(gameobject.name);
    //}
}
