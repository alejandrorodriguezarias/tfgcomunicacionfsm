﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sala : MonoBehaviour
{
    public List<Cela> entradas;
    public Cela centro;
    public List<Sala> salasBotin;
    public List<Sala> salasEscape;

    public List<Cela> ObterEntradas() {
        return entradas;
    }

    public Cela ObterCentro() {
        return centro;
    }

    public List<Sala> ObterSalasCercanas(){
        Script_ControladorXogador xogador = GameObject.Find("Xogador").GetComponent<Script_ControladorXogador>();
        if (xogador.botinObtido) {
            return salasEscape;
        } else {
            return salasBotin;
        }
    }
}
