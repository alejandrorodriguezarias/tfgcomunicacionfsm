﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Eventos_Celas : int {

    EventoPuntoPatrulla = 1,
    EventoCaminoObtido = 2,
    EventoMovementoCompletado = 3,
    EventoLider = 4,
    EventoPeticionEquipo = 5,
    EventoEquipoFormado = 6,
    EventoFinPlanificar = 7,
    EventoCaminoBloqueado = 8,
    EventoEnPosicion = 9,
    EventoXogadorPerdido = 10,
    EventoPatrullarObxectivo = 11,
    EventoVerXogador = 55,

}
