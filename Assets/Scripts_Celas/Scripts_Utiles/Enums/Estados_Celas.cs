﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Estados_Celas {

    public const string EstadoPatrullar = "E0";
    public const string EstadoFormarEquipo = "E1";
    public const string EstadoUnirseEquipo = "E2";
    public const string EstadoDividirTareas = "E3";
    public const string EstadoPosicionarse = "E4";
    public const string EstadoEntrarSala = "E5";
    public const string EstadoPerseguir = "E6";
    public const string EstadoXogadorPerdido = "E7";
    public const string EstadoPatrullarObxectivo = "E8";

}
