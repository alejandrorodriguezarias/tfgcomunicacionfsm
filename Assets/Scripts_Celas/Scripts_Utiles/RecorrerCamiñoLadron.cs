﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RecorrerCamiñoLadron : MonoBehaviour
{

    Ladron ladron;
    private Cela proximaCela;
    int i = 0;
    List<string> caminos = null;
    public float velocidade = 0.04f;
    NavMeshAgent axenteNav;
    private const float RetrasoPortaPechada = 5f;
    public bool finCamino { get; set; } = false;
    private BusquedaCamiñoLadron buscador;
    //Debug
    public bool debug;
    public Transform son;
    private const float TempoEntreEmisions = 1.5f;
    private const int RadioSon = 7;
    private float tempoUltimaEmision = 0f;
    // Start is called before the first frame update
    void Start()
    {
        ladron = gameObject.GetComponent<Ladron>();
        axenteNav = gameObject.GetComponent<NavMeshAgent>();
        buscador = gameObject.GetComponent<BusquedaCamiñoLadron>();
    }

    public void ObterDestino() {
        i = 0;
        finCamino = false;
        caminos = ladron.camino;
        if (ladron.camino != null)
        {
            if (ladron.camino.Count == 0)
            {
                finCamino = true;
            }
            else {
                axenteNav.destination = gameObject.transform.position;
                proximaCela = GameObject.Find(caminos[i]).GetComponent<Cela>();
                proximaCela.ocupada = false;
                if (proximaCela.EPorta())
                {
                    axenteNav.isStopped = true;
                    Script_Porta porta = proximaCela.transform.GetChild(0).gameObject.GetComponent<Script_Porta>();
                    if (porta.EstaPechada())
                    {
                        porta.Abrir();
                        StartCoroutine("EsperarPorta");
                    }
                    else
                    {
                        porta.Reinicio();
                        axenteNav.isStopped = false;
                    }
                }
                i++;
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (axenteNav.remainingDistance < 1 && ladron.camino !=null && !axenteNav.pathPending)
        {
            ladron.celaActual = proximaCela;
            if (i < caminos.Count)
            {
                proximaCela.ocupada = false;
                if (tempoUltimaEmision < 0)
                {
                    EmitirSon(proximaCela);
                    if (!debug)
                    {
                        Instantiate(son, transform.position, son.rotation); //MANTENER
                    }
                    tempoUltimaEmision = TempoEntreEmisions;
                }
                else
                {
                    tempoUltimaEmision -= Time.deltaTime;
                }
                proximaCela = GameObject.Find(caminos[i]).GetComponent<Cela>();
                if (!proximaCela.ocupada)
                {
                    //proximaCela.ocupada = true;
                    //Debug.Log("proximaCela: " + proximaCela.transform.position);
                    axenteNav.destination = proximaCela.transform.position;
                    if (proximaCela.EPorta())
                    {
                        axenteNav.isStopped = true;
                        Script_Porta porta = proximaCela.transform.GetChild(0).gameObject.GetComponent<Script_Porta>();
                        if (porta.EstaPechada())
                        {
                            porta.Abrir();
                            StartCoroutine("EsperarPorta");
                        }
                        else
                        {
                            porta.Reinicio();
                            axenteNav.isStopped = false;
                        }
                    }
                    i++;
                }
                else
                {
                    ladron.camino = buscador.ObterCamino(gameObject);
                    ObterDestino();
                }
            }
            else
            {
                finCamino = true;
                proximaCela.ocupada = true;
                axenteNav.isStopped = true;
            }
        }
        tempoUltimaEmision -= Time.deltaTime;
    }
    IEnumerator EsperarPorta()
    {
        yield return new WaitForSeconds(0.8f);
        axenteNav.isStopped = false;
    }
    private void Mover()
    {
        float movementoX = 0;
        float movementoZ = 0;
        if ((gameObject.transform.position.x - proximaCela.transform.position.x) > 0)
        {
            movementoX = -velocidade;
        }
        else if ((gameObject.transform.position.x - proximaCela.transform.position.x) < 0)
        {
            movementoX = velocidade;
        }
        if ((gameObject.transform.position.z - proximaCela.transform.position.z) > 0)
        {
            movementoZ = -velocidade;
        }
        else if ((gameObject.transform.position.z - proximaCela.transform.position.z) < 0)
        {
            movementoZ = velocidade;
        }
        Vector3 movemento = new Vector3(movementoX, 0.0f, movementoZ);
        transform.Translate(movemento, Space.World);
        Vector3 vista = new Vector3(proximaCela.transform.position.x, gameObject.transform.position.y, proximaCela.transform.position.z);

        if (Math.Abs(gameObject.transform.position.x - proximaCela.transform.position.x) > 0.2 && Math.Abs(gameObject.transform.position.z - proximaCela.transform.position.z) > 0.2)
        {
            transform.LookAt(vista);

        }
        //transform.rotation.y = 0;
    }
    private void EmitirSon(Cela celaActual)
    {
        Escoitar_Celas[] axentes = FindObjectsOfType<Escoitar_Celas>();
        foreach (Escoitar_Celas axente in axentes)
        {
            if ((RadioSon - Vector3.Distance(transform.position, axente.transform.position)) > 0)
            {
                axente.Escoitar(celaActual, ladron.habitacionActual);
            }
        }
        if (debug)
        {
            Axente_Humano xogador = FindObjectOfType<Axente_Humano>();
            if ((RadioSon - Vector3.Distance(transform.position, xogador.transform.position)) > 0)
            {
                Instantiate(son, transform.position, son.rotation);
            }
        }
    }
}
