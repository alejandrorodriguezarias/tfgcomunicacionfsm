﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColaPrioridad { 

    private List<NodoPrioridad> listaNodos;

    public ColaPrioridad() {
        listaNodos = new List<NodoPrioridad>();
    }

    public void Insertar(NodoPrioridad nodo) {
        int index = -1;
        bool insertar = true;
        NodoPrioridad copia = null;
        for (int i = 0; i < listaNodos.Count; i++) {
            if (listaNodos[i].costeTotal > nodo.costeTotal) {
                index = i;
            }
            if (listaNodos[i].nomeCela.Equals(nodo.nomeCela) && listaNodos[i].costeReal > nodo.costeReal)
            {
                copia = listaNodos[i];
            }
            else {
                if (listaNodos[i].nomeCela.Equals(nodo.nomeCela)) {
                    insertar = false;
                }
            }
        }
        if (index == -1)
        {
            index = listaNodos.Count;
        }
        if (insertar) {
            listaNodos.Insert(index, nodo);
            if (copia != null)
            {
                listaNodos.Remove(copia);
            }
        }
    }

    public NodoPrioridad ObterNodo() {
        NodoPrioridad nodo = listaNodos[0];
        listaNodos.Remove(nodo);
        return nodo;
    }
    public int Tamaño() {
        return listaNodos.Count;
    }
}