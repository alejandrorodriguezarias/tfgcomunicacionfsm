﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BusquedaCamiño
{
    private string destino;
    private int destinoXCoord;
    private int destinoYCoord;
    Axente_FSM_Celas axente;
    Cela celaActual;
    // Start is called before the first frame update
    public List<string> ObterCamino(GameObject gameObject)
    {
        axente = gameObject.GetComponent<Axente_FSM_Celas>();
        destino = axente.ObterDestino();
        string[] destinoDiv = destino.Split(',');
        destinoXCoord = int.Parse(destinoDiv[0]);
        destinoYCoord = int.Parse(destinoDiv[1]);
        celaActual = axente.ObterCelaActual();
        List<string> camino;
        if (celaActual.XCoord == destinoXCoord && celaActual.YCoord == destinoYCoord)
        {
            Debug.Log("Fin");
            return new List<string>();
        }
        else
        {
            camino = BusquedaA();
            if (camino!=null)
            {
                return camino;
            }
        }
        return null;
    }

    public List<string> BusquedaA()
    {
        bool caminoIncompleto = true;
        ColaPrioridad frontera = new ColaPrioridad();
        bool estadoExplorado = false;
        List<NodoPrioridad> explorados = new List<NodoPrioridad>();
        NodoPrioridad nodo;
        nodo = new NodoPrioridad(celaActual.gameObject.name, 0, ObterHeuristicaA(celaActual.gameObject.name), null);
        NodoPrioridad nodoExpl;
        frontera.Insertar(nodo);
        while (caminoIncompleto)
        {
            if (frontera.Tamaño() == 0)
            {
                Debug.Log("ops");
                return null;
            }
            nodo = frontera.ObterNodo();
            nodoExpl = nodo;
            explorados.Add(nodoExpl);
            if (ComprobarMeta(nodo.nomeCela))
            {
                caminoIncompleto = false;
                return ConstruirCaminoA(nodo, explorados);
            }
            Dictionary<string, int> novos = new Dictionary<string, int>(GameObject.Find(nodo.nomeCela).GetComponent<Cela>().ObterTransicions());
            foreach (KeyValuePair<string, int> novo in novos)
            {
                estadoExplorado = false;
                foreach (NodoPrioridad explorado in explorados)
                {
                    if (explorado.nomeCela.Equals(novo.Key))
                    {
                        estadoExplorado = true;
                    }
                }
                if (!estadoExplorado)
                {
                    Cela tmp = GameObject.Find(novo.Key).gameObject.GetComponent<Cela>();
                    //Mod 8 Dir
                    if ((!tmp.ocupada && DiagonalLibre(nodo, tmp)) || ComprobarMeta(novo.Key))
                    {
                        nodo = new NodoPrioridad(novo.Key, nodoExpl.costeReal + novo.Value, ObterHeuristicaA(novo.Key), nodoExpl.nomeCela);
                        frontera.Insertar(nodo);
                    }
                }
            }
        }
        Debug.Log("opss");
        return null;
    }
    private bool DiagonalLibre(NodoPrioridad nodo, Cela posibleDestino) {
        string nomePosibleObstaculo = "";
        string nomePosibleObstaculo2 = "";
        GameObject posibleObstaculo;
        GameObject posibleObstaculo2;
        bool bloqueo;
        bool bloqueo2;

        string[] coord = nodo.nomeCela.Split(',');
        int XCoord = int.Parse(coord[1]);
        int YCoord = int.Parse(coord[2]);

        coord = posibleDestino.name.Split(',');
        int XCoordP = int.Parse(coord[1]);
        int YCoordP = int.Parse(coord[2]);
        if (XCoord == XCoordP || YCoord == YCoordP) {
            return true;
        }
        if (XCoordP < XCoord && YCoordP < YCoord)
        {
            nomePosibleObstaculo = "cela," + (XCoord - 1) + "," + YCoord;
            nomePosibleObstaculo2 = "cela," + XCoord + "," + (YCoord - 1);
        }
        else if (XCoordP < XCoord && YCoordP > YCoord)
        {
            nomePosibleObstaculo = "cela," + (XCoord - 1) + "," + YCoord;
            nomePosibleObstaculo2 = "cela," + XCoord + "," + (YCoord + 1);
        }
        else if (XCoordP > XCoord && YCoordP < YCoord)
        {
            nomePosibleObstaculo = "cela," + (XCoord + 1) + "," + YCoord;
            nomePosibleObstaculo2 = "cela," + XCoord + "," + (YCoord -1);
        }
        else if (XCoordP > XCoord && YCoordP > YCoord)
        {
            nomePosibleObstaculo = "cela," + (XCoord + 1) + "," + YCoord;
            nomePosibleObstaculo2 = "cela," + XCoord + "," + (YCoord + 1);
        }

        posibleObstaculo = GameObject.Find(nomePosibleObstaculo);
        posibleObstaculo2 = GameObject.Find(nomePosibleObstaculo2);

        if (posibleObstaculo != null)
        {
            if (posibleObstaculo.GetComponent<Cela>().ocupada)
            {
                bloqueo = true;
            }else {
                bloqueo = false;
            }
        }else {
            bloqueo = true;
        }

        if (posibleObstaculo2 != null)
        {
            if (posibleObstaculo2.GetComponent<Cela>().ocupada)
            {
                bloqueo2 = true;
            }else{
                bloqueo2 = false;
            }
        }else{
            bloqueo2 = true;
        }

        if (bloqueo && bloqueo2)
        {
            return false;
        }
        else {
            return true;
        }
        //Debug.Log("Origen " + nodo.nomeCela);
        //Debug.Log("Posible Destino "  + posibleDestino.gameObject.name);
        //Debug.Log("Obstaculo: " + nomePosibleObstaculo);
        //Debug.Log("Obstaculo: " + nomePosibleObstaculo2);
    }
    public List<string> ConstruirCaminoA(NodoPrioridad celaFinal, List<NodoPrioridad> explorados)
    {
        bool construir = true;
        List<string> camino = new List<string>();
        string iterador = celaFinal.nomeCela;
        while (construir)
        {
            camino.Add(iterador);
            iterador = EncontrarPadre(explorados, iterador);
            if (iterador.Equals(celaActual.gameObject.name))
            {
                camino.Add(iterador);
                construir = false;
            }
        }
        camino.Reverse();
        //foreach (string cela in camino)
        //{
        //    Debug.Log("Camino: " + cela);
        //}
        return camino;

    }
    private string EncontrarPadre(List<NodoPrioridad> explorados, string hijo)
    {

        foreach (NodoPrioridad nodo in explorados)
        {
            if (nodo.nomeCela.Equals(hijo))
            {
                return nodo.padre;
            }
        }
        return null;
    }
    private int ObterHeuristicaA(string nodo)
    {
        int euclidea = -1;
        string[] transicionDiv = nodo.Split(',');
        int XCoord = int.Parse(transicionDiv[1]);
        int YCoord = int.Parse(transicionDiv[2]);
        int distancia = (int)Math.Floor(Math.Sqrt(Math.Pow(destinoXCoord - XCoord, 2) + Math.Pow(destinoYCoord - YCoord, 2)));
        euclidea = distancia;
        return euclidea;
    }
    public List<string> ConstruirCamino(string celaFinal, List<string> cerrados, Dictionary<string, string> padres)
    {
        bool construir = true;
        List<string> camino = new List<string>();
        camino.Add(celaFinal);
        string iterador = celaFinal;
        while (construir)
        {
            iterador = padres[iterador];
            camino.Add(iterador);
            if (iterador.Equals(celaActual.gameObject.name))
            {
                construir = false;
            }
        }
        camino.Reverse();
        foreach (string cela in camino)
        {
            //Debug.Log("Camino: " + cela);
        }
        //List<string> celaUnica = new List<string>();
        return camino;

    }

    private bool ComprobarMeta(string cela)
    {
        string[] celaDiv = cela.Split(',');
        int celaXCoord = int.Parse(celaDiv[1]);
        int celaYCoord = int.Parse(celaDiv[2]);
        bool meta = (celaXCoord == destinoXCoord && celaYCoord == destinoYCoord);
        return meta;
    }
}
