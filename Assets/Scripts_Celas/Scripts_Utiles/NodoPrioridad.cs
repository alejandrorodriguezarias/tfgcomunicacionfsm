﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodoPrioridad
{
    public int costeTotal { get;}
    public int costeReal { get;}
    public int costeHeuristica { get;}
    public string nomeCela { get;}
    public string padre { get; }

    public NodoPrioridad(string nome, int costeReal, int costeHeuristica , string padre) {
        this.costeReal = costeReal;
        this.costeHeuristica = costeHeuristica;
        this.costeHeuristica = costeReal + costeHeuristica;
        this.padre = padre;
        nomeCela = nome;
    }
    
}
