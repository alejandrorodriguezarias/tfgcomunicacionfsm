﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PosicionAutomatica : MonoBehaviour
{
    public Transform marcadorLadron;
    public Transform marcadorAxente;
    public Transform marcadorAxenteXogador;
    private string celaLadron = "";
    private string celaAxente = "";
    private string celaAxente2 = "";
    private string celaAxenteXogador = "";
    private GameObject ladron = null;
    private GameObject axente = null;
    private GameObject axente2 = null;
    private GameObject axenteXogador = null;

    // Start is called before the first frame update
    void Start()
    {
        PrepararEscenario();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Interactuar"))
        {
            PrepararEscenario();
        }
    }

    public void PrepararEscenario() {
        LimparEscenario();
        PosicionarLadron();
        PosicionarAxentes();
        PosicionarAxenteObxectivo();

    }

    private void PosicionarLadron() {
        KeyValuePair<Vector3, string> tmp = ObterPosicionValida();
        Vector3 posicion = tmp.Key;
        celaLadron = tmp.Value;
        Transform ladronTrans = Instantiate(marcadorLadron, posicion, Quaternion.identity);
        ladron = ladronTrans.gameObject;
        
    }

    private void PosicionarAxentes()
    {
        KeyValuePair<Vector3, string> tmp = ObterPosicionValida();
        Vector3 posicion = tmp.Key;
        celaAxente = tmp.Value;
        Transform axenteTrans = Instantiate(marcadorAxente, posicion, Quaternion.identity);
        axente = axenteTrans.gameObject;

        tmp = ObterPosicionValida();
        posicion = tmp.Key;
        celaAxente2 = tmp.Value;
        axenteTrans = Instantiate(marcadorAxente, posicion, Quaternion.identity);
        axente2 = axenteTrans.gameObject;
    }

    private void PosicionarAxenteObxectivo()
    {
        KeyValuePair<Vector3, string> tmp = ObterPosicionValida();
        Vector3 posicion = tmp.Key;
        celaAxenteXogador = tmp.Value;
        Transform axenteXogadorTrans = Instantiate(marcadorAxenteXogador, posicion, Quaternion.identity);
        axenteXogador = axenteXogadorTrans.gameObject;
    }

    private KeyValuePair<Vector3,string> ObterPosicionValida() {
        while (true) {
            string destino = "";
            int xCoord = Random.Range(0, 39);
            int yCoord = Random.Range(0, 59);
            destino = "" + xCoord + ',' + yCoord;
            GameObject cela = GameObject.Find("cela," + destino);
            if (cela != null)
            {
                if (!cela.Equals(celaLadron) && !cela.Equals(celaAxente) && !cela.Equals(celaAxente2) && !cela.Equals(celaAxenteXogador)) {

                }
                Vector3 pos = new Vector3(cela.transform.position.x, cela.transform.position.y, cela.transform.position.z);

                return new KeyValuePair<Vector3, string>(pos, cela.name);
            }
        }
    }
    private void LimparEscenario() {
        Destroy(ladron);
        Destroy(axente);
        Destroy(axente2);
        Destroy(axenteXogador);
        celaLadron = "";
        celaAxente = "";
        celaAxente2 = "";
        celaAxenteXogador = "";
    }
    public void Rexistrar(string celaDestino) {
        string path = "Botin.txt";
        StreamWriter writer = new StreamWriter(path, true);
        string data = celaLadron+ "\t" + celaAxente + "\t" + celaAxente2 + "\t" + celaAxenteXogador + "\t" + celaDestino;
        writer.WriteLine(data);
        writer.Close();
    }
}
